# ZHSI (Zibo737 Glass Cockpit Software Suite)

> ZHSI is a Glass Cockpit Software suite for ZiboMod 737 in XPlane inspired by XHSI

Pre-build v2.2.0 can be downloaded here: (https://gitlab.com/sum1els737/zhsi-releases)

The plugin is available to download under the releases above and the source code for that is here : https://gitlab.com/sum1els737/zhsi-plugin (slightly modified ExtPlane plugin - thanks to Ville Ranki and others.. (https://github.com/vranki/ExtPlane)
![](header.png)
   
## Meta
Andre Els – [https://www.facebook.com/sum1els737](https://www.facebook.com/sum1els737)

Free to modify and use in whatever way you want !