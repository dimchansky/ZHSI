/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.displays.nd;

//import java.awt.Color;
//import java.awt.Graphics2D;
//import java.awt.geom.AffineTransform;
//
//import org.andreels.zhsi.ModelFactory;
//import org.andreels.zhsi.ZHSIPreferences;
//import org.andreels.zhsi.displays.DUGraphicsConfig;
//import org.andreels.zhsi.elevationData.ElevationRepository;
//import org.andreels.zhsi.navdata.CoordinateSystem;
//import org.andreels.zhsi.resources.LoadResources;
//import org.andreels.zhsi.utils.AzimuthalEquidistantProjection;
//import org.andreels.zhsi.utils.Projection;
//import org.andreels.zhsi.xpdata.XPData;

public class ND_VSD   {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

//	private String title;
//	private String pilot;
//	private DUGraphicsConfig gc;
//	private NDComponent parent;
//	private ZHSIPreferences preferences;
//	private LoadResources rs;
//	private ModelFactory model_instance;
//	private XPData xpd;
//	private Projection map_projection;
//	private ElevationRepository elevation;
//	private AffineTransform original_at;
//	private Color original_color;
//	private float center_lat;
//	private float center_lon;
//	private float my_elevation;
//
//	public ND_VSD(ModelFactory model_factory, DUGraphicsConfig gc, NDComponent parent, String title, String pilot) {
//		super(model_factory, gc, parent, title, pilot);
//		this.model_instance = model_factory;
//		this.xpd = this.model_instance.getInstance();
//		this.preferences = ZHSIPreferences.getInstance();
//		this.rs = LoadResources.getInstance();
//		this.gc = gc;
//		this.parent = parent;
//		this.title = title;
//		this.pilot = pilot;
//		this.map_projection = new AzimuthalEquidistantProjection(this.pilot);
//		this.elevation = ElevationRepository.get_instance();
//	}
//
//	@Override
//	public void paint(Graphics2D g2) {
//		original_at = g2.getTransform();
//		original_color = g2.getColor();
//
//		this.center_lat = this.xpd.latitude();
//		this.center_lon = this.xpd.longitude();
//		
//		
//		my_elevation = 578f * 3.28084f;
//
//		
//		g2.setColor(Color.WHITE);
//		g2.drawLine(80, (int)(500 - (my_elevation / 20f)), 95, (int)(500 - (my_elevation / 20f)));
//		int xx = 0;
//		
//		//float elevation1 = this.elevation.get_elevation_at_distance(this.center_lat, this.center_lon, 80, this.xpd.track());
//		//System.out.println(elevation1);
//		
//		for(float i = 0f; i < this.parent.max_range + 1f; i = i + (this.parent.max_range /14f)) {
//			///System.out.println(i);
//			//float[] latlon = CoordinateSystem.latlon_at_dist_heading(this.center_lat, this.center_lon, i, this.xpd.track());
//			//System.out.println(latlon[0] + " -- " + latlon[1]);
//			float elevation = this.elevation.get_elevation_at_distance(this.center_lat, this.center_lon, i, this.xpd.track());
//			
//			float delta_y = elevation - my_elevation;
//			System.out.println(i + " = " +elevation);
//			if(elevation > my_elevation + 2000) {
//				
//				g2.setColor(Color.RED);
//				g2.drawLine((int)(100 +  xx), (int)(500 - ((my_elevation + 2000) / 20f)) , (int)(100 + xx),  (int)(500 - (elevation/ 20f)));
//				g2.setColor(gc.color_amber);
//				g2.drawLine((int)(100 +  xx), (int)(500 - ((my_elevation - 500) / 20f)) , (int)(100 + xx),  (int)(500 - ((my_elevation + 2000) / 20f)));
//				g2.setColor(gc.color_lime);
//				g2.drawLine((int)(100 +  xx), 500 , (int)(100 + xx),  (int)(500 - ((my_elevation - 500) / 20f)));
//			}
//			else if(elevation < my_elevation + 2000 && elevation > my_elevation - 500) {
//				
//				g2.setColor(gc.color_amber);
//				g2.drawLine((int)(100 +  xx), (int)(500 - ((my_elevation - 500) / 20f)) , (int)(100 + xx),  (int)(500 - (elevation/ 20f)));
//				g2.setColor(gc.color_lime);
//				g2.drawLine((int)(100 +  xx), 500 , (int)(100 + xx),  (int)(500 - ((my_elevation - 500) / 20f)));
//			}
//			else if( elevation < my_elevation - 500) {
//				g2.setColor(gc.color_lime);
//				g2.drawLine((int)(100 +  xx), 500 , (int)(100 + xx),  (int)(500 - (elevation/ 20f)));
//			}
//			
////			g2.setColor(Color.RED);
////			g2.drawLine((int)(100 + xx), 500 , (int)(100 + xx), (int)(500 - (elevation / 20f)));
////			g2.setColor(gc.color_amber);
////			g2.drawLine((int)(100 +  xx), 500 , (int)(100 + xx),  (int)(500 - ((amber_min) / 20f)));
////			g2.setColor(gc.color_lime);
////			g2.drawLine((int)(100 +  xx), 500 , (int)(100 + xx),  (int)(500 - ((my_elevation - 500) / 20f)));
//			
//			xx += 20;
//
//			
//			
//		}
//		
//		
//		
//
//	}

}
