/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */

package org.andreels.zhsi.displays.nd;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.andreels.zhsi.ModelFactory;
import org.andreels.zhsi.ZHSIPreferences;
import org.andreels.zhsi.ZHSIStatus;
import org.andreels.zhsi.displays.DUBaseClass;
import org.andreels.zhsi.navdata.Airport;
import org.andreels.zhsi.navdata.CoordinateSystem;
import org.andreels.zhsi.navdata.Fix;
import org.andreels.zhsi.navdata.NavigationObject;
import org.andreels.zhsi.navdata.NavigationObjectRepository;
import org.andreels.zhsi.navdata.NavigationRadio;
import org.andreels.zhsi.navdata.RadioNavBeacon;
import org.andreels.zhsi.navdata.RadioNavigationObject;
import org.andreels.zhsi.navdata.Runway;
import org.andreels.zhsi.utils.AzimuthalEquidistantProjection;
import org.andreels.zhsi.utils.Projection;
import org.andreels.zhsi.utils.RunningAverager;


public class ND2 extends DUBaseClass {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private RunningAverager turn_speed_averager = new RunningAverager(30);
	private NavigationObjectRepository nor;
	private Projection map_projection;
	private RenderTerrain2 terrain;
	private RenderWeather weather;

	private String peak_max_string = "";
	private String peak_min_string = "";

	private Color peak_max_color = new Color(0,0,0);
	private Color peak_min_color = new Color(0,0,0);

	private AffineTransform temp_trans;
	private AffineTransform main_map;
	private AffineTransform airport_trans;
	private AffineTransform vordme_trans;
	private AffineTransform vor_trans;
	private AffineTransform fix_trans;
	private AffineTransform fms_trans;
	@SuppressWarnings("unused")
	private AffineTransform dest_runway_trans;
	@SuppressWarnings("unused")
	private AffineTransform dep_runway_trans;
	private AffineTransform compassRoseTransform;
	@SuppressWarnings("unused")
	private AffineTransform compassRoseTextTransform;
	@SuppressWarnings("unused")
	private Paint original_paint;

	private float scaling_factor = 0.64f;
	private float map_up;
	private float center_lon;
	private float center_lat;
	private float map_center_x = 686f / 2;
	private float map_center_y = 674f / 2;
	private float max_range;
	private float pixels_per_nm;
	private float rose_radius;
	@SuppressWarnings("unused")
	private float pixels_per_deg_lat;
	@SuppressWarnings("unused")
	private float pixels_per_deg_lon;
	private float track_line_rotate = 0f;
	private float terrain_cycle = 0f;
	private float weather_cycle = 0f;
	private float initial_sweep_rotate = 0f;
	private float compass_rotate = 0f;
	private float heading_bug_rotate = 0f;
	private float current_heading_bug_rotate = 0f;

	private int heading_box_offset = 0;
	private int old_map_range_value = 0;
	private int old_map_mode_value = 0;
	private int old_terr_on_value = 0;
	private int old_wxr_on_value = 0;
	private int old_map_ctr_int = 0;
	private int plane_x = 0;
	private int plane_y = 0;
	private int[] vnav_path_xpoints = {1075, 972, 1075};
	private int[] vnav_path_ypoints = {662, 662, 630};   

	private RadioNavBeacon nav1;
	private RadioNavBeacon nav2;
	private RadioNavigationObject nav1_object;
	private RadioNavigationObject nav2_object;
	private NavigationRadio nav1_radio;
	private NavigationRadio nav2_radio;
	private NavigationObject origin;
	private NavigationObject dest;
	private NavigationObject origin_runway;
	@SuppressWarnings("unused")
	private NavigationObject dest_runway;

	private String map_range;
	private String max_range_str;
	private String tunedVOR1dme = "";
	private String tunedVOR2dme = "";
	private String tunedVOR1 = "";
	private String tunedVOR2 = "";
	private String tunedDME1 = "";
	private String tunedDME2 = "";

	private boolean app_mode = false;
	private boolean app_ctr_mode = false;
	private boolean vor_mode = false;
	private boolean vor_ctr_mode = false;
	private boolean map_mode = false;
	private boolean map_ctr_mode = false;
	private boolean pln_mode = false;
	private boolean isMapCenter = false;
	private boolean navFreqDisagree = false;
	private boolean vorTo = false;
	private boolean terrain_invert = false;
	private boolean weather_invert = false;
	private boolean initial_sweep_required = false;
	private boolean egpws_loaded = false;

	private Arc2D map_clip = new Arc2D.Float();
	private Arc2D terrain_clip = new Arc2D.Float();
	private Arc2D terrain_sweep_clip = new Arc2D.Float();
	private Arc2D weather_clip = new Arc2D.Float();
	private Arc2D weather_sweep_clip = new Arc2D.Float();
	private Arc2D compass_arc1 = new Arc2D.Float();
	private Arc2D range_arc1 = new Arc2D.Float();
	private Arc2D range_arc2 = new Arc2D.Float();
	private Arc2D range_arc3 = new Arc2D.Float();
	private Arc2D pln_mode_circle1 = new Arc2D.Float();
	private Arc2D pln_mode_circle2 = new Arc2D.Float();
	private Arc2D altRangeArc = new Arc2D.Float();

	ArrayList<Point2D> waypoints = new ArrayList<Point2D>();
	ArrayList<String> waypoint_names = new ArrayList<String>();
	ArrayList<Point2D> leg_start = new ArrayList<Point2D>();
	ArrayList<Point2D> leg_end = new ArrayList<Point2D>();
	ArrayList<Point2D> mod_waypoints = new ArrayList<Point2D>();
	ArrayList<String> mod_waypoint_names = new ArrayList<String>();
	ArrayList<Point2D> mod_leg_start = new ArrayList<Point2D>();
	ArrayList<Point2D> mod_leg_end = new ArrayList<Point2D>();

	QuadCurve2D q = new QuadCurve2D.Float();

	private float[] heading_line_dash = {20f,50f};
	private Stroke heading_bug_line = new BasicStroke(5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, heading_line_dash, 0.0f);
	@SuppressWarnings("unused")
	private Stroke stroke2_5 = new BasicStroke(2.5f);
	private Stroke stroke3 = new BasicStroke(3f);
	private Stroke stroke4 = new BasicStroke(4f);
	private Stroke stroke5 = new BasicStroke(5f);
	@SuppressWarnings("unused")
	private Stroke compassBgStroke = new BasicStroke(85f);
	


	NDvariableRenderLoop renderLoop;

	public ND2(ModelFactory model_factory, String title, String pilot) {
		super(model_factory, title, pilot);
		this.nor = NavigationObjectRepository.get_instance();
		this.map_projection = new AzimuthalEquidistantProjection(this.pilot);
		terrain = new RenderTerrain2(this.xpd, this.pilot);
		weather = new RenderWeather(this.xpd, this.pilot);
		if(ZHSIStatus.egpws_db_status.equals(ZHSIStatus.STATUS_EGPWS_DB_LOADED) || ZHSIStatus.egpws_db_status.equals(ZHSIStatus.STATUS_EGPWS_PART_LOADED)) {
			egpws_loaded = true;
		}else {
			egpws_loaded = false;
		}
		//renderLoop = new NDvariableRenderLoop(this.model_instance, this, pilot);
	
		
//		//draw images
//		//
//		//airport symbol
//		arpt_symbol.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		arpt_symbol.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//				RenderingHints.VALUE_ANTIALIAS_ON);
//		arpt_symbol.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
//				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//		arpt_symbol.setStroke(new BasicStroke(4f));
//		arpt_symbol.setColor(gc.color_navaid);
//		arpt_symbol.draw(new Ellipse2D.Float(4f,4f,34f,34f));
//		//
//		//airport fix symbol
//		arpt_symbol_fix.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		arpt_symbol_fix.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//				RenderingHints.VALUE_ANTIALIAS_ON);
//		arpt_symbol_fix.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
//				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//		arpt_symbol_fix.setStroke(new BasicStroke(4f));
//		arpt_symbol_fix.setColor(gc.color_lime);
//		arpt_symbol_fix.draw(new Ellipse2D.Float(15f,15f,34f,34f));
//		arpt_symbol_fix.draw(new Ellipse2D.Float(2f,2f,60f,60f));
//		//
//		//Fix symbol
//		//
//		fix_symbol.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		fix_symbol.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//				RenderingHints.VALUE_ANTIALIAS_ON);
//		fix_symbol.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
//				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//		fix_symbol.setStroke(new BasicStroke(4f));
//		fix_symbol.setColor(gc.color_navaid);
//		int[] fix_xpoints = {4, 20, 36};
//		int[] fix_ypoints = {36, 4, 36};
//		fix_symbol.setStroke(new BasicStroke(4f));
//		fix_symbol.drawPolygon(fix_xpoints, fix_ypoints, 3);
//		//
//		//Fix symbol (fix)
//		//
//		fix_symbol_fix.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//		fix_symbol_fix.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//				RenderingHints.VALUE_ANTIALIAS_ON);
//		fix_symbol_fix.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
//				RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//		fix_symbol_fix.setStroke(new BasicStroke(4f));
//		fix_symbol_fix.setColor(gc.color_lime);
//		int[] fix_xpoints_fix = {16, 32, 48};
//		int[] fix_ypoints_fix = {48, 12, 48};
//		fix_symbol_fix.setStroke(new BasicStroke(4f));
//		fix_symbol_fix.drawPolygon(fix_xpoints_fix, fix_ypoints_fix, 3);
//		fix_symbol_fix.draw(new Ellipse2D.Float(2f,2f,60f,60f));
//		//
		
	}

	public void drawInstrument(Graphics2D g2) {
		

		updateStuff();
	
		
		if(this.xpd.power_on()) {
			

			if ((this.pilot == "cpt" && this.xpd.power_on()) || (this.pilot == "fo" && this.xpd.dc_standby_on())) {

				if(this.xpd.irs_aligned()) {


					if(this.map_mode || this.map_ctr_mode || this.vor_mode || this.app_mode) {
						drawTerrain();
					}


					if(this.xpd.efis_wxr_on(pilot) && ZHSIStatus.weather_receiving) {
						drawWeather();
					}

					drawMap();
					
					//drawMap2();
					
					showTunedNavAids();
				}


				//track line above map, but below FMC route
				drawTrackLine();

				if(this.map_mode || this.map_ctr_mode || this.pln_mode) {
					
		
					if (this.xpd.vnav_idx() >= 2) {
						
						drawActiceRoute();
					}
					
					if((this.xpd.legs_mod_active() && this.xpd.vnav_idx() >= 2) || !this.xpd.fpln_active() && this.xpd.vnav_idx() >= 2) {
						drawModRoute();
					}
					
					//drawActiceRoute2();

				}
				
				displayRunways();

				if(this.xpd.tcas_show(pilot)) {
					displayTCASTraffic();
				}

				if(this.xpd.nd_vert_path() && (this.map_mode || this.map_ctr_mode)) {
		
					displayNdVnavPath();
				}

				if(this.xpd.irs_aligned()) {
					drawTopLeft();

					drawTopRight();
				}
				drawCompassRose();

				if(this.xpd.irs_aligned()) {
					drawVorAdf();
				}
				
				if(this.max_range <= 80 && (this.map_mode || this.map_ctr_mode || this.vor_mode || this.app_mode) && this.xpd.tcas_on(pilot)) {
					drawTcasRings();
				}
				
				
				

				drawForeGround();
				
				if(this.xpd.efis_vsd_map(pilot)) {
					//drawVSD();
				}
				
				
				
//				NavigationObject temp = nor.get_runway(this.xpd.dest_arpt(), "32R", dest.lat, dest.lon, false);
//				Runway rwy = (Runway)temp;
//				System.out.println(rwy);
//				System.out.println(rwy.lat1);
//				System.out.println(rwy.lon1);
				
//				origin_runway = nor.get_runway(this.xpd.origin_arpt(), this.xpd.ils_runway0(), origin.lat, origin.lon, false);
//				Runway takeoff_rwy = (Runway)origin_runway;
//				if(this.xpd.ils_runway0().equals(takeoff_rwy.rwy_num1)) {
//					
//				}else {
//					
//				}
				

			}
		}
	}

	private void drawTcasRings() {
		
		
		g2.scale(gc.scalex, gc.scaley);
		g2.setColor(rs.color_markings);
		g2.setStroke(rs.stroke2_5);
		g2.setTransform(original_trans);
		
		int smallCircleSize = 0;
		int bigCircleSize = 0;
		
		if(this.max_range == 80) {
			bigCircleSize = (int) (0.53f * this.pixels_per_nm);
			smallCircleSize =  (int) (0.5f * this.pixels_per_nm);
		}else if(this.max_range == 40) {
			bigCircleSize = (int) (0.8f * this.pixels_per_nm);
			smallCircleSize =  (int) (0.55f * this.pixels_per_nm);
		}else if(this.max_range == 20) {
			bigCircleSize = (int) (0.5f * this.pixels_per_nm);
			smallCircleSize =  (int) (0.35f * this.pixels_per_nm);
		}else if(this.max_range == 10) {
			bigCircleSize = (int) (0.3f * this.pixels_per_nm);
			smallCircleSize =  (int) (0.2f * this.pixels_per_nm);
		}else {
			bigCircleSize = (int) (0.15f * this.pixels_per_nm);
			smallCircleSize =  (int) (0.095f * this.pixels_per_nm);
		}
		
		g2.translate(this.map_center_x, this.map_center_y);
		g2.rotate(Math.toRadians(current_heading_bug_rotate), 0, 0);



		for(int i = 0; i < 360; i += 30) {
			
			if(i % 90 == 0) {
				g2.fillOval(bigCircleSize / -2,  (int)(-3f * this.pixels_per_nm)+ (bigCircleSize / -2), bigCircleSize, bigCircleSize);
			}else {
				g2.fillOval(smallCircleSize / -2,  (int)(-3f * this.pixels_per_nm)+ (smallCircleSize / -2), smallCircleSize, smallCircleSize);
			}
			g2.rotate(Math.toRadians(30f), 0,0);
		}
		
		g2.setTransform(original_trans);
		
	}
	
	@SuppressWarnings("unused")
	private void drawVSD() {
		
		g2.scale(gc.scalex, gc.scaley);
		
		//background
		g2.setStroke(rs.stroke6);
		g2.translate(536, 650);
		g2.setColor(Color.BLACK);
		g2.fillRect(-370, 0, 740, 350);
		g2.setColor(rs.color_instrument_gray);
		g2.drawRect(-370, 0, 740, 350);
		g2.fillRect(-370, 50, 150, 300);
		g2.fillRect(-370, 290, 740, 60);
		
		//mcp alt
		g2.setColor(rs.color_magenta);
		g2.drawString(gc.df3.format(this.xpd.mcp_alt() % 1000), -300, 40);
		int ap1000 = this.xpd.mcp_alt() / 1000;
		if ( ap1000 > 9 ) {
			g2.drawString("" + ap1000, -338, 40);
		}else {
			g2.drawString("" + ap1000, -320, 40);
		}
		
		//bottom scale
		g2.setStroke(rs.stroke4);
		g2.setColor(rs.color_markings);
		g2.drawLine(-120, 295, -120, 315);
		g2.drawLine(-5, 295, -5, 305);
		g2.drawLine(110, 295, 110, 315);
		g2.drawLine(225, 295, 225, 305);
		g2.drawLine(340, 295, 340, 315);
		
		
//		//plane
//		int plane_x[] = {-210, -210, -160};
//		int plane_y[] = {165, 140, 165};
//		g2.drawPolygon(plane_x, plane_y, 3);
		
		g2.setTransform(original_trans);
		
		
		//draw bottom scale text
		gc.drawText2("0", 416, 55f, 26f, 0, rs.color_markings, false, "center", g2);
		gc.drawText2(this.map_range, 646, 55f, 26f, 0, rs.color_markings, false, "center", g2);
		gc.drawText2(this.max_range_str, 876, 55f, 26f, 0, rs.color_markings, false, "center", g2);
		
		g2.scale(gc.scalex, gc.scaley);
		

		float alt_tape = Math.round(this.xpd.altitude(pilot) + 0f);
		

		int scale = 1000;
		float factor = 2000;
		int line_mod = 250;
		int line_mod_value = 0;
		int mod = 500;
		int mod_value = 0;

		
		if(this.max_range == 5) {
			scale = 2000;
			factor = 0.08f;
			mod = 1000;
			mod_value = 0;
			line_mod = 500;
			line_mod_value = 0;

		}else if (this.max_range == 10) {
			scale = 4000;
			factor = 0.04f;
			mod = 2000;
			mod_value = 0;
			line_mod = 1000;
			line_mod_value = 0;

		}else if(this.max_range == 20) {
			scale = 12000;
			factor = 20000;
			mod = 4000;
			mod_value = 0;
			line_mod = 2000;
			line_mod_value = 0;

		}else if(this.max_range == 40) {
			scale = 20000;
			factor = 35000;
			mod = 8000;
			mod_value = 0;
			line_mod = 4000;
			line_mod_value = 0;

		}
		

		
		int alt_bug_y = (int)(780f - Math.round((this.xpd.mcp_alt() - this.xpd.altitude(pilot))) * factor);

		if(alt_bug_y < 695) {
			alt_bug_y = 695;
		}else if(alt_bug_y > 939) {
			alt_bug_y = 939;
		}
		
		//g2.clipRect(160, 700, 745, 235);
		//g2.drawRect(160, 700, 745, 235);
		
		//mcp alt bug
		g2.setStroke(rs.stroke4);
		g2.setColor(rs.color_magenta);
		//bug
		int[] bug_x = {305, 320, 320, 305, 320};
		int[] bug_y = {alt_bug_y - 15, alt_bug_y - 15, alt_bug_y + 15,alt_bug_y + 15,alt_bug_y};
		g2.drawPolygon(bug_x, bug_y, 5);
		
		//bug line
		g2.setStroke(rs.stroke5dash1530);
		g2.drawLine(320, alt_bug_y, 900, alt_bug_y);

		int alt_plane_y = (int)(780f - Math.round((this.xpd.altitude(pilot))) * factor);
		
		
		int plane_x[] = {350, 350, 415};
		int plane_y[] = {(int)alt_plane_y, (int)(alt_plane_y - 25), (int)alt_plane_y};
		g2.setColor(Color.BLACK);
		g2.fillPolygon(plane_x, plane_y, 3);
		g2.setStroke(rs.stroke4);
		g2.setColor(rs.color_markings);
		g2.drawPolygon(plane_x, plane_y, 3);
		
//		//mcp vvi
//		if(this.xpd.vs_mode()) {
//			g2.setColor(rs.color_magenta);
//			g2.setStroke(rs.stroke5dash10);
//			g2.rotate(Math.toRadians(this.xpd.mcp_vvi() / -90f), 430, plane_height);
//			g2.drawLine(430, (int)plane_height, 900, (int)plane_height);
//		}
		g2.setTransform(original_trans);
//		
//		g2.scale(gc.scalex, gc.scaley);
		g2.setStroke(rs.stroke4);
		g2.setColor(rs.color_markings);
//		
//		g2.rotate(Math.toRadians(this.xpd.vvi(pilot) / -90f), 430, plane_height);
//		g2.drawLine(430, (int)plane_height, 650, (int)plane_height);
//
//		g2.setTransform(original_trans);
		
		g2.scale(gc.scalex, gc.scaley);
		
		

		int alt100 = Math.round(alt_tape / 100.0f) * 100;
		

		for (int alt_mark = alt100 - scale; alt_mark <= alt100 + scale; alt_mark += 50) {
			
			int alt_y = (int) (780f - Math.round(((float) alt_mark - Math.round(this.xpd.altitude(pilot))) * factor));
			
			if(alt_mark >= this.xpd.radio_alt_feet()) {
				if(alt_mark % mod == mod_value) {
					g2.drawLine(290, alt_y, 315, alt_y);
				}
				
				if(alt_mark % line_mod == line_mod_value) {
					g2.drawLine(305, alt_y, 315, alt_y);
				}

				if(alt_mark % mod == mod_value) {
					
					if(alt_mark >= 10000) {
						g2.drawString("" + Math.abs(alt_mark), 195f, alt_y + 9f);
					}else if(alt_mark >= 1000) {
						g2.drawString("" + Math.abs(alt_mark), 210f, alt_y + 9f);
					}else if(alt_mark > 100) {
						g2.drawString("" + Math.abs(alt_mark), 230f, alt_y + 9f);
					}else {
						g2.drawString("" + Math.abs(alt_mark), 260f, alt_y + 9f);
					}
					
				}
			}
		}

		g2.setTransform(original_trans);
		g2.setClip(original_clipshape);
		
	}

	private void displayRunways() {
		
		g2.clip(map_clip);
		///
		main_map = g2.getTransform();
		///

		if(this.xpd.ils_show(pilot)) {
			drawRunway(this.xpd.ils_runway(), this.xpd.ils_x(pilot), this.xpd.ils_y(pilot), this.xpd.ils_rotate(pilot));
		}
		
		if(this.xpd.ils_show0(pilot)) {		
			drawRunway(this.xpd.ils_runway0(), this.xpd.ils_x0(pilot), this.xpd.ils_y0(pilot), this.xpd.ils_rotate0(pilot));	
		}
		
		///
		g2.setTransform(main_map);
		///
		g2.setClip(original_clipshape);
		
	}

	private void drawRunway(String rwy_number, float x, float y, float rotation) {
		
		
		float rwy0_x = zibo_x(x);
		float rwy0_y = zibo_y(y);
		g2.setStroke(gc.stroke_four);
		g2.setColor(gc.color_markings);
		g2.rotate(Math.toRadians(rotation),rwy0_x, rwy0_y);
		g2.drawLine((int)rwy0_x, (int)(rwy0_y - (10 * gc.scaley)), (int)(rwy0_x - (85 * gc.scalex)), (int)(rwy0_y - (10 * gc.scaley)));
		g2.drawLine((int)rwy0_x, (int)(rwy0_y + (10 * gc.scaley)), (int)(rwy0_x - (85 * gc.scalex)), (int)(rwy0_y + (10 * gc.scaley)));
		//centerline
		//g2.drawLine((int)rwy0_x, (int)(rwy0_y - 3f * gc.scaley), (int)rwy0_x, (int)(rwy0_y - pixels_per_nm * 7.1f));
		//g2.drawLine((int)rwy0_x, (int)(rwy0_y + 3f * gc.scaley), (int)rwy0_x, (int)(rwy0_y + pixels_per_nm * 7.1f));
		g2.setStroke(gc.runway_center_line);
		g2.drawLine((int)(rwy0_x - (85 * gc.scalex)), (int)(rwy0_y), (int)(rwy0_x - (85 * gc.scalex)  - pixels_per_nm * 7.1f), (int)rwy0_y);
		g2.drawLine((int)(rwy0_x), (int)(rwy0_y), (int)(rwy0_x  + pixels_per_nm * 7.1f), (int)rwy0_y);
		//
		// runway number
		//
		g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
		g2.drawString(rwy_number, rwy0_x - (180f * gc.scalex), rwy0_y - (12f * gc.scaley));
		g2.setTransform(original_trans);
		
	}

	private void displayNdVnavPath() {
		
		
		g2.scale(gc.scalex, gc.scaley);
		//set background
		g2.setColor(Color.BLACK);
//		int[] vnav_path_xpoints = {1075, 972, 1075};
//		int[] vnav_path_ypoints = {662, 662, 630};                     
		g2.fillPolygon(vnav_path_xpoints, vnav_path_ypoints, vnav_path_xpoints.length);
		g2.fillRect(972, 660, 105, 275);
		g2.setColor(gc.color_markings);
		g2.setStroke(stroke4);                                                                             
		//draw scale
		g2.drawLine(1015, 670, 1015, 880);
		//top horizontal
		g2.drawLine(990, 670, 1015, 670);
		//middle
		g2.drawLine(980, 775, 1015, 775);
		// bottom horizontal
		g2.drawLine(990, 880, 1015, 880);
		
		g2.setTransform(original_trans);
		//
		//text above
		if(this.xpd.vnav_err() > 400) {
			gc.drawText2("" + (int)Math.abs(this.xpd.vnav_err()), 1020, 400, 28, 0, gc.color_markings, true, "right", g2);	  
		}
		
		
		//text below
		if(this.xpd.vnav_err() < -400) {
			gc.drawText2("" + (int)Math.abs(this.xpd.vnav_err()), 1020, 135, 28, 0, gc.color_markings, true, "right", g2);
		}
		
		
		//diamond
		float ydelta = (210 * this.xpd.vnav_err()) / 800f;

		if(ydelta > 105) {
			ydelta = 105;
		}else if(ydelta < -105) {
			ydelta = -105;
		}
		gc.displayImage(rs.img_hdef_ind, 0, 1015 , (278 - rs.img_hdef_ind.getHeight() / 2) + ydelta, g2);
		
	}

	private void displayTCASTraffic() {

		g2.clip(map_clip);
		///
		main_map = g2.getTransform();
		///

		for(int i = 0; i < 19; i ++) {
			if(this.xpd.tcas_ai_show(pilot, i)) {

				if(this.xpd.tcas_type_show(pilot, i) == 1.0f) { // red box
					showTraffic(i, Color.RED, rs.img_tcas_redbox, rs.img_tcas_red_arrowdn, rs.img_tcas_red_arrowup);
				}
				if(this.xpd.tcas_type_show(pilot, i) == 2.0f) { // amber circle
					showTraffic(i, gc.color_amber, rs.img_tcas_ambercircle, rs.img_tcas_amber_arrowdn, rs.img_tcas_amber_arrowup);
				}
				if(this.xpd.tcas_type_show(pilot, i) == 3.0f) { // solid white
					showTraffic(i, gc.color_markings,rs.img_tcas_solid_diamond, rs.img_tcas_white_arrowdn, rs.img_tcas_white_arrowup);
				}
				if(this.xpd.tcas_type_show(pilot, i) == 4.0f) {
					showTraffic(i, gc.color_markings, rs.img_tcas_damond, rs.img_tcas_white_arrowdn, rs.img_tcas_white_arrowup);
				}
			}
		}

		///
		g2.setTransform(main_map);
		///
		g2.setClip(original_clipshape);
	}

	private void showTraffic(int i, Color color, BufferedImage image, BufferedImage downArrow, BufferedImage upArrow) {

		g2.setColor(color);
		String altString;
		if(pilot == "cpt") {
			altString = this.xpd.tcas_alt(i);
		}else {
			altString = this.xpd.tcas_alt_fo(i);
		}
//		float x = getXpx(this.xpd.tcas_x(pilot, i), 0, 0);
//		float y = getYpx(this.xpd.tcas_y(pilot, i), 0, 0);
		float x = zibo_x(this.xpd.tcas_x(pilot, i));
		float y = zibo_y(this.xpd.tcas_y(pilot, i));


		g2.translate(x, y);

		g2.drawImage(image, (int)(0 - image.getWidth() / 2), (int)(0 - image.getHeight() / 2), (int)(image.getWidth() * gc.scaling_factor), (int)(image.getHeight() * gc.scaling_factor), null);

		if(this.xpd.tcas_arrow_dn_up_show(pilot, i) == 1) { //arrow down 
			g2.drawImage(downArrow, (int)(0 + 20f * gc.scaling_factor), (int)(0 - downArrow.getHeight() / 2), (int)(downArrow.getWidth() * gc.scaling_factor), (int)(downArrow.getHeight() * gc.scaling_factor), null);
		}
		if(this.xpd.tcas_arrow_dn_up_show(pilot, i) == 2) { //up down  {
			g2.drawImage(upArrow, (int)(0 + 20f * gc.scaling_factor), (int)(0 - upArrow.getHeight() / 2), (int)(upArrow.getWidth() * gc.scaling_factor), (int)(upArrow.getHeight() * gc.scaling_factor), null);
		}
		

		g2.setFont(rs.glassFont.deriveFont(28f * gc.scaling_factor));


		if(altString != null) {
			if(this.xpd.tcas_alt_dn_up_show(pilot, i) == 0) { // underneath
				g2.drawString(altString, 0 - 40f * gc.scaling_factor, 0 + 40f * gc.scaling_factor);
			}else {
				g2.drawString(altString, 0 - 40f * gc.scaling_factor, 0 - 35f * gc.scaling_factor);
			}
		}
		g2.setTransform(original_trans);

	}

	private void drawActiceRoute() {
		

		drawRoute(true, this.xpd.active_num_of_wpts(), this.xpd.active_waypoints(), this.xpd.legs_lat(), this.xpd.legs_lon(), this.xpd.legs_type(), this.xpd.legs_alt_calc(), this.xpd.legs_alt_rest1(), this.xpd.legs_alt_rest2(), this.xpd.legs_alt_rest_type());
		
//		g2.clip(map_clip);
//		///
//		main_map = g2.getTransform();
//		///
//		g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
//		
//		//draw route
//
//		if(this.xpd.active_num_of_wpts() > 2) {
//			for(int i = 0; i < this.xpd.active_num_of_wpts() - 1; i++) {
//				float lat_start = this.xpd.legs_lat()[i];
//				float lon_start = this.xpd.legs_lon()[i];
//				map_projection.setPoint(lat_start, lon_start);
//				int x = (int) map_projection.getX();
//				int y = (int) map_projection.getY();
//				float lat_end = this.xpd.legs_lat()[i + 1];
//				float lon_end = this.xpd.legs_lon()[i + 1];
//				map_projection.setPoint(lat_end, lon_end);
//				int xx = (int) map_projection.getX();
//				int yy = (int) map_projection.getY();
//
//				//set color and stroke
//				if(this.xpd.fpln_active()) {
//					g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
//					g2.setColor(gc.color_magenta);
//
//					if(i >= this.xpd.missed_app_wpt_idx() - 2 && this.xpd.missed_app_active()) {
//						g2.setStroke(gc.inactive_route_dashes);
//						g2.setColor(gc.color_navaid);
//					}
//				}else {
//					g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
//					g2.setColor(gc.color_markings);
//				}			
//				g2.drawLine(x, y, xx, yy);	
//			}
//		}
		
		//draw waypoints

//		if(this.xpd.active_num_of_wpts() > 1) {
//			for(int i = 0; i < this.xpd.active_num_of_wpts(); i++) {
//				AffineTransform fms_trans = g2.getTransform();
//				if(this.xpd.active_waypoints()[i] != null) {
//					String wpt = this.xpd.active_waypoints()[i];
//					float lat = this.xpd.legs_lat()[i];
//					float lon = this.xpd.legs_lon()[i];
//					float alt_rest1 = this.xpd.legs_alt_rest1()[i];
//					float alt_rest2 = this.xpd.legs_alt_rest2()[i];
//					//System.out.println(wpt + " lat=" + lat + " lon=" + lon + " alt_rest1=" + alt_rest1 + " alt_rest2=" + alt_rest2);
//					map_projection.setPoint(lat, lon);
//					int x = (int) map_projection.getX();
//					int y = (int) map_projection.getY();
//					g2.rotate(Math.toRadians(this.map_up * -1), x, y);
//					gc.displayNavAid(rs.img_magenta_waypoint, x, y, g2);
//				}
//				g2.setTransform(fms_trans);
//			}
//		}

		
//		///
//		g2.setTransform(main_map);
//		///
//		g2.setClip(original_clipshape);

//		waypoints.clear();
//		waypoint_names.clear();
//		leg_start.clear();
//		leg_end.clear();
//
//		for(int i = 0; i < this.xpd.fms_legs_num(); i++) {
//
//			if(FMS.active_legs[i].startName != "") {
//				map_projection.setPoint(FMS.active_legs[i].getStartLat(), FMS.active_legs[i].getStartLon());
//				int x = (int) map_projection.getX();
//				int y = (int) map_projection.getY();
//
//				if(FMS.active_legs[i].endName == this.xpd.dest_arpt()) {
//					map_projection.setPoint(FMS.active_legs[i].getStartLat(), FMS.active_legs[i].getStartLon());
//				}else {
//					map_projection.setPoint(FMS.active_legs[i].getEndLat(), FMS.active_legs[i].getEndLon());
//				}
//
//
//				int x1 = (int) map_projection.getX();
//				int y1 = (int) map_projection.getY();
//
//				float ratio = 0.0f;
//
//				ratio = 0.2f;
//				x = (int) (ratio*x1 + (1.0 - ratio)*x);
//				y = (int) (ratio*y1 + (1.0 - ratio)*y);
//				ratio = 0.8f;
//				x1 = (int) (ratio*x1 + (1.0 - ratio)*x);
//				y1 = (int) (ratio*y1 + (1.0 - ratio)*y);
//
//				map_projection.setPoint(FMS.active_entries[i].getLat(), FMS.active_entries[i].getLon());
//				int x2 = (int) map_projection.getX();
//				int y2 = (int) map_projection.getY();
//
//				Point2D waypoint = new Point2D.Float(x2, y2);
//				waypoints.add(waypoint);
//				String waypoint_name = FMS.active_entries[i].getName();
//				waypoint_names.add(waypoint_name);
//				Point2D legStart = new Point2D.Float(x, y);
//				leg_start.add(legStart);
//				Point2D legEnd = new Point2D.Float(x1, y1);
//				leg_end.add(legEnd);
//
//			}
//		}
//
//		g2.clip(map_clip);
//		///
//		main_map = g2.getTransform();
//		///
//		g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
//
//		// draw legs
//
//		for(int i = 1; i < waypoint_names.size() - 1; i++) {
//
//			int leg_start_x = (int) leg_start.get(i).getX();
//			int leg_start_y = (int) leg_start.get(i).getY();
//
//			if(waypoint_names.get(i - 1).equals(this.xpd.origin_arpt())) {
//
//				leg_start_x = (int) waypoints.get(i).getX();
//				leg_start_y = (int) waypoints.get(i).getY();
//			}
//
//			if(waypoint_names.get(i - 1).equals("VECTOR") || waypoint_names.get(i - 1).equals("DISCONTINUITY")) {
//
//				leg_start_x = (int) waypoints.get(i).getX();
//				leg_start_y = (int) waypoints.get(i).getY();
//			}
//
//			int leg_end_x = (int) leg_end.get(i).getX();
//			int leg_end_y = (int) leg_end.get(i).getY();
//
//			if(waypoint_names.get(i + 1).equals(this.xpd.dest_arpt())) {
//
//				leg_end_x = (int) waypoints.get(i).getX();
//				leg_end_y = (int) waypoints.get(i).getY();
//			}
//			if(waypoint_names.get(i + 1).equals("VECTOR") || waypoint_names.get(i + 1).equals("DISCONTINUITY")) {
//
//				leg_end_x = (int) waypoints.get(i - 1).getX();
//				leg_end_y = (int) waypoints.get(i - 1).getY();
//			}
//
//
//			int curve_start_x = (int) leg_end.get(i).getX();
//			int curve_start_y = (int) leg_end.get(i).getY();
//
//			int curve_end_x = (int) leg_start.get(i + 1).getX();
//			int curve_end_y = (int) leg_start.get(i + 1).getY();
//
//
//			if(waypoint_names.get(i + 1).equals("VECTOR") || waypoint_names.get(i + 1).equals("DISCONTINUITY")) {
//
//
//				curve_end_x = (int) waypoints.get(i).getX();
//				curve_end_y = (int) waypoints.get(i).getY();
//			}
//
//			int control_p_x = (int) waypoints.get(i + 1).getX();
//			int control_p_y = (int) waypoints.get(i + 1).getY();
//
//			if(this.xpd.fpln_active()) {
//				g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
//				g2.setColor(gc.color_magenta);
//
//				if(i >= this.xpd.missed_app_wpt_idx() - 2 && this.xpd.missed_app_active()) {
//					g2.setStroke(gc.inactive_route_dashes);
//					g2.setColor(gc.color_navaid);
//				}
//			}else {
//				g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
//				g2.setColor(gc.color_markings);
//			}
//
//
//			if(!(FMS.active_entries[i].getName().equals(this.xpd.origin_arpt()) || 
//					FMS.active_entries[i].getName().equals(this.xpd.dest_arpt()) || 
//					FMS.active_entries[i].getName().equals("VECTOR") || 
//					FMS.active_entries[i].getName().equals("DISCONTINUITY"))) {
//				g2.drawLine(leg_start_x, leg_start_y, leg_end_x, leg_end_y);
//				q.setCurve(curve_start_x, curve_start_y, control_p_x, control_p_y, curve_end_x, curve_end_y);
//				g2.draw(q);
//			}
//
//		}
//
//		// draw waypoints
//
//		for(int i = 0; i < waypoints.size(); i++) {
//
//			int wpt_x = (int) waypoints.get(i).getX();
//			int wpt_y = (int) waypoints.get(i).getY();
//
//
//			AffineTransform fms_trans = g2.getTransform();
//			if(!(FMS.active_entries[i].getName().equals(this.xpd.origin_arpt()) || 
//					FMS.active_entries[i].getName().equals(this.xpd.dest_arpt()) || 
//					FMS.active_entries[i].getName().equals("VECTOR") || 
//					FMS.active_entries[i].getName().equals("DISCONTINUITY"))) {
//				g2.rotate(Math.toRadians(this.map_up * -1), wpt_x, wpt_y);
//				if(FMS.active_entries[i].getName().equals(xpd.active_waypoint())) {
//					g2.setColor(gc.color_magenta);
//					if(FMS.active_entries[i].getType() == 0.0f) {
//						gc.displayNavAid(rs.img_magenta_waypoint, wpt_x, wpt_y, g2);
//					}
//					if(FMS.active_entries[i].getType()  == 5.0f) {
//						gc.displayNavAid(rs.img_magenta_flyover_waypoint, wpt_x, wpt_y, g2);
//					}
//				}else {
//					g2.setColor(gc.color_markings);
//					if(FMS.active_entries[i].getType() == 0.0f) {
//						gc.displayNavAid(rs.img_white_waypoint, wpt_x, wpt_y, g2);
//					}
//					if(FMS.active_entries[i].getType()  == 5.0f) {
//						gc.displayNavAid(rs.img_white_flyover_waypoint, wpt_x, wpt_y, g2);
//					}
//				}
//			}
//
//			int alt_rest = (int)FMS.active_entries[i].getAlt_rest();
//			int alt_rest2 = (int)FMS.active_entries[i].getAlt_rest_2();
//			g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
//			if(!(FMS.active_entries[i].getName().equals(this.xpd.origin_arpt()) || 
//					FMS.active_entries[i].getName().equals(this.xpd.dest_arpt()) || 
//					FMS.active_entries[i].getName().equals("VECTOR") || 
//					FMS.active_entries[i].getName().equals("DISCONTINUITY"))) {
//				g2.drawString(FMS.active_entries[i].getName(), wpt_x + (30f * gc.scaling_factor), wpt_y + (30f * gc.scaling_factor));
//				if(this.xpd.efis_data_on(pilot)) {
//					g2.setFont(rs.glassFont.deriveFont(22f * gc.scaling_factor));
//					if(alt_rest != 0) {
//						if(FMS.active_entries[i].getAlt_rest_type() == 0.0f) { //  at
//							g2.drawString("" + alt_rest, wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}else if(FMS.active_entries[i].getAlt_rest_type() == 1.0f) { // below
//							g2.drawString("" + alt_rest + "B", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}else if(FMS.active_entries[i].getAlt_rest_type() == 2.0f) { // above
//							g2.drawString("" + alt_rest + "A", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}else if(FMS.active_entries[i].getAlt_rest_type() == 3.0f) { // between
//							g2.drawString("" + alt_rest + "A", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//							g2.drawString("" + alt_rest2 + "B", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));
//						}else if(FMS.active_entries[i].getAlt_rest_type() == 4.0f) { // destination runway
//							g2.drawString("" + alt_rest, wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}
//					}
//				}
//			}
//
//			g2.setTransform(fms_trans);
//		}
//		///
//		g2.setTransform(main_map);
//		///
//		g2.setClip(original_clipshape);
	}

	private void drawModRoute() {
		

		drawRoute(false, this.xpd.mod_num_of_wpts(), this.xpd.mod_waypoints(), this.xpd.mod_legs_lat(), this.xpd.mod_legs_lon(), this.xpd.mod_legs_type(), this.xpd.mod_legs_alt_calc(), this.xpd.mod_legs_alt_rest1(), this.xpd.mod_legs_alt_rest2(), this.xpd.mod_legs_alt_rest_type());

	}
	
	private void drawRoute(boolean isActive, int num_waypoints, String[] wpts, float[] lats, float[] lons, float[] type, float[] alt_calc, float[] alt_rest1, float[] alt_rest2, float[] alt_rest_type) {

		

		g2.clip(map_clip);
		
		
		///
		// draw fix
		
		
		
		//draw holds
		for(int i = 0; i < 5; i++) {
			
	
			if(this.xpd.show_hold(this.pilot, i)) {
				int hold_type = this.xpd.hold_type(this.pilot, i);
				int hold_crs = this.xpd.hold_crs(this.pilot, i);
				float hold_dist = this.xpd.hold_dist(this.pilot, i);
				float hold_x = this.xpd.hold_x(this.pilot, i);
				float hold_y = this.xpd.hold_y(this.pilot, i);
				if(hold_dist > 0) {
					drawHold(hold_type, hold_crs, hold_dist, hold_x, hold_y);
				}
				
			}
		}
		
		main_map = g2.getTransform();
		///
		g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
		
		//draw route

		if(this.xpd.vnav_idx() >= 2) {
			float lat_start;
			float lon_start;
			float lat_end;
			float lon_end;
			@SuppressWarnings("unused")
			boolean isDiscon = false;
			@SuppressWarnings("unused")
			boolean isVector = false;
			
			
			int vnav_index = Math.max(0, this.xpd.vnav_idx() - 1);

			
			for(int i = vnav_index; i < num_waypoints - 1; i++) {
				//checks if there is a departing runway in the route
				if(i < 3 && wpts[i].equals(this.xpd.origin_arpt()) && wpts[i + 1].startsWith("RW")) {
					lat_start = lats[i + 1];
					lon_start = lons[i + 1];
				}else {
					lat_start = lats[i];
					lon_start = lons[i];
				}
				int x;
				int y;

				
				
				map_projection.setPoint(lat_start, lon_start); //get the start of the leg
				x = (int) map_projection.getX();
				y = (int) map_projection.getY();
				
				// do not draw last line to destination airport
				if(wpts[i + 1].equals(this.xpd.dest_arpt())) {
					lat_end = lats[i];
					lon_end = lons[i];
				}else {
					if (wpts[i].equals("DISCONTINUITY")) {
						isDiscon = true;
						lat_end = lats[i];
						lon_end = lons[i];
					}else if(wpts[i].equals("VECTOR")) {
						isVector = true;
						lat_end = lats[i];
						lon_end = lons[i];
					}else {
						isDiscon = false;
						isVector = false;
						lat_end = lats[i + 1];
						lon_end = lons[i + 1];
					}
				}
				
				map_projection.setPoint(lat_end, lon_end); // get the end of the leg
				int xx = (int) map_projection.getX();
				int yy = (int) map_projection.getY();

				if(isActive) {
					if(this.xpd.fpln_active()) {
						g2.setStroke(this.stroke3);
						g2.setColor(gc.color_magenta);

						if(i >= this.xpd.missed_app_wpt_idx() - 2 && this.xpd.missed_app_wpt_idx() > 1) {
							//g2.setStroke(gc.inactive_route_dashes);
							float[] inactive_line_dash = {20f * gc.scaling_factor, 20f * gc.scaling_factor};
							g2.setStroke(new BasicStroke(4f * gc.scaling_factor, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 20.0f * gc.scaling_factor, inactive_line_dash, 0.0f));
							g2.setColor(gc.color_navaid);
						}
					}else {
						g2.setStroke(this.stroke3);
						g2.setColor(gc.color_markings);
					}
				}else {
					//set color and stroke
					float[] inactive_line_dash = {20f * gc.scaling_factor, 20f * gc.scaling_factor};
					g2.setStroke(new BasicStroke(4f * gc.scaling_factor, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 20.0f * gc.scaling_factor, inactive_line_dash, 0.0f));
		
					if(this.xpd.fpln_active()) {
						g2.setColor(gc.color_markings);
					}else {
						g2.setColor(gc.color_navaid);
					}
				}

				g2.drawLine(x, y, xx, yy);
			}
			
			
			//draw first leg
			
			map_projection.setPoint(lats[this.xpd.vnav_idx() - 2], lons[this.xpd.vnav_idx() - 2]);
			int x = (int) map_projection.getX();
			int y = (int) map_projection.getY();
			map_projection.setPoint(lats[this.xpd.vnav_idx() - 1], lons[this.xpd.vnav_idx() - 1]);
			int xx = (int) map_projection.getX();
			int yy = (int) map_projection.getY();
			

			if(/*wpts[this.xpd.vnav_idx() - 2].startsWith("RW") ||*/ 
					wpts[this.xpd.vnav_idx() - 2].equals("VECTOR") || 
					wpts[this.xpd.vnav_idx() - 2].equals("DISCONTINUITY") ||
					wpts[this.xpd.vnav_idx() - 2].equals(this.xpd.origin_arpt()) ||
					wpts[this.xpd.vnav_idx() - 2].equals(this.xpd.dest_arpt())) {
				xx = x;
				yy = y;
			}
			
			if(!isActive) {
				
				float[] inactive_line_dash = {20f * gc.scaling_factor, 40f * gc.scaling_factor};
				g2.setStroke(new BasicStroke(4f * gc.scaling_factor, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 20.0f * gc.scaling_factor, inactive_line_dash, 0.0f));
				g2.setColor(gc.color_markings);

				if(this.pln_mode) {
					if(wpts[this.xpd.vnav_idx() - 2].startsWith("RW") || 
							wpts[this.xpd.vnav_idx() - 2].equals("VECTOR") || 
							wpts[this.xpd.vnav_idx() - 2].equals("DISCONTINUITY") ||
							wpts[this.xpd.vnav_idx() - 2].equals(this.xpd.origin_arpt()) ||
							wpts[this.xpd.vnav_idx() - 2].equals(this.xpd.dest_arpt())) {
						xx = plane_x;
						yy = plane_y;
					}
					x = plane_x;
					y = plane_y;
					
				}else {
		
					if(wpts[this.xpd.vnav_idx() - 2].startsWith("RW") || 
							wpts[this.xpd.vnav_idx() - 2].equals("VECTOR") || 
							wpts[this.xpd.vnav_idx() - 2].equals("DISCONTINUITY") ||
							wpts[this.xpd.vnav_idx() - 2].equals(this.xpd.origin_arpt()) ||
							wpts[this.xpd.vnav_idx() - 2].equals(this.xpd.dest_arpt())) {
						xx = (int)this.map_center_x;
						yy = (int)this.map_center_y;
					}
					
					x = (int)this.map_center_x;
					y = (int)this.map_center_y;

				}
				if(this.xpd.direct_active() || this.xpd.legs_mod_active()) {
			
					map_projection.setPoint(lats[this.xpd.vnav_idx() - 1], lons[this.xpd.vnav_idx() - 1]);
					xx = (int) map_projection.getX();
					yy = (int) map_projection.getY();
					float track = (this.xpd.rte_edit_rot_act() - 180 + 0) % 360;
					if(!this.pln_mode) {
						track = (this.xpd.rte_edit_rot_act() - 180 + this.xpd.track()) % 360;
					}
					track = (track - 180 - this.xpd.magnetic_variation()) % 360;
		

					float[] latlon = CoordinateSystem.latlon_at_dist_heading(lats[this.xpd.vnav_idx() - 1], lons[this.xpd.vnav_idx() - 1], this.xpd.fpln_nav_id_dist() * 4, track);
					map_projection.setPoint(latlon[0], latlon[1]);
					int xxx = (int) map_projection.getX();
					int yyy = (int) map_projection.getY();
					if(this.pln_mode) {
						g2.drawLine(xxx, yyy, xx, yy);

					}else {
						g2.drawLine(xxx, yyy, xx, yy);
	
					}
				}else {
					
					g2.drawLine(x, y, xx, yy);
				}
				
				
			}else {
				//Active route magenta
				g2.setStroke(this.stroke3);
				g2.setColor(gc.color_magenta);
				
				if(this.xpd.direct_active()) {
					map_projection.setPoint(lats[this.xpd.vnav_idx() - 1], lons[this.xpd.vnav_idx() - 1]);
					xx = (int) map_projection.getX();
					yy = (int) map_projection.getY();
					//get point based on FMS track and distance
					float track = (this.xpd.fms_track() - 180) % 360;
//					if(!this.pln_mode) {
//						track = ((this.xpd.fms_track() - 180) % 360);
//					}
					float[] latlon = CoordinateSystem.latlon_at_dist_heading(lats[this.xpd.vnav_idx() - 1], lons[this.xpd.vnav_idx() - 1], this.xpd.fpln_nav_id_dist(), track);
					map_projection.setPoint(latlon[0], latlon[1]);
					int xxx = (int) map_projection.getX();
					int yyy = (int) map_projection.getY();

					if(this.pln_mode) {
						//TODO: intercept course
						if(this.xpd.fms_track_active()) {
							g2.drawLine(xxx, yyy, xx, yy);
						}else {
							g2.drawLine(plane_x, plane_y, xx, yy);
						}
					}else {
						if (this.xpd.fms_track_active()) {
							g2.drawLine(xxx, yyy, xx, yy);
						}else {
							g2.drawLine((int)this.map_center_x, (int)this.map_center_y, xx, yy);
						}
					}
				}else {

					g2.drawLine(x, y, xx, yy);
				}
			}

			

			
			//draw waypoints
			
			int index = 0;
			if(this.xpd.direct_active()) {
				index = this.xpd.vnav_idx() - 1;
			}else {
				index = this.xpd.vnav_idx() - 2;
			}
			
			for(int i = index; i < num_waypoints; i++) {

				String dep_runway = "";
				@SuppressWarnings("unused")
				String dep_runway_name = "";
				String dest_runway = "";
				@SuppressWarnings("unused")
				String dest_runway_name = "";
				String wpt_name  = "";
				float wpt_lat = 0.0f;
				float wpt_lon = 0.0f;
				float wpt_type = 0.0f;
				@SuppressWarnings("unused")
				boolean isDepRunway = false;
				@SuppressWarnings("unused")
				boolean isDestRunway = false;
				
				wpt_name = wpts[i];
				


				
				if(i > 0 && i < 3 && wpt_name.startsWith("RW")) { //assuming this is the departing runway
					isDepRunway = true;
					dep_runway = wpts[i];
					dep_runway_name = dep_runway.substring(2, dep_runway.length());
				}else {
					isDepRunway = false;
				}
				
				if(i > 3 && i < num_waypoints && wpt_name.startsWith("RW")) { //assuming this is the destination runway
					isDestRunway = true;
					dest_runway = wpts[i];
					dest_runway_name = dest_runway.substring(2, dest_runway.length());

				}else {
					isDestRunway = false;
				}
				wpt_lat = lats[i];
				wpt_lon = lons[i];
				wpt_type = type[i];
				
				
				
				map_projection.setPoint(wpt_lat, wpt_lon);
				int wpt_x = (int) map_projection.getX();
				int wpt_y = (int) map_projection.getY();

				
				fms_trans = g2.getTransform();
				g2.rotate(Math.toRadians(this.map_up * -1), wpt_x, wpt_y);
				
				
				//draw the waypoints
				if(!(wpt_name.equals(this.xpd.origin_arpt()) || wpt_name.equals(this.xpd.dest_arpt()) || wpt_name.equals("DISCONTINUITY") || wpt_name.equals("VECTOR"))) { // do not draw origin/dest 
					if(isActive) {
						if(this.xpd.fpln_active() /* && i > this.xpd.vnav_idx() - 3 */) {
							if(wpt_name.equals(xpd.active_waypoint())) {
								g2.setColor(gc.color_magenta);
								if(wpt_type == 0.0f) {
									gc.displayNavAid(rs.img_magenta_waypoint, wpt_x, wpt_y, g2);
								}
								if(wpt_type  == 5.0f) {
									gc.displayNavAid(rs.img_magenta_flyover_waypoint, wpt_x, wpt_y, g2);
								}
							}else {
								g2.setColor(gc.color_markings);
								if(wpt_type == 0.0f) {
									gc.displayNavAid(rs.img_white_waypoint, wpt_x, wpt_y, g2);
								}
								if(wpt_type  == 5.0f) {
									gc.displayNavAid(rs.img_white_flyover_waypoint, wpt_x, wpt_y, g2);
								}
							}
						}
					}else {
						//if(i > this.xpd.vnav_idx() - 3) {
							g2.setColor(gc.color_markings);
							gc.displayNavAid(rs.img_white_waypoint, wpt_x, wpt_y, g2);
						//}
					}

					if(i > this.xpd.vnav_idx() - 3) {
						
						g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
						g2.drawString(wpt_name, wpt_x + (30f * gc.scaling_factor), wpt_y + (30f * gc.scaling_factor));

						
						
						//draw alt restrictions
						if(alt_rest1[i] != 0 || alt_rest2[i] != 0) {
							if(alt_rest_type[i] == 0.0f) { //  at
								g2.drawString("" + (int)alt_rest1[i], wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
								if(this.xpd.efis_data_on(this.pilot)) {
									//draw eta if available
									g2.drawString("----Z", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));		
								}	
							}else if(alt_rest_type[i] == 1.0f) { // below
								g2.drawString("" + (int)alt_rest1[i] + "B", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
								if(this.xpd.efis_data_on(this.pilot)) {
									//draw eta if available
									g2.drawString("----Z", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));		
								}	
							}else if(alt_rest_type[i] == 2.0f) { // above
								g2.drawString("" + (int)alt_rest1[i] + "A", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
								if(this.xpd.efis_data_on(this.pilot)) {
									//draw eta if available
									g2.drawString("----Z", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));		
								}	
							}else if(alt_rest_type[i] == 3.0f) { // between
								g2.drawString("" + (int)alt_rest2[i] + "B", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
								g2.drawString("" + (int)alt_rest1[i] + "A", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));
								if(this.xpd.efis_data_on(this.pilot)) {
									//draw eta if available
									g2.drawString("----Z", wpt_x + (30 * gc.scaling_factor), wpt_y + (120 * gc.scaling_factor));		
								}	
							}else if(alt_rest_type[i] == 4.0f) { // destination runway
								g2.drawString("" + (int)alt_rest1[i], wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
								if(this.xpd.efis_data_on(this.pilot)) {
									//draw eta if available
									g2.drawString("----Z", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));		
								}	
							}	
						}else{
							if(this.xpd.efis_data_on(this.pilot)) {

								//draw eta if available
								g2.drawString("----Z", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
								
							}	
						}
					}
					
				}
				
//				if(isDepRunway) { //draw departing runway
//					if(this.xpd.ils_show0(this.pilot)) {
//						float dashes[] = { 10.0f, 10.0f };
//						float runway_rotate = (this.xpd.ils_rotate0(pilot) % 360f) - 90f;
//						g2.setStroke(this.stroke2_5);
//						g2.setColor(gc.color_markings);
//						dep_runway_trans = g2.getTransform();
//						g2.rotate(Math.toRadians(runway_rotate), wpt_x, wpt_y);
//						g2.drawLine((int)(wpt_x - 9f * gc.scalex), (int)(wpt_y + 9f * gc.scaley), (int)(wpt_x - 9f * gc.scalex), (int)(wpt_y - 75f * gc.scaley));
//						g2.drawLine((int)(wpt_x + 9f * gc.scalex), (int)(wpt_y + 9f * gc.scaley), (int)(wpt_x + 9f * gc.scalex), (int)(wpt_y - 75f * gc.scaley));
//						g2.setStroke(new BasicStroke(2.5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, dashes, 0f));
//						g2.drawLine(wpt_x, (int)(wpt_y - 3f * gc.scaley), wpt_x, (int)(wpt_y - pixels_per_nm * 7.1f));
//						g2.drawLine(wpt_x, (int)(wpt_y + 3f * gc.scaley), wpt_x, (int)(wpt_y + pixels_per_nm * 7.1f));
//						g2.setTransform(dep_runway_trans);
//					}
//
//				}
//				if(isDestRunway) { // draw destination runway
//					if(this.xpd.ils_show(this.pilot)) {
//						float dashes[] = { 10.0f, 10.0f };
//						float runway_rotate = (this.xpd.ils_rotate(pilot) % 360f) - 90f;
//						g2.setStroke(this.stroke2_5);
//						g2.setColor(gc.color_markings);
//						dest_runway_trans = g2.getTransform();
//						g2.rotate(Math.toRadians(runway_rotate), wpt_x, wpt_y);
//						g2.drawLine((int)(wpt_x - 9f * gc.scalex), (int)(wpt_y + 9f * gc.scaley), (int)(wpt_x - 9f * gc.scalex), (int)(wpt_y - 75f * gc.scaley));
//						g2.drawLine((int)(wpt_x + 9f * gc.scalex), (int)(wpt_y + 9f * gc.scaley), (int)(wpt_x + 9f * gc.scalex), (int)(wpt_y - 75f * gc.scaley));
//						g2.setStroke(new BasicStroke(2.5f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10f, dashes, 0f));
//						g2.drawLine(wpt_x, (int)(wpt_y - 3f * gc.scaley), wpt_x, (int)(wpt_y - pixels_per_nm * 7.1f));
//						g2.drawLine(wpt_x, (int)(wpt_y + 3f * gc.scaley), wpt_x, (int)(wpt_y + pixels_per_nm * 7.1f));
//						g2.setTransform(dest_runway_trans);
//					}
//				}
				
				g2.setTransform(fms_trans);
			}
		}
		

		///
		g2.setTransform(main_map);
		///
		//draw tc/decel/td
		//tc

		if(this.xpd.tc_show(this.pilot)) {
			float xx = zibo_x(this.xpd.tc_x(this.pilot));
			float yy = zibo_y(this.xpd.tc_y(this.pilot));
			g2.setStroke(stroke3);
			g2.setColor(gc.color_lime);
			g2.drawOval((int)(xx - (10f * gc.scalex)), (int)(yy - (10f * gc.scaley)), (int)(20f * gc.scaling_factor), (int)(20f * gc.scaling_factor));
			g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
			if(this.xpd.tc_id(this.pilot) != null) {
				g2.drawString(this.xpd.tc_id(this.pilot), xx + (35f * gc.scalex), yy + (15 * gc.scaley));
			}
		}
		//decel
		if(this.xpd.decel_show(this.pilot)) {
			float xx = zibo_x(this.xpd.decel_x(this.pilot));
			float yy = zibo_y(this.xpd.decel_y(this.pilot));
			g2.setStroke(stroke3);
			g2.setColor(gc.color_lime);
			g2.drawOval((int)(xx - (10f * gc.scalex)), (int)(yy - (10f * gc.scaley)), (int)(20f * gc.scaling_factor), (int)(20f * gc.scaling_factor));
			g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
			if(this.xpd.decel_id(this.pilot) != null) {
				g2.drawString(this.xpd.decel_id(this.pilot), xx + (35f * gc.scalex), yy + (15 * gc.scaley));
			}
		}
		//td
		if(this.xpd.td_show(this.pilot)) {
			float xx = zibo_x(this.xpd.td_x(this.pilot));
			float yy = zibo_y(this.xpd.td_y(this.pilot));
			g2.setStroke(stroke3);
			g2.setColor(gc.color_lime);
			g2.drawOval((int)(xx - (10f * gc.scalex)), (int)(yy - (10f * gc.scaley)), (int)(20f * gc.scaling_factor), (int)(20f * gc.scaling_factor));
			g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
			if(this.xpd.td_id(this.pilot) != null) {
				g2.drawString(this.xpd.td_id(this.pilot), xx + (35f * gc.scalex), yy + (15 * gc.scaley));
			}
		}
		
		//draw runways
		
		

		g2.setClip(original_clipshape);
		
		


		
		
//		mod_waypoints.clear();
//		mod_waypoint_names.clear();
//		mod_leg_start.clear();
//		mod_leg_end.clear();
//
//		for(int i = 0; i < this.xpd.fms_legs_num2(); i++) {
//
//
//
//			if(FMS.mod_legs[i].startName != "") {
//
//				map_projection.setPoint(FMS.mod_legs[i].getStartLat(), FMS.mod_legs[i].getStartLon());
//				int x = (int) map_projection.getX();
//				int y = (int) map_projection.getY();
//
//				if(FMS.mod_legs[i].endName == this.xpd.dest_arpt()) {
//					map_projection.setPoint(FMS.mod_legs[i].getStartLat(), FMS.mod_legs[i].getStartLon());
//				}else {
//					map_projection.setPoint(FMS.mod_legs[i].getEndLat(), FMS.mod_legs[i].getEndLon());
//				}
//
//				int x1 = (int) map_projection.getX();
//				int y1 = (int) map_projection.getY();
//
//				//float ratio = 0.0f;
//
//				//				ratio = 0.2f;
//				//				x = (int) (ratio*x1 + (1.0 - ratio)*x);
//				//				y = (int) (ratio*y1 + (1.0 - ratio)*y);
//				//				ratio = 0.8f;
//				//				x1 = (int) (ratio*x1 + (1.0 - ratio)*x);
//				//				y1 = (int) (ratio*y1 + (1.0 - ratio)*y);
//
//				map_projection.setPoint(FMS.modified_entries[i].getLat(), FMS.modified_entries[i].getLon());
//				int x2 = (int) map_projection.getX();
//				int y2 = (int) map_projection.getY();
//
//				Point2D waypoint = new Point2D.Float(x2, y2);
//				mod_waypoints.add(waypoint);
//				String waypoint_name = FMS.modified_entries[i].getName();
//				mod_waypoint_names.add(waypoint_name);
//				Point2D legStart = new Point2D.Float(x, y);
//				mod_leg_start.add(legStart);
//				Point2D legEnd = new Point2D.Float(x1, y1);
//				mod_leg_end.add(legEnd);
//
//			}
//		}
//
//		g2.clip(map_clip);
//		///
//		main_map = g2.getTransform();
//		///
//		g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
//
//		// draw legs
//
//		for(int i = 1; i < mod_waypoint_names.size() - 1; i++) {
//
//
//			int leg_start_x = (int) mod_leg_start.get(i).getX();
//			int leg_start_y = (int) mod_leg_start.get(i).getY();
//
//			if(mod_waypoint_names.get(i - 1).equals(this.xpd.origin_arpt())) {
//
//				leg_start_x = (int) mod_waypoints.get(i).getX();
//				leg_start_y = (int) mod_waypoints.get(i).getY();
//			}
//
//			if(mod_waypoint_names.get(i - 1).equals("VECTOR") || mod_waypoint_names.get(i - 1).equals("DISCONTINUITY")) {
//
//				leg_start_x = (int) mod_waypoints.get(i).getX();
//				leg_start_y = (int) mod_waypoints.get(i).getY();
//			}
//
//			int leg_end_x = (int) mod_leg_end.get(i).getX();
//			int leg_end_y = (int) mod_leg_end.get(i).getY();
//
//			if(mod_waypoint_names.get(i + 1).equals(this.xpd.dest_arpt())) {
//
//				leg_end_x = (int) mod_waypoints.get(i).getX();
//				leg_end_y = (int) mod_waypoints.get(i).getY();
//			}
//			if(mod_waypoint_names.get(i + 1).equals("VECTOR") || mod_waypoint_names.get(i + 1).equals("DISCONTINUITY")) {
//
//				leg_end_x = (int) mod_waypoints.get(i - 1).getX();
//				leg_end_y = (int) mod_waypoints.get(i - 1).getY();
//			}
//
//			if(i == mod_waypoint_names.size() - 2) {
//				leg_end_x = (int) mod_waypoints.get(i + 1).getX();
//				leg_end_y = (int) mod_waypoints.get(i + 1).getY();
//			}
//
//			int curve_start_x = (int) mod_leg_end.get(i).getX();
//			int curve_start_y = (int) mod_leg_end.get(i).getY();
//
//			int curve_end_x = (int) mod_leg_start.get(i + 1).getX();
//			int curve_end_y = (int) mod_leg_start.get(i + 1).getY();
//
//
//			if(mod_waypoint_names.get(i + 1).equals("VECTOR") || mod_waypoint_names.get(i + 1).equals("DISCONTINUITY")) {
//
//
//				curve_end_x = (int) mod_waypoints.get(i).getX();
//				curve_end_y = (int) mod_waypoints.get(i).getY();
//			}
//
//			if(i == mod_waypoint_names.size() - 2) {
//				curve_end_x = (int) mod_waypoints.get(i).getX();
//				curve_end_y = (int) mod_waypoints.get(i).getY();
//			}
//
//			int control_p_x = (int) mod_waypoints.get(i + 1).getX();
//			int control_p_y = (int) mod_waypoints.get(i + 1).getY();
//
//			float[] inactive_line_dash = {20f * gc.scaling_factor, 20f * gc.scaling_factor};
//			g2.setStroke(new BasicStroke(4f * gc.scaling_factor, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 20.0f * gc.scaling_factor, inactive_line_dash, 0.0f));
//
//			if(this.xpd.fpln_active()) {
//				g2.setColor(gc.color_markings);
//			}else {
//				g2.setColor(gc.color_navaid);
//			}
//			if(!(FMS.modified_entries[i].getName().equals(this.xpd.origin_arpt()) || 
//					FMS.modified_entries[i].getName().equals(this.xpd.dest_arpt()) || 
//					FMS.modified_entries[i].getName().equals("VECTOR") || 
//					FMS.modified_entries[i].getName().equals("DISCONTINUITY"))) {
//				g2.drawLine(leg_start_x, leg_start_y, leg_end_x, leg_end_y);
//				//q.setCurve(curve_start_x, curve_start_y, control_p_x, control_p_y, curve_end_x, curve_end_y);
//				//g2.draw(q);
//			}
//
//		}
//
//		// draw waypoints
//
//		for(int i = 0; i < mod_waypoints.size(); i++) {
//
//			int wpt_x = (int) mod_waypoints.get(i).getX();
//			int wpt_y = (int) mod_waypoints.get(i).getY();
//
//
//			AffineTransform fms_trans = g2.getTransform();
//			if(!(FMS.modified_entries[i].getName().equals(this.xpd.origin_arpt()) || 
//					FMS.modified_entries[i].getName().equals(this.xpd.dest_arpt()) || 
//					FMS.modified_entries[i].getName().equals("VECTOR") || 
//					FMS.modified_entries[i].getName().equals("DISCONTINUITY"))) {
//				g2.rotate(Math.toRadians(this.map_up * -1), wpt_x, wpt_y);
//				if(FMS.modified_entries[i].getName().equals(xpd.active_waypoint())) {
//					g2.setColor(gc.color_magenta);
//					if(FMS.modified_entries[i].getType() == 0.0f) {
//						gc.displayNavAid(rs.img_magenta_waypoint, wpt_x, wpt_y, g2);
//					}
//					if(FMS.active_entries[i].getType()  == 5.0f) {
//						gc.displayNavAid(rs.img_magenta_flyover_waypoint, wpt_x, wpt_y, g2);
//					}
//				}else {
//					g2.setColor(gc.color_markings);
//					if(FMS.active_entries[i].getType() == 0.0f) {
//						gc.displayNavAid(rs.img_white_waypoint, wpt_x, wpt_y, g2);
//					}
//					if(FMS.active_entries[i].getType()  == 5.0f) {
//						gc.displayNavAid(rs.img_white_flyover_waypoint, wpt_x, wpt_y, g2);
//					}
//				}
//			}
//
//			int alt_rest = (int)FMS.modified_entries[i].getAlt_rest();
//			int alt_rest2 = (int)FMS.modified_entries[i].getAlt_rest_2();
//			g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
//			if(!(FMS.modified_entries[i].getName().equals(this.xpd.origin_arpt()) || 
//					FMS.modified_entries[i].getName().equals(this.xpd.dest_arpt()) || 
//					FMS.modified_entries[i].getName().equals("VECTOR") || 
//					FMS.modified_entries[i].getName().equals("DISCONTINUITY"))) {
//				g2.drawString(FMS.modified_entries[i].getName(), wpt_x + (30f * gc.scaling_factor), wpt_y + (30f * gc.scaling_factor));
//				if(this.xpd.efis_data_on(pilot)) {
//					g2.setFont(rs.glassFont.deriveFont(22f * gc.scaling_factor));
//					if(alt_rest != 0) {
//						if(FMS.modified_entries[i].getAlt_rest_type() == 0.0f) { //  at
//							g2.drawString("" + alt_rest, wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}else if(FMS.modified_entries[i].getAlt_rest_type() == 1.0f) { // below
//							g2.drawString("" + alt_rest + "B", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}else if(FMS.modified_entries[i].getAlt_rest_type() == 2.0f) { // above
//							g2.drawString("" + alt_rest + "A", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}else if(FMS.modified_entries[i].getAlt_rest_type() == 3.0f) { // between
//							g2.drawString("" + alt_rest + "A", wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//							g2.drawString("" + alt_rest2 + "B", wpt_x + (30 * gc.scaling_factor), wpt_y + (90 * gc.scaling_factor));
//						}else if(FMS.modified_entries[i].getAlt_rest_type() == 4.0f) { // destination runway
//							g2.drawString("" + alt_rest, wpt_x + (30 * gc.scaling_factor), wpt_y + (60 * gc.scaling_factor));
//						}
//					}
//				}
//			}
//
//			g2.setTransform(fms_trans);
//		}
//		///
//		g2.setTransform(main_map);
//		///
//		g2.setClip(original_clipshape);
		
	}

	private void drawHold(int type, int crs, float dist, float x, float y) {

		boolean isRight = false;
		if(type % 2 == 0) {
			isRight = true;
		}else {
			isRight = false;
		}

		float xx = zibo_x(x);
		float yy = zibo_y(y);
		//g2.setStroke(stroke3);
		//g2.setColor(gc.color_lime);
		//g2.drawRect((int)xx, (int)yy, 20 , 20);
		if(type > 30 && type < 40) { //solid cyan
			g2.setStroke(stroke3);
			g2.setColor(gc.color_navaid);
		}else if(type > 20 && type < 30 ) { // dashed white
			float[] inactive_line_dash = {15f * gc.scaling_factor, 15f * gc.scaling_factor};
			g2.setStroke(new BasicStroke(4f * gc.scaling_factor, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 15.0f * gc.scaling_factor, inactive_line_dash, 0.0f));
			g2.setColor(gc.color_markings);
		}else { // magenta
			g2.setStroke(stroke3);
			g2.setColor(gc.color_magenta);
		}
		
		original_trans = g2.getTransform();
		g2.rotate(Math.toRadians(crs), xx, yy);
		double distance = Math.cos(Math.toRadians(20)) * dist;
		//double radius_distance = Math.tan(Math.toRadians(25)) * distance;
		float radius_distance = 4f;
		//float distance = dist * 1f;
		//float radius_distance = dist * 0.7f;
		int yyy = (int) (yy + (distance * pixels_per_nm));
		int radius = (int) (radius_distance * pixels_per_nm);
		if(isRight) {
			g2.drawLine((int)xx, (int)yy, (int)xx, yyy);
			g2.drawArc((int)xx, yyy - radius / 2, radius, radius, 180, 180);
			g2.drawLine((int)xx + radius, (int)yy, (int)xx + radius, yyy);
			g2.drawArc((int)xx, (int)(yyy - (distance * pixels_per_nm) - radius / 2)  , radius, radius, 0, 180);
		}else {
			g2.drawLine((int)xx, (int)yy, (int)xx, yyy);
			g2.drawArc((int)xx - radius, yyy - radius / 2, radius, radius, 0, -180);
			g2.drawLine((int)xx - radius, (int)yy, (int)xx - radius, yyy);
			g2.drawArc((int)xx - radius, (int)(yyy - (distance * pixels_per_nm) - radius / 2)  , radius, radius, 0, 180);
		}

		g2.setTransform(original_trans);

	}

	private void drawWeather() {


		weather_cycle += 0.008f;
		if (weather_cycle > 1f) {
			weather_cycle = 0f;
			weather_invert = !weather_invert;
		}

		int start = 180;
		int extent = Math.round(weather_cycle * -180f);

		if (weather_invert) {
			extent = -180 - extent;
		}

		weather_sweep_clip.setFrame(this.map_center_x - this.rose_radius, this.map_center_y - this.rose_radius, this.rose_radius * 2, this.rose_radius * 2);
		weather_sweep_clip.setArcType(Arc2D.PIE);
		weather_sweep_clip.setAngleStart(start);
		weather_sweep_clip.setAngleExtent(extent);

		if(extent <= 0 && extent >= -2) {
			weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
		}

		if(extent <= -179 && extent >= -180) {
			weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
		}
		weather_clip.setArcByCenter(this.map_center_x, this.map_center_y, 670f * gc.scaling_factor, 0f, 180f, Arc2D.PIE);

		g2.clip(weather_clip);

		if(weather.img_weather1 != null) {
			g2.translate(this.map_center_x, this.map_center_y);
			g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), 0, 0));
			g2.drawImage(weather.img_weather1, -(weather.img_weather1.getWidth()/2), -(weather.img_weather1.getHeight()/2), null);
			g2.setTransform(original_trans);
		}
		if(weather.img_weather2 != null) {
			g2.clip(weather_sweep_clip);
			g2.translate(this.map_center_x, this.map_center_y);
			g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), 0, 0));
			g2.drawImage(weather.img_weather2, -(weather.img_weather2.getWidth()/2 - 3), -(weather.img_weather2.getHeight()/2 - 3), null);
			g2.setTransform(original_trans);
			g2.setClip(original_clipshape);
		}

	}

	private void drawTerrain() {

		terrain_cycle += 0.008f;
		if (terrain_cycle > 1f) {
			terrain_cycle = 0f;
			terrain_invert = !terrain_invert;
		}

		int start = 180;
		int extent = Math.round(terrain_cycle * -180f);

		if (terrain_invert) {
			extent = -180 - extent;
		}

		terrain_sweep_clip.setFrame(this.map_center_x - this.rose_radius, this.map_center_y - this.rose_radius, this.rose_radius * 2, this.rose_radius * 2);
		terrain_sweep_clip.setArcType(Arc2D.PIE);
		terrain_sweep_clip.setAngleStart(start);
		terrain_sweep_clip.setAngleExtent(extent);

		if(extent <= 0 && extent >= -2) {
			if(isMapCenter) {
				terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, true);
			}else {
				terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
			}

		}
		if(extent <= -179 && extent >= -180) {
			if(isMapCenter) {
				terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, true);
			}else {
				terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);

			}		
		}

		if(!isMapCenter) {
			terrain_clip.setArcByCenter(this.map_center_x, this.map_center_y, 670f * gc.scaling_factor, 0f, 180f, Arc2D.PIE);
		}else {
			terrain_clip.setArcByCenter(this.map_center_x, this.map_center_y, 500f * gc.scaling_factor, 0f, 180f, Arc2D.CHORD);
		}
		g2.clip(terrain_clip);

		if(this.xpd.efis_terr_on(pilot) && egpws_loaded) {

			if(terrain.img_terrain1 != null) {
				g2.translate(this.map_center_x, this.map_center_y);
				g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), 0, 0));
				g2.drawImage(terrain.img_terrain1, -(terrain.img_terrain1.getWidth()/2), -(terrain.img_terrain1.getHeight()/2), null);
				g2.setTransform(original_trans);
			}
			if(terrain.img_terrain2 != null) {
				g2.clip(terrain_sweep_clip);
				g2.translate(this.map_center_x, this.map_center_y);
				g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), 0, 0));
				g2.drawImage(terrain.img_terrain2, -(terrain.img_terrain2.getWidth()/2 - 3), -(terrain.img_terrain2.getHeight()/2 - 3), null);
				g2.setTransform(original_trans);
				g2.setClip(original_clipshape);
			}

			if(initial_sweep_rotate > 0f) {
				initial_sweep_rotate = initial_sweep_rotate - 1.4f;
			}else if (initial_sweep_rotate < 0f) {
				initial_sweep_rotate = 0f;
				initial_sweep_required = false;
			}

			if(initial_sweep_required) {
				g2.translate(this.map_center_x, this.map_center_y);
				g2.setColor(Color.BLACK);
				Arc2D.Float initial_sweep = new Arc2D.Float(Arc2D.PIE);
				initial_sweep.setFrame((int)-this.rose_radius, (int)-this.rose_radius, (int)(this.rose_radius * 2), (int)(this.rose_radius * 2));
				initial_sweep.setAngleStart(0f);
				initial_sweep.setAngleExtent(initial_sweep_rotate);
				g2.fill(initial_sweep);
				g2.setTransform(original_trans);

			}

		}else {
			initial_sweep_required = true;
			initial_sweep_rotate = 180f;
		}
		g2.setClip(original_clipshape);
	}

	private void drawTrackLine() {
		if(!this.pln_mode) {
			g2.translate(this.map_center_x, this.map_center_y);
			g2.scale(gc.scalex, gc.scaley);
			g2.setColor(gc.color_markings);
			g2.rotate(Math.toRadians(track_line_rotate), 0, 0);
			g2.setStroke(stroke5);
			if(isMapCenter) {
				//ctr mode
				g2.drawLine(-10, 188, 10, 188);
				g2.drawLine(-10, -188, 10, -188);
				g2.drawLine(0, -160, 0, -405);
				//g2.drawLine(0, -20, 0, -150); // this should be turn indicator
				g2.drawLine(0, 150, 0, 380);
			}else {
				//expanded mode
				if(this.xpd.irs_aligned()) {
					
					if(!this.xpd.on_gound()) {
						g2.drawLine(0, -165, 0, -783);
					}else {
						g2.drawLine(0, -15, 0, -783);
					}
					
					
					
//					if(!(this.map_mode || this.map_ctr_mode)) {
//						g2.drawLine(0, -20, 0, -783); // this should be turn indicator
//					}else {
//						if(this.map_range.equals("2.5")) {
//							g2.drawLine(0, -310, 0, -783);
//						}else if(this.map_range.equals("5")) {
//							g2.drawLine(0, -180, 0, -783);
//						}else if(this.map_range.equals("10")) {
//							g2.drawLine(0, -205, 0, -783);
//						}else if(this.map_range.equals("20")) {
//							g2.drawLine(0, -180, 0, -783);
//						}else if(this.map_range.equals("40")) {
//							g2.drawLine(0, -100, 0, -783);
//						}else {
//							g2.drawLine(0, -50, 0, -783);
//						}
//						
//					}
					
				}
				

				if(!(this.xpd.efis_terr_on(pilot) || this.xpd.efis_wxr_on(pilot) || this.xpd.tcas_on(pilot))) {
					g2.drawLine(-10, -190, 10, -190);
					g2.drawLine(-10, -379, 10, -379);
					g2.drawLine(-10, -569, 10, -569);
				}
			}
		}
		g2.setTransform(original_trans);
		
		float turn_speed = turn_speed_averager.running_average(this.xpd.yaw_rotation()); // turn speed in deg/s
		//float turn_speed = turn_speed_averager.running_average(-2f); // turn speed in deg/s
		float turn_radius = turn_radius(turn_speed, this.xpd.groundspeed()); // turn radius in nm
		
		if(this.map_mode || this.map_ctr_mode) {
	        if (this.max_range > 20) {
	            // first segment : 30sec
	           draw_position_trend_vector_segment(g2, turn_radius, turn_speed, pixels_per_nm, 0f, 23f);
	            // second segment : 60sec
	           draw_position_trend_vector_segment(g2, turn_radius, turn_speed, pixels_per_nm, 38f, 68f);
	            // third segment : 90sec
	           draw_position_trend_vector_segment(g2, turn_radius, turn_speed, pixels_per_nm, 83f, 113f);
	        } else if (this.max_range == 20) {
	            // first segment : 30sec
	           draw_position_trend_vector_segment(g2, turn_radius, turn_speed, pixels_per_nm, 0f, 30f);
	            // second segment : 60sec
	           draw_position_trend_vector_segment(g2, turn_radius, turn_speed, pixels_per_nm, 40f, 70f);
	        } else if(this.max_range <= 10) {
	            // first segment : 30sec
	        	draw_position_trend_vector_segment(g2, turn_radius, turn_speed, pixels_per_nm, 0f, 27f);
	        }
		}
	}

	@SuppressWarnings("unchecked")
	private void drawMap() {

		float delta_lat = this.max_range * CoordinateSystem.deg_lat_per_nm();
		float delta_lon = this.max_range * CoordinateSystem.deg_lon_per_nm(this.center_lat);
		float lat_max = this.center_lat + delta_lat * 1.5f;
		float lat_min = this.center_lat - delta_lat * 1.5f;
		float lon_max = this.center_lon + delta_lon * 1.5f;
		float lon_min = this.center_lon - delta_lon * 1.5f;
		this.pixels_per_deg_lat = rose_radius / delta_lat;
		this.pixels_per_deg_lon = rose_radius / delta_lon;
		if(this.pln_mode) {
			map_clip.setArcByCenter(this.map_center_x, this.map_center_y, 425f * gc.scaling_factor, 0f, 360f, Arc2D.CHORD);
		}else {
			map_clip.setArcByCenter(this.map_center_x, this.map_center_y, 670f * gc.scaling_factor, 0f, 360f, Arc2D.CHORD);
		}
		g2.clip(map_clip);
		///
		main_map = g2.getTransform();
		///

//		//draw tc/decel/td
//		//tc
//
//		if(this.xpd.tc_show(this.pilot)) {
//			float xx = zibo_x(this.xpd.tc_x(this.pilot));
//			float yy = zibo_y(this.xpd.tc_y(this.pilot));
//			g2.setStroke(stroke3);
//			g2.setColor(gc.color_lime);
//			g2.drawOval((int)(xx - 6f), (int)(yy - 6f), 12, 12);
//			g2.setFont(rs.glassFont.deriveFont(16f));
//			if(this.xpd.tc_id(this.pilot) != null) {
//				g2.drawString(this.xpd.tc_id(this.pilot), xx + (40f * gc.scalex), yy + (20 * gc.scaley));
//			}
//		}
//		//decel
//		if(this.xpd.decel_show(this.pilot)) {
//			float xx = zibo_x(this.xpd.decel_x(this.pilot));
//			float yy = zibo_y(this.xpd.decel_y(this.pilot));
//			g2.setStroke(stroke3);
//			g2.setColor(gc.color_lime);
//			g2.drawOval((int)(xx - 6f), (int)(yy - 6f), 12, 12);
//			g2.setFont(rs.glassFont.deriveFont(16f));
//			if(this.xpd.decel_id(this.pilot) != null) {
//				g2.drawString(this.xpd.decel_id(this.pilot), xx + (40f * gc.scalex), yy + (20 * gc.scaley));
//			}
//		}
//		//td
//		if(this.xpd.td_show(this.pilot)) {
//			float xx = zibo_x(this.xpd.td_x(this.pilot));
//			float yy = zibo_y(this.xpd.td_y(this.pilot));
//			g2.setStroke(stroke3);
//			g2.setColor(gc.color_lime);
//			g2.drawOval((int)(xx - 6f), (int)(yy - 6f), 12, 12);
//			g2.setFont(rs.glassFont.deriveFont(16f));
//			if(this.xpd.td_id(this.pilot) != null) {
//				g2.drawString(this.xpd.td_id(this.pilot), xx + (40f * gc.scalex), yy + (20 * gc.scaley));
//			}
//		}


		g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
		

		displayOriginDestAirports();

		
		//displayOriginDestRunways();

		for (int lat = (int) lat_min; lat <= (int) lat_max; lat++) {
			for (int lon = (int) lon_min; lon <= (int) lon_max; lon++) {
				

				if (this.xpd.efis_apt_mode(pilot) && this.max_range <= 320 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
					draw_nav_objects(NavigationObject.NO_TYPE_AIRPORT, nor.get_nav_objects(NavigationObject.NO_TYPE_AIRPORT, lat, lon));
				}
				if (this.xpd.efis_sta_mode(pilot) && this.max_range <= 160 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
					draw_nav_objects(NavigationObject.NO_TYPE_VOR, nor.get_nav_objects(NavigationObject.NO_TYPE_VOR, lat, lon));
				}
				if (this.xpd.efis_wpt_mode(pilot) && this.max_range <= 40 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
					draw_nav_objects(NavigationObject.NO_TYPE_FIX, nor.get_nav_objects(NavigationObject.NO_TYPE_FIX, lat, lon));
				}
//				if (this.xpd.efis_wpt_mode(pilot) && this.max_range <= 40 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
//					draw_nav_objects(NavigationObject.NO_TYPE_FIX, nor.get_nav_objects(NavigationObject.NO_TYPE_FIX, lat, lon));
//				}
				
				//draw_nav_objects(NavigationObject.NO_TYPE_RUNWAY, nor.get_nav_objects(NavigationObject.NO_TYPE_RUNWAY, lat, lon));
				
				
		
				g2.setColor(gc.color_lime);
				
				for(int i = 0; i < 5; i++) {
					
					if(this.xpd.fix_show(pilot)[i]) {
						
						
						NavigationObject fix = nor.get_navobj(this.xpd.fix_id(pilot)[i], lat, lon);
						if(fix != null) {
							map_projection.setPoint(fix.lat, fix.lon);
							float cdufix_x = map_projection.getX();
							float cdufix_y = map_projection.getY();
		
							if(fix instanceof Airport) {
								drawAirport(cdufix_x, cdufix_y, (Airport) fix, true);
							}else if(fix instanceof Fix) {
								drawFix(cdufix_x, cdufix_y, (Fix) fix, true);
							}else if(fix instanceof RadioNavBeacon) {
								RadioNavBeacon fixx = (RadioNavBeacon) fix;
								
								if(fixx.has_dme) {
									if(fixx.type == RadioNavBeacon.TYPE_STANDALONE_DME) {
										drawDME(cdufix_x, cdufix_y, (RadioNavBeacon) fix, false, true);
									}else {
										drawVORDME(cdufix_x, cdufix_y, (RadioNavBeacon) fix, false, 0,0, true);
									}
									
								}else {
									drawVOR(cdufix_x, cdufix_y, (RadioNavBeacon) fix, false, 0,0, true);
								}
								
							}
							
							displayFixRings(i, cdufix_x, cdufix_y);
						}
					
					}				
				}
				
			}			
		}
		
		//display runway fixes
		for(int i = 0; i < 5; i++) {
			if(this.xpd.fix_show(pilot)[i]) {
				if(this.xpd.fix_id(pilot)[i].startsWith("RW")) {
					String dest_runway = this.xpd.fix_id(pilot)[i].replaceAll("RW", "");
					NavigationObject dest_runway_fix = nor.get_runway(this.xpd.dest_arpt(), dest_runway, dest.lat, dest.lon, false);
					if(dest_runway_fix != null) {
						Runway rwy = (Runway) dest_runway_fix;
			
						if(rwy != null) {
							if(dest_runway.equals(rwy.rwy_num1)) {
								map_projection.setPoint(rwy.lat1, rwy.lon1);
							}else {
								map_projection.setPoint(rwy.lat2, rwy.lon2);
							}
							float cdufix_x = map_projection.getX();
							float cdufix_y = map_projection.getY();
							drawRunwayFix(cdufix_x, cdufix_y, rwy, this.xpd.fix_id(pilot)[i], true);
							displayFixRings(i, cdufix_x, cdufix_y);
						}
					}
				}
			}
		}


		///
		g2.setTransform(main_map);
		///
		g2.setClip(original_clipshape);
	}
	
	private void displayFixRings(int i, float cdufix_x, float cdufix_y) {
		
		
		
		if(this.xpd.fix_rad_dist_0()[i] > 0f) {
			

			if(this.xpd.fix_rad_dist_0()[i] == 1f) {
				
			}else {
				int dist = (int) (this.xpd.fix_dist_0()[i] * 2f);
				g2.setStroke(gc.runway_center_line);
				g2.drawOval((int)(cdufix_x - (dist * pixels_per_nm / 2)), (int)(cdufix_y - (dist * pixels_per_nm / 2)), (int)(dist * pixels_per_nm), (int)(dist * pixels_per_nm));
				temp_trans = g2.getTransform();
				g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
				g2.rotate(Math.toRadians(this.map_up * -1), cdufix_x, cdufix_y);
				g2.drawString("" + dist /2, cdufix_x + 20 * gc.scalex, (cdufix_y - (dist * pixels_per_nm / 2)) - 10 * gc.scaley);
				g2.drawString("" + dist /2, cdufix_x + 20 * gc.scalex, (cdufix_y + (dist * pixels_per_nm / 2)) + 30 * gc.scaley);
				g2.setTransform(temp_trans);
			}
		}
		if(this.xpd.fix_rad_dist_1()[i] > 0f) {

			if(this.xpd.fix_rad_dist_1()[i] == 1f) {
				
				
				
			}else {
				int dist = (int) (this.xpd.fix_dist_1()[i] * 2f);
				g2.setStroke(gc.runway_center_line);
				g2.drawOval((int)(cdufix_x - (dist * pixels_per_nm / 2)), (int)(cdufix_y - (dist * pixels_per_nm / 2)), (int)(dist * pixels_per_nm), (int)(dist * pixels_per_nm));
				g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
				temp_trans = g2.getTransform();
				g2.rotate(Math.toRadians(this.map_up * -1), cdufix_x, cdufix_y);
				g2.drawString("" + dist /2, cdufix_x + 20 * gc.scalex, (cdufix_y - (dist * pixels_per_nm / 2)) - 10 * gc.scaley);
				g2.drawString("" + dist /2, cdufix_x + 20 * gc.scalex, (cdufix_y + (dist * pixels_per_nm / 2)) + 30 * gc.scaley);
				g2.setTransform(temp_trans);
			}
		}
		if(this.xpd.fix_rad_dist_2()[i] > 0f) {

			if(this.xpd.fix_rad_dist_2()[i] == 1f) {
				
			}else {
				int dist = (int) (this.xpd.fix_dist_2()[i] * 2f);
				g2.setStroke(gc.runway_center_line);
				g2.drawOval((int)(cdufix_x - (dist * pixels_per_nm / 2)), (int)(cdufix_y - (dist * pixels_per_nm / 2)), (int)(dist * pixels_per_nm), (int)(dist * pixels_per_nm));
				g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
				temp_trans = g2.getTransform();
				g2.rotate(Math.toRadians(this.map_up * -1), cdufix_x, cdufix_y);
				g2.drawString("" + dist /2, cdufix_x + 20 * gc.scalex, (cdufix_y - (dist * pixels_per_nm / 2)) - 10 * gc.scaley);
				g2.drawString("" + dist /2, cdufix_x + 20 * gc.scalex, (cdufix_y + (dist * pixels_per_nm / 2)) + 30 * gc.scaley);
				g2.setTransform(temp_trans);
			}
		}	
		
	}


	@SuppressWarnings({ "unused", "unchecked" })
	private void drawMap2() {
		
		float delta_lat = this.max_range * CoordinateSystem.deg_lat_per_nm();
		float delta_lon = this.max_range * CoordinateSystem.deg_lon_per_nm(this.center_lat);
		float lat_max = this.center_lat + delta_lat * 1f;
		float lat_min = this.center_lat - delta_lat * 1f;
		float lon_max = this.center_lon + delta_lon * 1f;
		float lon_min = this.center_lon - delta_lon * 1f;
		this.pixels_per_deg_lat = rose_radius / delta_lat;
		this.pixels_per_deg_lon = rose_radius / delta_lon;
		if(this.pln_mode) {
			map_clip.setArcByCenter(this.map_center_x, this.map_center_y, 425f * gc.scaling_factor, 0f, 360f, Arc2D.CHORD);
		}else {
			map_clip.setArcByCenter(this.map_center_x, this.map_center_y, 670f * gc.scaling_factor, 0f, 360f, Arc2D.CHORD);
		}
		g2.clip(map_clip);
		///
		main_map = g2.getTransform();
		

		for (int lat = (int) lat_min; lat <= (int) lat_max; lat++) {
			for (int lon = (int) lon_min; lon <= (int) lon_max; lon++) {
				
				//draw cdu fixes
				
				//
				
				if (this.xpd.efis_apt_mode(pilot) && this.max_range <= 320 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
					draw_nav_objects(NavigationObject.NO_TYPE_AIRPORT, nor.get_nav_objects(NavigationObject.NO_TYPE_AIRPORT, lat, lon));
				}
				if (this.xpd.efis_sta_mode(pilot) && this.max_range <= 160 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
					draw_nav_objects(NavigationObject.NO_TYPE_VOR, nor.get_nav_objects(NavigationObject.NO_TYPE_VOR, lat, lon));
				}
				if (this.xpd.efis_wpt_mode(pilot) && this.max_range <= 40 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
					draw_nav_objects(NavigationObject.NO_TYPE_FIX, nor.get_nav_objects(NavigationObject.NO_TYPE_FIX, lat, lon));
				}
//				if (this.xpd.efis_wpt_mode(pilot) && this.max_range <= 40 && !(this.xpd.map_mode_vor(pilot) || this.xpd.map_mode_app(pilot))) {
//					draw_nav_objects(NavigationObject.NO_TYPE_FIX, nor.get_nav_objects(NavigationObject.NO_TYPE_FIX, lat, lon));
//				}
				
				//draw_nav_objects(NavigationObject.NO_TYPE_RUNWAY, nor.get_nav_objects(NavigationObject.NO_TYPE_RUNWAY, lat, lon));
			}
		}

		
		///
		g2.setTransform(main_map);
		///
		g2.setClip(original_clipshape);
		
	}

	private void displayOriginDestAirports() {
		if(this.xpd.vnav_idx() >= 1) {
			if(this.xpd.origin_arpt() != null) {
				origin = nor.get_airport(this.xpd.origin_arpt());
				if(origin != null) {
					map_projection.setPoint(origin.lat, origin.lon);
					float origin_x = map_projection.getX();
					float origin_y = map_projection.getY();
					drawAirport(origin_x, origin_y, (Airport) origin, false);
				}


			}
			if(this.xpd.dest_arpt() != null) {
				dest = nor.get_airport(this.xpd.dest_arpt());
				if(dest != null) {
					map_projection.setPoint(dest.lat, dest.lon);
					float dest_x = map_projection.getX();
					float dest_y = map_projection.getY();
					drawAirport(dest_x, dest_y, (Airport) dest, false);
				}
			}	
		}	
	}
	@SuppressWarnings("unused")
	private void displayOriginDestRunways() {
		
		origin = nor.get_airport(this.xpd.origin_arpt());
		
		if(this.xpd.origin_arpt() != null && this.xpd.ils_show0(pilot)) {
			origin_runway = nor.get_runway(this.xpd.origin_arpt(), this.xpd.ils_runway0(), origin.lat, origin.lon, false);
			if(origin_runway != null) {
				Runway takeoff_rwy = (Runway)origin_runway;
				if(this.xpd.ils_runway0().equals(takeoff_rwy.rwy_num1)) {
					map_projection.setPoint(takeoff_rwy.lat1, takeoff_rwy.lon1);
					
				}else {
					map_projection.setPoint(takeoff_rwy.lat2, takeoff_rwy.lon2);
				}
				float takeoff_rwy_x = map_projection.getX();
				float takeoff_rwy_y = map_projection.getY();
				float runway_length_in_nm = takeoff_rwy.length * 0.0001645788f;
				
				g2.rotate(Math.toRadians(this.xpd.ils_rotate0(pilot)), takeoff_rwy_x, takeoff_rwy_y);
				g2.setColor(Color.RED);
				g2.fillOval((int)(takeoff_rwy_x - 5), (int)(takeoff_rwy_y - 5), 10, 10);
				g2.drawLine((int)(takeoff_rwy_x), (int)(takeoff_rwy_y), (int)( takeoff_rwy_x + (runway_length_in_nm * this.pixels_per_nm)) , (int)(takeoff_rwy_y));
				System.out.println(runway_length_in_nm * this.pixels_per_nm);
				//drawAirport(origin_x, origin_y, (Airport) origin, false);
				g2.setTransform(original_trans);
			}


		}
//		if(this.xpd.dest_arpt() != null) {
//			dest = nor.get_airport(this.xpd.dest_arpt());
//			if(dest != null) {
//				map_projection.setPoint(dest.lat, dest.lon);
//				float dest_x = map_projection.getX();
//				float dest_y = map_projection.getY();
//				drawAirport(dest_x, dest_y, (Airport) dest, false);
//			}
//		}	
		
	}
	
	private void showTunedNavAids() {

		g2.clip(map_clip);
		///
		main_map = g2.getTransform();
		///
		
		//System.out.println(CoordinateSystem.get_bearing(this.xpd.latitude(), this.xpd.longitude(), lat, lon, 0f));

		if(this.map_mode || this.map_ctr_mode || this.pln_mode) {
			if (this.nav1 != null) {
				
				drawNavStation(this.nav1, this.xpd.course("cpt"), this.xpd.back_course("cpt"), this.xpd.nav1_rel_bearing());
				
//				int course = this.xpd.course("cpt");
//				int bcourse = this.xpd.back_course("cpt");
//				float dist = CoordinateSystem.distance(this.xpd.latitude(), this.xpd.longitude(), nav1.lat, nav1.lon);
//				
//				float rotate = (float) Math.toRadians(course);
//				float rotatetext =  (float) Math.toRadians(course + 90);
//				@SuppressWarnings("unused")
//				float tr = 0f;
//				float[] latlon = CoordinateSystem.latlon_at_dist_heading(this.xpd.latitude(), this.xpd.longitude(), dist, (360 + this.xpd.nav1_rel_bearing() + this.xpd.track()) % 360);
//				if(!this.pln_mode) {
//					tr = this.xpd.track();
//					rotatetext = (float) Math.toRadians(course - this.xpd.track() + 90);
//					rotate = (float) Math.toRadians(course - this.xpd.track());
//					latlon = CoordinateSystem.latlon_at_dist_heading(this.xpd.latitude(), this.xpd.longitude(), dist, (360 + this.xpd.nav1_rel_bearing()) % 360);
//				}				
//				//float brng = CoordinateSystem.get_vor_bearing(this.xpd.latitude(), this.xpd.longitude(), nav1.lat, nav1.lon, nav1.offset, this.xpd.magnetic_variation(), tr);		
//				
//				map_projection.setPoint(latlon[0], latlon[1]);
//				float nav1_x = (int) map_projection.getX();
//				float nav1_y = (int) map_projection.getY();
//
//				
//				//System.out.println(this.xpd.heading(pilot) - CoordinateSystem.get_bearing(this.xpd.latitude(), this.xpd.longitude(), nav1.lat, nav1.lon, 0f) - nav1.offset);
//
////				System.out.println("rel bearing=" + (360 + this.xpd.nav1_rel_bearing()) % 360);
////				System.out.println("bearing=" + brng);
//				
////				float nav1_dist = this.xpd.nav1_dme_nm() * this.pixels_per_nm;
////				double nav1_angle = Math.toRadians(this.xpd.nav1_rel_bearing());
////				
////				float nav1_x = (float) (nav1_dist * Math.sin(nav1_angle));
////				float nav1_y = (float) (nav1_dist * Math.cos(nav1_angle));
////				float plane_x = this.map_center_x;
////				float plane_y = this.map_center_y;
////				if(this.pln_mode) {
////					plane_x = this.plane_x;
////					plane_y = this.plane_y;
////				}
////				nav1_x = plane_x + nav1_x;
////				if(this.pln_mode) {
////					nav1_y = plane_y + nav1_y;
////				}else {
////					nav1_y = plane_y - nav1_y;
////				}
//				if (this.nav1.type == RadioNavBeacon.TYPE_VOR) {
//					if (this.nav1.has_dme) {
//						int course_line = (int) (nav1.range * this.pixels_per_nm * 1.5f);
//						g2.setColor(gc.color_lime);
//						//if(nav1.ilt.equals(this.xpd.fix_id(pilot)[0]) && this.xpd.fix_show(pilot)[0]) {
//						//	gc.displayNavAid(rs.img_nd_vortac_fix, nav1_x, nav1_y, g2);
//						//}else {
//							gc.displayNavAid(rs.img_nd_vortac_tuned, nav1_x, nav1_y, g2);
//						//}
//						
//						g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
//						g2.drawString(nav1.ilt, nav1_x - rs.img_nd_vortac_tuned.getWidth() + rs.img_nd_vortac_tuned.getWidth() * 0.75f, nav1_y - rs.img_nd_vortac_tuned.getHeight() / 3f);						
//						g2.rotate(rotatetext, nav1_x, nav1_y);
//						g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
//						g2.drawString(String.format("%03d", course), (int) (nav1_x - 100 * gc.scalex), (int) (nav1_y - 10 * gc.scaley));
//						g2.drawString(String.format("%03d", bcourse), (int) (nav1_x + 80 * gc.scalex), (int) (nav1_y - 10 * gc.scaley));
//						g2.setTransform(original_trans);
//						g2.rotate(rotate, nav1_x, nav1_y);
//						g2.setStroke(gc.vor_dashes);
//						g2.drawLine((int)nav1_x, (int)nav1_y - course_line, (int)nav1_x, (int)nav1_y);
//						g2.drawLine((int)nav1_x, (int)nav1_y, (int)nav1_x, (int)nav1_y + course_line);
//						g2.setTransform(original_trans);
//						
//					}else {
//						g2.setColor(gc.color_lime);
//						gc.displayNavAid(rs.img_nd_vor_tuned, nav1_x, nav1_y, g2);
//						g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
//						g2.drawString(nav1.ilt, nav1_x - rs.img_nd_vor_tuned.getWidth() + rs.img_nd_vor_tuned.getWidth() * 0.75f, nav1_y - rs.img_nd_vor_tuned.getHeight() / 3f);
//					}
//				}else if (this.nav1.type == RadioNavBeacon.TYPE_STANDALONE_DME) {
//	
//					main_map = g2.getTransform();
//					g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
//					map_projection.setPoint(nav1.lat, nav1.lon);
//					float x = map_projection.getX();
//					float y = map_projection.getY();
//					drawDME(x, y, this.nav1, true, false);
//					g2.setTransform(main_map);
//				}
//
//				if (this.xpd.efis_pos_on(pilot)) {
//					Stroke original_stroke = g2.getStroke();
//					g2.setStroke(gc.vor1_radial_dashes);
//					g2.setColor(gc.color_lime);
//					g2.drawLine((int) map_center_x, (int) map_center_y, (int)nav1_x, (int)nav1_y);
//					g2.setStroke(original_stroke);
//				}

			}
			if (this.nav2 != null) {
				
				drawNavStation(this.nav2, this.xpd.course("fo"), this.xpd.back_course("fo"), this.xpd.nav2_rel_bearing());
				
//				int course = xpd.course("fo");
//				int bcourse = xpd.back_course("fo");
//				float dist = CoordinateSystem.distance(this.xpd.latitude(), this.xpd.longitude(), nav2.lat, nav2.lon);
//				
//				float rotate = (float) Math.toRadians(course);
//				float rotatetext =  (float) Math.toRadians(course + 90);
//				@SuppressWarnings("unused")
//				float tr = 0f;
//				float[] latlon = CoordinateSystem.latlon_at_dist_heading(this.xpd.latitude(), this.xpd.longitude(), dist, (360 + this.xpd.nav2_rel_bearing()) % 360);
//				if(!this.pln_mode) {
//					tr = this.xpd.track();
//					rotatetext = (float) Math.toRadians(course - this.xpd.track() + 90);
//					rotate = (float) Math.toRadians(course - this.xpd.track());
//				}
//				
////				float brng = CoordinateSystem.get_vor_bearing(this.xpd.latitude(), this.xpd.longitude(), nav2.lat, nav2.lon, nav2.offset, this.xpd.magnetic_variation(), tr);
//				
//				
////				
//				map_projection.setPoint(latlon[0], latlon[1]);
//				float nav2_x = (int) map_projection.getX();
//				float nav2_y = (int) map_projection.getY();
//				
////
////				float nav2_dist = this.xpd.nav2_dme_nm() * this.pixels_per_nm;
////				double nav2_angle = Math.toRadians(this.xpd.nav2_rel_bearing());
////				float nav2_x = (float) (nav2_dist * Math.sin(nav2_angle));
////				float nav2_y = (float) (nav2_dist * Math.cos(nav2_angle));
////				nav2_x = this.map_center_x + nav2_x;
////				nav2_y = this.map_center_y - nav2_y;
//				
//				if (this.nav2.type == RadioNavBeacon.TYPE_VOR) {
//					if (this.nav2.has_dme) {
//						int course_line = (int) (nav2.range * this.pixels_per_nm * 1.5f);
//						g2.setColor(gc.color_lime);
//						gc.displayNavAid(rs.img_nd_vortac_tuned, nav2_x, nav2_y, g2);
//						g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
//						g2.drawString(nav2.ilt, nav2_x - rs.img_nd_vortac_tuned.getWidth() + rs.img_nd_vortac_tuned.getWidth() * 0.75f, nav2_y - rs.img_nd_vortac_tuned.getHeight() / 3f);						
//						g2.rotate(rotatetext, nav2_x, nav2_y);
//						g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
//						g2.drawString(String.format("%03d", course), (int) (nav2_x - 100 * gc.scalex), (int) (nav2_y - 10 * gc.scaley));
//						g2.drawString(String.format("%03d", bcourse), (int) (nav2_x + 80 * gc.scalex), (int) (nav2_y - 10 * gc.scaley));
//						g2.setTransform(original_trans);
//						g2.rotate(rotate, nav2_x, nav2_y);
//						g2.setStroke(gc.vor_dashes);
//						g2.drawLine((int)nav2_x, (int)nav2_y - course_line, (int)nav2_x, (int)nav2_y);
//						g2.drawLine((int)nav2_x, (int)nav2_y, (int)nav2_x, (int)nav2_y + course_line);
//						g2.setTransform(original_trans);
//					}else {
//						g2.setColor(gc.color_lime);
//						gc.displayNavAid(rs.img_nd_vor_tuned, nav2_x, nav2_y, g2);
//						g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
//						g2.drawString(nav2.ilt, nav2_x - rs.img_nd_vor_tuned.getWidth() + rs.img_nd_vor_tuned.getWidth() * 0.75f, nav2_y - rs.img_nd_vor_tuned.getHeight() / 3f);
//					}
//				}else if (this.nav2.type == RadioNavBeacon.TYPE_STANDALONE_DME) {
//					main_map = g2.getTransform();
//					g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
//					map_projection.setPoint(nav2.lat, nav2.lon);
//					float x = map_projection.getX();
//					float y = map_projection.getY();
//					drawDME(x, y, this.nav2, true, false);
//					g2.setTransform(main_map);
//
//					
//				}
//
//				if (this.xpd.efis_pos_on(pilot)) {
//					Stroke original_stroke = g2.getStroke();
//					g2.setStroke(gc.vor2_radial_dashes);
//					g2.setColor(gc.color_lime);
//					g2.drawLine((int) map_center_x, (int) map_center_y, (int)nav2_x, (int)nav2_y);
//					g2.setStroke(original_stroke);
//				}
			}
		}
		///
		g2.setTransform(main_map);
		///
		g2.setClip(original_clipshape);
	}

	private void drawNavStation(RadioNavBeacon station, int crs, int bcrs, float rel_bearing) {
		
		float dist = CoordinateSystem.distance(this.xpd.latitude(), this.xpd.longitude(), station.lat, station.lon);

		float rotate = (float) Math.toRadians(crs);
		float rotatetext =  (float) Math.toRadians(bcrs + 90);
		@SuppressWarnings("unused")
		float tr = 0f;
		float[] latlon = CoordinateSystem.latlon_at_dist_heading(this.xpd.latitude(), this.xpd.longitude(), dist, (360 + rel_bearing + this.xpd.track()) % 360);
		if(!this.pln_mode) {
			tr = this.xpd.track();
			rotatetext = (float) Math.toRadians(crs - this.xpd.track() + 90);
			rotate = (float) Math.toRadians(crs - this.xpd.track());
			latlon = CoordinateSystem.latlon_at_dist_heading(this.xpd.latitude(), this.xpd.longitude(), dist, (360 + rel_bearing) % 360);
		}				
		//float brng = CoordinateSystem.get_vor_bearing(this.xpd.latitude(), this.xpd.longitude(), nav1.lat, nav1.lon, nav1.offset, this.xpd.magnetic_variation(), tr);		
		
		map_projection.setPoint(latlon[0], latlon[1]);
		float nav_x = (int) map_projection.getX();
		float nav_y = (int) map_projection.getY();

		if (station.type == RadioNavBeacon.TYPE_VOR) {
			if (station.has_dme) {
				int course_line = (int) (station.range * this.pixels_per_nm * 1.5f);
				g2.setColor(gc.color_lime);

				gc.displayNavAid(rs.img_nd_vortac_tuned, nav_x, nav_y, g2);

				
				g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
				g2.drawString(station.ilt, nav_x - rs.img_nd_vortac_tuned.getWidth() + rs.img_nd_vortac_tuned.getWidth() * 0.75f, nav_y - rs.img_nd_vortac_tuned.getHeight() / 3f);						
				g2.rotate(rotatetext, nav_x, nav_y);
				g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
				g2.drawString(String.format("%03d", crs), (int) (nav_x - 100 * gc.scalex), (int) (nav_y - 10 * gc.scaley));
				g2.drawString(String.format("%03d", bcrs), (int) (nav_x + 80 * gc.scalex), (int) (nav_y - 10 * gc.scaley));
				g2.setTransform(original_trans);
				g2.rotate(rotate, nav_x, nav_y);
				g2.setStroke(gc.vor_dashes);
				g2.drawLine((int)nav_x, (int)nav_y - course_line, (int)nav_x, (int)nav_y);
				g2.drawLine((int)nav_x, (int)nav_y, (int)nav_x, (int)nav_y + course_line);
				g2.setTransform(original_trans);
				
			}else {
				g2.setColor(gc.color_lime);
				gc.displayNavAid(rs.img_nd_vor_tuned, nav_x, nav_y, g2);
				g2.setFont(rs.glassFont.deriveFont(24f * gc.scaling_factor));
				g2.drawString(station.ilt, nav_x - rs.img_nd_vor_tuned.getWidth() + rs.img_nd_vor_tuned.getWidth() * 0.75f, nav_y - rs.img_nd_vor_tuned.getHeight() / 3f);
			}
		}else if (station.type == RadioNavBeacon.TYPE_STANDALONE_DME) {

			main_map = g2.getTransform();
			g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
			map_projection.setPoint(station.lat, station.lon);
			float x = map_projection.getX();
			float y = map_projection.getY();
			drawDME(x, y, station, true, false);
			g2.setTransform(main_map);
		}

		if (this.xpd.efis_pos_on(pilot)) {
			Stroke original_stroke = g2.getStroke();
			g2.setStroke(gc.vor1_radial_dashes);
			g2.setColor(gc.color_lime);
			g2.drawLine((int) map_center_x, (int) map_center_y, (int)nav_x, (int)nav_y);
			g2.setStroke(original_stroke);
		}
		
	}

	private void draw_nav_objects(int type, ArrayList<NavigationObject> nav_objects) {

		NavigationObject navobj = null;
		RadioNavBeacon rnb = null;

		float min_rwy = 1700f;

		for (int i = 0; i < nav_objects.size(); i++) {
			navobj = nav_objects.get(i);
			map_projection.setPoint(navobj.lat, navobj.lon);
			float x = map_projection.getX();
			float y = map_projection.getY();
			double dist = Math.hypot(x - this.map_center_x, y - this.map_center_y); // distance in px
			float max_dist = rose_radius * 2f;
			if (dist < max_dist) {
				if (type == NavigationObject.NO_TYPE_AIRPORT) {
					if (((Airport) navobj).longest >= min_rwy) {
						drawAirport(x, y, (Airport) navobj, false);
					}
				}else if (type == NavigationObject.NO_TYPE_VOR) {
					rnb = (RadioNavBeacon) navobj;
					if (rnb.type == RadioNavBeacon.TYPE_VOR) {
						if (rnb.has_dme) {
							drawVORDME(x, y, (RadioNavBeacon) navobj, false, 0, 0, false); //hexagon with leaves
						}else {
							drawVOR(x, y, (RadioNavBeacon) navobj, false, 0, 0, false); //hexagon
						}
					}else if (rnb.type == RadioNavBeacon.TYPE_STANDALONE_DME) {
						drawDME(x, y, (RadioNavBeacon) navobj, false, false); // Y symbol
					}
				}else if (type == NavigationObject.NO_TYPE_FIX) {
					drawFix(x, y, (Fix) navobj, false);
					
				} else if (type == NavigationObject.NO_TYPE_RUNWAY) {
//					Runway runway = (Runway) navobj;
//					if(runway.name.equals(this.xpd.origin_arpt())) {
//						
//						if(runway.rwy_num1.equals(this.xpd.ils_runway0())) {
//							System.out.println(runway.lat1);
//							System.out.println(runway.lon1);
//						}
//						
//						//System.out.println(runway.rwy_num2);			
//					}
				}
			}
		}		
	}

	private void drawRunwayFix(float x, float y, Runway rwy, String name, boolean isCduFix) { // for CDU fixes only
		
		fix_trans = g2.getTransform();
		g2.rotate(Math.toRadians(this.map_up * -1), x, y);
		g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
		if(isCduFix) {
			g2.setColor(gc.color_lime);
//			g2.setStroke(gc.stroke_four);
//			g2.drawOval((int)(x - (28f * gc.scalex)), (int)(y - (28f * gc.scaley)), (int)(56f * gc.scalex), (int)(56f * gc.scaley));
			gc.displayNavAid(rs.img_nd_wpt_fix, x, y, g2);
		}else {
			g2.setColor(gc.color_navaid);
		}
		
		g2.drawString(name, (int)(x + (25 * gc.scalex)), (int)(y + (30 * gc.scaley)));
//		int[] xpoints = {(int)x, (int)(x + (15 * gc.scaling_factor)), (int)(x - (15 * gc.scaling_factor))};
//		int[] ypoints = {(int)(y - (15 * gc.scaling_factor)), (int)(y + (15 * gc.scaling_factor)), (int)(y + (15 * gc.scaling_factor))};
//		g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
//		g2.drawPolygon(xpoints, ypoints, 3);
		g2.setTransform(fix_trans);
		
	}
	
	private void drawFix(float x, float y, Fix fix, boolean isCduFix) {

		fix_trans = g2.getTransform();
		g2.rotate(Math.toRadians(this.map_up * -1), x, y);
		g2.setFont(rs.glassFont.deriveFont(22f * gc.scaling_factor));
		if(isCduFix) {
			g2.setColor(gc.color_lime);
			gc.displayNavAid(rs.img_nd_wpt_fix, x, y, g2);
			//g2.setStroke(gc.stroke_four);
			//g2.drawOval((int)(x - (28f * gc.scalex)), (int)(y - (28f * gc.scaley)), (int)(56f * gc.scalex), (int)(56f * gc.scaley));
		}else {
			g2.setColor(gc.color_navaid);
			gc.displayNavAid(rs.img_nd_wpt, x, y, g2);
		}
		
		g2.drawString(fix.name, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));
		g2.setTransform(fix_trans);

	}

	private void drawDME(float x, float y, RadioNavBeacon dme, boolean is_tuned, boolean isCduFix) {// Y symbol
		
		if(nav1 != null && dme.ilt.equals(nav1.ilt)) {
			tunedDME1 = nav1.ilt;
		}else {
			tunedDME1 = "";
		}
		if(nav2!= null && dme.ilt.equals(nav2.ilt)) {
			tunedDME2 = nav2.ilt;
		}else {
			tunedDME2 = "";
		}

		vordme_trans = g2.getTransform();
		g2.rotate(Math.toRadians(this.map_up * -1), x, y);
		g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
		
		if(!(dme.ilt.equals(tunedDME1) || dme.ilt.equals(tunedDME2))) {
			
				g2.setColor(gc.color_navaid);
				gc.displayNavAid(rs.img_nd_dme, x, y, g2);
				g2.drawString(dme.ilt, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));

		}
		
		if(is_tuned) {
			g2.setColor(gc.color_lime);
			gc.displayNavAid(rs.img_nd_dme_tuned, x, y, g2);
			g2.drawString(dme.ilt, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));
		}
		if (isCduFix) {
			g2.setColor(gc.color_lime);
			gc.displayNavAid(rs.img_nd_dme_fix, x, y, g2);
			g2.drawString(dme.ilt, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));
		}
		
		g2.setTransform(vordme_trans);
	}

	private void drawVOR(float x, float y, RadioNavBeacon vor, boolean is_tuned, int course, int back_course, boolean isCduFix) { // hexagon

		if(nav1 != null && vor.ilt.equals(nav1.ilt)) {
			tunedVOR1 = nav1.ilt;
		}else {
			tunedVOR1 = "";
		}
		if(nav2!= null && vor.ilt.equals(nav2.ilt)) {
			tunedVOR2 = nav2.ilt;
		}else {
			tunedVOR2 = "";
		}
		g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
		if(!(vor.ilt.equals(tunedVOR1) || vor.ilt.equals(tunedVOR2))) {
			vor_trans = g2.getTransform();

			g2.rotate(Math.toRadians(this.map_up * -1), x, y);
			if(isCduFix) {
				g2.setColor(gc.color_lime);
				gc.displayNavAid(rs.img_nd_vor_fix, x, y, g2);
				//g2.setStroke(gc.stroke_four);
				//g2.drawOval((int)(x - (28f * gc.scalex)), (int)(y - (28f * gc.scaley)), (int)(56f * gc.scalex), (int)(56f * gc.scaley));
			}
			else if (is_tuned) {
				g2.setColor(gc.color_lime);
				gc.displayNavAid(rs.img_nd_vor_tuned, x, y, g2);
			} else {
				g2.setColor(gc.color_navaid);
				gc.displayNavAid(rs.img_nd_vor, x, y, g2);
			}
//			int[] xpoints = {(int)(x - (16 * gc.scaling_factor)), (int)(x - (8 * gc.scaling_factor)), (int)(x + (8 * gc.scaling_factor)), (int)(x + 16 * gc.scaling_factor), (int)(x + (8 * gc.scaling_factor)),  (int)(x - (8 * gc.scaling_factor)) };
//			int[] ypoints = {(int)(y), (int)(y + (14 * gc.scaling_factor)),(int)(y + (14 * gc.scaling_factor)), (int)y, (int)(y - (14 * gc.scaling_factor)), (int)(y - (14 * gc.scaling_factor))};
//			g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
//			g2.drawPolygon(xpoints, ypoints, 6);
			g2.drawString(vor.ilt, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));

			g2.setTransform(vor_trans);		
		}
	}

	private void drawVORDME(float x, float y, RadioNavBeacon vordme, boolean is_tuned, int course, int back_course, boolean isCduFix) { // hexagon with leaves

		
		if(nav1 != null && vordme.ilt.equals(nav1.ilt)) {
			tunedVOR1dme = nav1.ilt;
		}else {
			tunedVOR1dme = "";
		}
		if(nav2!= null && vordme.ilt.equals(nav2.ilt)) {
			tunedVOR2dme = nav2.ilt;
		}else {
			tunedVOR2dme = "";
		}
		
		vordme_trans = g2.getTransform();
		g2.rotate(Math.toRadians(this.map_up * -1), x, y);
		g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
		
		if(!(vordme.ilt.equals(tunedVOR1dme) || vordme.ilt.equals(tunedVOR2dme))) {
			
			g2.setColor(gc.color_navaid);
			gc.displayNavAid(rs.img_nd_vortac, x, y, g2);
			
//			int course_line = (int) (vordme.range * this.pixels_per_nm * 1.5f);
//
//
//			 if (is_tuned) {
//				g2.setColor(gc.color_lime);
//				gc.displayNavAid(rs.img_nd_vortac_tuned, x, y, g2);
//			} else {
//				g2.setColor(gc.color_navaid);
//				gc.displayNavAid(rs.img_nd_vortac, x, y, g2);
//			}
////			int[] xpoints = {(int)(x - (16 * gc.scaling_factor)), (int)(x - (8 * gc.scaling_factor)), (int)(x + (8 * gc.scaling_factor)), (int)(x + 16 * gc.scaling_factor), (int)(x + (8 * gc.scaling_factor)),  (int)(x - (8 * gc.scaling_factor)) };
////			int[] ypoints = {(int)(y), (int)(y + (14 * gc.scaling_factor)),(int)(y + (14 * gc.scaling_factor)), (int)y, (int)(y - (14 * gc.scaling_factor)), (int)(y - (14 * gc.scaling_factor))};
////			g2.setStroke(new BasicStroke(4f * gc.scaling_factor));
////			g2.drawPolygon(xpoints, ypoints, 6);
//			g2.setFont(rs.glassFont.deriveFont(23f * gc.scaling_factor));
//			g2.drawString(vordme.ilt, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));
//			g2.setTransform(vordme_trans);
//			if (is_tuned) {
//				g2.rotate(Math.toRadians((course - this.xpd.magnetic_variation()) + 90), x, y);
//				g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
//				g2.drawString(String.format("%03d", course), (int) (x - 100 * gc.scalex), (int) (y - 10 * gc.scaley));
//				g2.drawString(String.format("%03d", back_course), (int) (x + 80 * gc.scalex), (int) (y - 10 * gc.scaley));
//				g2.setTransform(vordme_trans);
//				g2.rotate(Math.toRadians(course - this.xpd.magnetic_variation()), x, y);
//				g2.setStroke(gc.vor_dashes);
//				g2.drawLine((int)x, (int)y - course_line, (int)x, (int)y);
//				g2.drawLine((int)x, (int)y, (int)x, (int)y + course_line);
//			}
			if(isCduFix) {
				g2.setColor(gc.color_lime);
				gc.displayNavAid(rs.img_nd_vortac_fix, x, y, g2);
			}
//			
			g2.drawString(vordme.ilt, (int)(x + (30 * gc.scalex)), (int)(y + (30 * gc.scaley)));
		}

		

		g2.setTransform(vordme_trans);
	}
	
	private void drawAirport(float x, float y, Airport airport, boolean isCduFix) {
		
		
		//System.out.println(method + "=" + x + " " + y);
		
		airport_trans = g2.getTransform();
		g2.rotate(Math.toRadians(this.map_up * -1), x, y);
		

		//if (!airport.icao_code.matches(".*\\d+.*")) {

//			if(airport.icao_code.equals(this.xpd.fix_id()[0]) && this.xpd.fix_show(pilot)[0]) {
//				g2.setColor(gc.color_lime);
//				gc.displayNavAid(rs.img_arpt_fix, x, y, g2);
//				drawFixRadRings(x, y, (int)this.xpd.fix_dist_0()[0] * 2, (int)this.xpd.fix_dist_1()[0] * 2, (int)this.xpd.fix_dist_2()[0] * 2, (int)this.xpd.fix_rad_dist_0()[0], (int)this.xpd.fix_rad_dist_1()[0], (int)this.xpd.fix_rad_dist_2()[0]);
//				
//			} else if (airport.icao_code.equals(this.xpd.fix_id()[1]) && this.xpd.fix_show(pilot)[1]) {
//				g2.setColor(gc.color_lime);
//				gc.displayNavAid(rs.img_arpt_fix, x, y, g2);
//				drawFixRadRings(x, y, (int)this.xpd.fix_dist_0()[1] * 2, (int)this.xpd.fix_dist_1()[1] * 2, (int)this.xpd.fix_dist_2()[1] * 2, (int)this.xpd.fix_rad_dist_0()[1], (int)this.xpd.fix_rad_dist_1()[1], (int)this.xpd.fix_rad_dist_2()[1]);
//			
//			} else if(airport.icao_code.equals(this.xpd.fix_id()[2]) && this.xpd.fix_show(pilot)[2]) {
//				g2.setColor(gc.color_lime);
//				gc.displayNavAid(rs.img_arpt_fix, x, y, g2);
//				drawFixRadRings(x, y, (int)this.xpd.fix_dist_0()[2] * 2, (int)this.xpd.fix_dist_1()[2] * 2, (int)this.xpd.fix_dist_2()[2] * 2, (int)this.xpd.fix_rad_dist_0()[2], (int)this.xpd.fix_rad_dist_1()[2], (int)this.xpd.fix_rad_dist_2()[2]);
//				
//			} else if(airport.icao_code.equals(this.xpd.fix_id()[3]) && this.xpd.fix_show(pilot)[3]) {
//				g2.setColor(gc.color_lime);
//				gc.displayNavAid(rs.img_arpt_fix, x, y, g2);
//				drawFixRadRings(x, y, (int)this.xpd.fix_dist_0()[3] * 2, (int)this.xpd.fix_dist_1()[3] * 2, (int)this.xpd.fix_dist_2()[3] * 2, (int)this.xpd.fix_rad_dist_0()[3], (int)this.xpd.fix_rad_dist_1()[3], (int)this.xpd.fix_rad_dist_2()[3]);
//			
//			} else if(airport.icao_code.equals(this.xpd.fix_id()[4]) && this.xpd.fix_show(pilot)[4]) {
//				g2.setColor(gc.color_lime);
//				gc.displayNavAid(rs.img_arpt_fix, x, y, g2);
//				drawFixRadRings(x, y, (int)this.xpd.fix_dist_0()[4] * 2, (int)this.xpd.fix_dist_1()[4] * 2, (int)this.xpd.fix_dist_2()[4] * 2, (int)this.xpd.fix_rad_dist_0()[4], (int)this.xpd.fix_rad_dist_1()[4], (int)this.xpd.fix_rad_dist_2()[4]);

//			} else {
			if(isCduFix) {

				g2.setColor(gc.color_lime);
				gc.displayNavAid(rs.img_nd_arpt_fix, x, y, g2);
				//g2.setStroke(gc.stroke_four);
				//g2.drawOval((int)(x - (28f * gc.scalex)), (int)(y - (28f * gc.scaley)), (int)(56f * gc.scalex), (int)(56f * gc.scaley));
			}else {
				g2.setColor(gc.color_navaid);
				gc.displayNavAid(rs.img_nd_arpt, x, y, g2);
			}
				
				//gc.displayNavAid(rs.img_arpt, x, y, g2);
				//g2.setStroke(gc.stroke_four);		
				//g2.drawOval((int)(x - (16f * gc.scalex)), (int)(y - (16f * gc.scaley)), (int)(32 * gc.scalex), (int)(32 * gc.scaley));
				
//			}
			g2.setFont(rs.glassFont.deriveFont(26f * gc.scaling_factor));
			g2.drawString(airport.icao_code, (int)(x + (40 * gc.scalex)), (int)(y + (30 * gc.scaley)));
		//}
		g2.setTransform(airport_trans);
	}

	private void drawVorAdf() {


		int vor1_efis_pos = this.xpd.vor1_pos(pilot);
		int vor2_efis_pos = this.xpd.vor2_pos(pilot);

		String nav1_type = "VOR 1";
		Color nav1_color = gc.color_lime;
		String nav1_id = "";
		String nav1_dme = "---";
		String nav2_type = "VOR 2";
		Color nav2_color = gc.color_lime;
		String nav2_id = "";
		String nav2_dme = "---";
		String fmcSource = "FMC L";

		// backgrounds
		temp_trans = g2.getTransform();
		g2.setColor(Color.BLACK);
		g2.scale(gc.scalex, gc.scaley);
		if (vor1_efis_pos != 0) {
			g2.fillRect(0, 930, 150, 199); // VOR1
		}
		if (vor2_efis_pos != 0) {
			g2.fillRect(905, 930, 170, 199); // VOR2
		}
		if (this.xpd.fmc_source(pilot) == 2) {
			fmcSource = "FMC R";
		}
		if (this.map_mode || this.map_ctr_mode || this.pln_mode) {
			g2.fillRect(149, 1003, 105, 50); // FMC source
		}
		if (this.xpd.rnp() > 0) {
			g2.fillRect(445, 983, 65, 70); // RNP
			g2.fillRect(565, 983, 65, 70); // RNP
		}
		g2.setTransform(temp_trans);

		if (this.nav1 != null) {
			nav1_id = this.nav1.ilt;
			if (this.nav1.has_dme) {
				nav1_dme = gc.dme_formatter.format(xpd.nav1_dme_nm());
			}
			if (this.nav1.type == RadioNavBeacon.TYPE_NDB && !this.xpd.map_mode_pln(pilot)) {
				if(isMapCenter) {
					gc.displayImage(rs.img_Adf1_Arrow_ctr, this.xpd.adf1_rel_bearing(), 516.048f, 132.152f, g2);
				}else {
					gc.displayImage(rs.img_Adf1_Arrow, this.xpd.adf1_rel_bearing(), 516.048f, -587.912f, g2);
				}
			} else if (this.nav1.type == RadioNavBeacon.TYPE_VOR && !this.xpd.map_mode_pln(pilot)) {
				if(isMapCenter) {
					gc.displayImage(rs.img_Vor1_Arrow_ctr, this.xpd.nav1_rel_bearing(), 516.048f, 132.152f, g2);
				}else {
					gc.displayImage(rs.img_Vor1_Arrow, this.xpd.nav1_rel_bearing(), 516.048f, -587.912f, g2);
				}

			}
		} else {
			if (vor1_efis_pos == 1) {
				nav1_id = gc.nav_freq_formatter.format(this.xpd.nav1_freq() / 100);
			} else {
				nav1_id = gc.adf_freq_formatter.format(this.xpd.adf1_freq());
			}
		}
		if (this.nav2 != null) {
			nav2_id = this.nav2.ilt;
			if (this.nav2.has_dme) {
				nav2_dme = gc.dme_formatter.format(xpd.nav2_dme_nm());
			}
			if (this.nav2.type == RadioNavBeacon.TYPE_NDB && !this.xpd.map_mode_pln(pilot)) {
				if(isMapCenter) {
					gc.displayImage(rs.img_Adf2_Arrow_ctr, this.xpd.adf2_rel_bearing(), 516.048f, 132.152f, g2);
				}else {
					gc.displayImage(rs.img_Adf2_Arrow, this.xpd.adf2_rel_bearing(), 516.048f, -587.912f, g2);
				}

			} else if (this.nav2.type == RadioNavBeacon.TYPE_VOR && !this.xpd.map_mode_pln(pilot)) {
				if(isMapCenter) {
					gc.displayImage(rs.img_Vor2_Arrow_ctr, this.xpd.nav2_rel_bearing(), 516.048f, 132.152f, g2);
				}else {
					gc.displayImage(rs.img_Vor2_Arrow, this.xpd.nav2_rel_bearing(), 516.048f, -587.912f, g2);
				}		
			}
		} else {
			if (vor2_efis_pos == 1) {
				nav2_id = gc.nav_freq_formatter.format(this.xpd.nav2_freq() / 100);
			} else {
				nav2_id = gc.adf_freq_formatter.format(this.xpd.adf2_freq());
			}
		}
		if (vor1_efis_pos == 1) {
			nav1_type = "VOR 1";
			nav1_color = gc.color_lime;
		} else {
			nav1_type = "ADF 1";
			nav1_color = gc.color_navaid;
		}

		if (vor2_efis_pos == 1) {
			nav2_type = "VOR 2";
			nav2_color = gc.color_lime;
		} else {
			nav2_type = "ADF 2";
			nav2_color = gc.color_navaid;
		}

		if (!this.pln_mode) {
			// VOR1 / ADF1
			if (vor1_efis_pos != 0) {
				gc.drawText2(nav1_type, 10, 90, 30, 0, nav1_color, false, "left", g2);
				gc.drawText2(nav1_id, 10, 55, 30, 0, nav1_color, false, "left", g2);
			}

			if (vor1_efis_pos == 1) {
				gc.drawText2("DME", 10, 20, 24, 0, nav1_color, false, "left", g2);
				gc.drawText2(nav1_dme, 75, 20, 30, 0, nav1_color, false, "left", g2);
			}

			// VOR2 / ADF2
			if (vor2_efis_pos != 0) {
				gc.drawText2(nav2_type, 908, 90, 30, 0, nav2_color, false, "left", g2);
				gc.drawText2(nav2_id, 908, 55, 30, 0, nav2_color, false, "left", g2);
			}
			if (vor2_efis_pos == 1) {
				gc.drawText2("DME", 908, 20, 24, 0, nav2_color, false, "left", g2);
				gc.drawText2(nav2_dme, 973, 20, 30, 0, nav2_color, false, "left", g2);
			}
		}

		if (this.map_mode || this.map_ctr_mode || this.pln_mode) {
			gc.drawText2(fmcSource, 175, 20, 24, 0, gc.color_lime, false, "left", g2);
		}

		// ANP RNP
		if (this.xpd.rnp() > 0) {
			Color color;
			if (this.xpd.anp() > this.xpd.rnp()) {
				color = gc.color_amber;
			} else {
				color = gc.color_lime;
			}
			if (!this.xpd.map_mode_pln(pilot)) {
				gc.drawText2("RNP", 478, 45, 24, 0, color, false, "center", g2);
				gc.drawText2(gc.mfd_ff.format(this.xpd.rnp()), 450, 20, 24, 0, color, false, "left", g2);
				gc.drawText2("ANP", 598, 45, 24, 0, color, false, "center", g2);
				gc.drawText2(gc.mfd_ff.format(Math.round(this.xpd.anp() * 100f) / 100f), 570, 20, 24, 0, color, false, "left", g2);
			}
		}
	}

	/**
	 * 
	 */
	private void drawCompassRose() {

		String heading_track = "TRK";
		compass_rotate = 0f;
		heading_bug_rotate = 0f;
		current_heading_bug_rotate = 0f;
		String display_heading;

		if (this.map_mode || this.map_ctr_mode) {
			display_heading = gc.df3.format(this.xpd.track());
			compass_rotate = xpd.track() * -1;
			heading_bug_rotate = xpd.mcp_heading() - xpd.track(); 
			current_heading_bug_rotate = this.xpd.heading(pilot) - xpd.track();
			track_line_rotate = 0f;

		} else {
			display_heading = gc.df3.format(this.xpd.heading(pilot));
			compass_rotate = xpd.heading(pilot) * -1;
			heading_bug_rotate = this.xpd.mcp_heading() - this.xpd.heading(pilot);
			heading_track = "HDG";
			current_heading_bug_rotate = 0f;
			track_line_rotate = this.xpd.track() - this.xpd.heading(pilot);
		}
		if(this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
			heading_box_offset = 51;
		}else {
			heading_box_offset = 0;
		}

		if(!this.pln_mode) {
			g2.translate(this.map_center_x, 0);
			g2.scale(gc.scalex, gc.scaley);
			g2.setColor(Color.BLACK);
			g2.fillRect(-120, 10 + heading_box_offset , 240, 60);
			g2.setColor(gc.color_markings);
			g2.setStroke(new BasicStroke(4f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND));
			int[] trackBox_xPoints = {-60, -60, 60, 60};
			int[] trackBox_yPoints = {10 + heading_box_offset, 65 + heading_box_offset, 65 + heading_box_offset, 10 + heading_box_offset};
			g2.drawPolyline(trackBox_xPoints, trackBox_yPoints, trackBox_xPoints.length);
			g2.setTransform(original_trans);

			if(this.xpd.irs_aligned()) {

				gc.drawText2(display_heading, 535f, 1000f - heading_box_offset, 48f, 0f, gc.color_markings, false, "center", g2);
				gc.drawText2(heading_track, 410f, 1000f - heading_box_offset, 30f, 0f, gc.color_lime, false, "left", g2);
				gc.drawText2("MAG", 600f, 1000f - heading_box_offset, 30f, 0f, gc.color_lime, false, "left", g2);
				g2.rotate(Math.toRadians(current_heading_bug_rotate), this.map_center_x, this.map_center_y);
				g2.scale(gc.scalex, gc.scaley);
				g2.translate(536 , 854.5);
				int[] cur_heading_bug_xPoints = {0, -15, 15, 0};
				int[] cur_heading_bug_yPoints = {-758 + heading_box_offset, -785 + heading_box_offset, -785 + heading_box_offset, -758 + heading_box_offset};
				g2.drawPolyline(cur_heading_bug_xPoints, cur_heading_bug_yPoints, 4);
				g2.setTransform(original_trans);
			}
			

			if(!isMapCenter) {
				g2.translate(this.map_center_x, this.map_center_y);
				g2.scale(gc.scalex, gc.scaley);
				g2.setColor(gc.color_markings);
				g2.setStroke(rs.stroke5);
				range_arc1.setArcByCenter(0, 0, 189.5f, 0f, 180f, Arc2D.OPEN);//range arcs
				range_arc2.setArcByCenter(0, 0, 379f, 0f, 180f, Arc2D.OPEN);//range arcs
				range_arc3.setArcByCenter(0, 0, 568.5f, 0f, 180f, Arc2D.OPEN); //range arcs
				
				if((this.xpd.tcas_on(pilot) || this.xpd.efis_terr_on(pilot) || this.xpd.efis_wxr_on(pilot)) && this.xpd.irs_aligned()) {
					g2.draw(range_arc1);//range arcs
					g2.draw(range_arc2);//range arcs
					g2.draw(range_arc3);//range arcs
				}
				
				g2.setTransform(original_trans);
				g2.scale(gc.scalex, gc.scaley);
				
				
				
				g2.translate(-222.5, 96);

				
				
				rs._compassRose.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
						RenderingHints.VALUE_INTERPOLATION_BILINEAR);
				rs._compassRose.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
				rs._compassRose.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING,
						RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
				rs._compassRose.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS,
						RenderingHints.VALUE_FRACTIONALMETRICS_ON);

				compassRoseTransform = rs._compassRose.getTransform();

				rs._compassRose.setComposite(AlphaComposite.Clear);
				
				rs._compassRose.fillRect(0, 0, 1517, 450);
				
				rs._compassRose.setComposite(AlphaComposite.Src);

				rs._compassRose.translate(758.5, 758.5);

//				rs._compassRose.setColor(Color.RED);
//				rs._compassRose.setStroke(compassBgStroke);
//				rs._compassRose.drawOval(-715, -715, 1430, 1430);

				rs._compassRose.setColor(rs.color_markings);
				rs._compassRose.setStroke(rs.stroke5);
//				Arc2D.Float arc = new Arc2D.Float();
//				arc.setArcByCenter(0, 0, 756f, 0f, 180f, Arc2D.OPEN);
//				rs._compassRose.draw(arc);
//				rs._compassRose.drawOval(-756, -756, 1512, 1512);

				rs._compassRose.setFont(rs.glass34);
				
				

				int hdg5 = (int) Math.round(-compass_rotate / 5.0f) * 5;

				rs._compassRose.rotate(Math.toRadians(-50f), 0, 0);

				for (int i = -50; i <= 50; i += 5) {

					int mark5 = ((hdg5 + i) + 360) % 360;

					if (mark5 % 10 == 0) {

						String marktext = "" + mark5 / 10;
						rs._compassRose.drawLine(0, -712, 0, -756);

						if (mark5 % 30 == 0) {

							// draw headings

							if (this.xpd.irs_aligned()) {
								if (mark5 > 100) {

									rs._compassRose.drawString(marktext, -22f, -675f);
								} else {

									rs._compassRose.drawString(marktext, -11f, -675f);

								}
							}
						}

					} else {

						rs._compassRose.drawLine(0, -740, 0, -756);

					}

					rs._compassRose.rotate(Math.toRadians(5f), 0, 0);

				}

				rs._compassRose.setTransform(compassRoseTransform);

				g2.rotate(Math.toRadians((hdg5 - -compass_rotate)), 758.5, 758.5);

				g2.drawImage(rs.img_CompassRose, 0, 0, null);

				g2.setTransform(original_trans);

				// gc.displayImage(rs.img_compass_rose, compass_rotate, -222.305, -559.800, g2);
				//
				// if(this.xpd.irs_aligned()) {
				// gc.displayImage(rs.img_compass_rose_text, compass_rotate, -169.549, -506.940,
				// g2);
				// }

				g2.translate(this.map_center_x, this.map_center_y);
				g2.scale(gc.scalex, gc.scaley);
				g2.setColor(rs.color_markings);
				g2.setStroke(rs.stroke5);
				compass_arc1.setArcByCenter(0, 0, 760f, 0f, 360f, Arc2D.CHORD); //
				g2.draw(compass_arc1);
				g2.setTransform(original_trans);

			}else {
				//ctr mode
				if(this.app_ctr_mode || this.vor_ctr_mode) {
					g2.translate(this.map_center_x, this.map_center_y);
					g2.scale(gc.scalex, gc.scaley);

					g2.setStroke(stroke5);
					g2.setColor(gc.color_markings);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.rotate(Math.toRadians(45f), 0, 0);
					g2.drawLine(0, -380, 0, -435);
					g2.setTransform(original_trans);
				}
				gc.displayImage(rs.img_ctr_compass_rose, compass_rotate, 161, 151.500, g2);
			}
		}

		if(this.pln_mode) {
			g2.translate(this.map_center_x, this.map_center_y);
			g2.scale(gc.scalex, gc.scaley);
			g2.setColor(gc.color_markings);
			g2.setStroke(stroke5);
			pln_mode_circle1.setArcByCenter(0, 0, 212.5f, 0f, 360f, Arc2D.OPEN);
			pln_mode_circle2.setArcByCenter(0, 0, 425f, 0f, 360f, Arc2D.OPEN);
			g2.draw(pln_mode_circle1);
			g2.draw(pln_mode_circle2);
			g2.setColor(Color.BLACK);
			g2.fillRect(-40, 410, 80, 30);
			g2.fillRect(-40, 195, 80, 30);
			g2.fillRect(-40, -225, 80, 30);
			g2.fillRect(-40, -440, 80, 30);
			g2.setTransform(original_trans);

			
			// north east south west
			
			g2.setColor(gc.color_lime);
			g2.translate(this.getWidth() /2,  this.getHeight() /2);
			g2.scale(gc.scalex, gc.scaley);
			
			g2.setFont(rs.glass44);
			
			g2.drawString("N", -34, -460);
			g2.drawString("E", 467, 20);
			g2.drawString("S", -16, 498);
			g2.drawString("W", -503, 20);
			
			//arrow
			g2.setStroke(rs.stroke5round);
			
			g2.drawLine(14, -460, 14, -498);
			g2.drawLine(5, -490, 14 , -498);
			g2.drawLine(23, -490, 14 , -498);

			g2.setTransform(original_trans);
			//

			gc.drawText2("" + (int)this.max_range, 532f, 940f, 26f, 0f, gc.color_markings, false, "center", g2);
			gc.drawText2(map_range, 532f, 727.375f, 26f, 0f, gc.color_markings, false, "center", g2);
			gc.drawText2(map_range, 532f, 306.375f, 26f, 0f, gc.color_markings, false, "center", g2);
			gc.drawText2("" + (int)this.max_range, 532f, 93f, 26f, 0f, gc.color_markings, false, "center", g2);

			//if(this.xpd.fpln_active() || this.xpd.legs_mod_active()) {
			g2.clip(map_clip);
			g2.transform(AffineTransform.getRotateInstance(Math.toRadians(map_up), map_center_x, map_center_y));
			g2.rotate(Math.toRadians(this.xpd.track() - this.xpd.magnetic_variation()), plane_x, plane_y);
			g2.drawImage(rs.img_plane_pln, (int)(plane_x - (rs.img_plane_pln.getWidth() * gc.scalex)/2),  (int)(plane_y - 10 * gc.scaley) , (int)(rs.img_plane_pln.getWidth() * gc.scalex), (int)(rs.img_plane_pln.getHeight() * gc.scaley), null);
			g2.setTransform(original_trans);
			g2.setClip(original_clipshape);
			//}
		}

		//green arc
		g2.scale(gc.scalex, gc.scaley);
		g2.translate(1072 / 2, 1053 - 198.55);
		g2.setColor(gc.color_lime);
		if(this.xpd.green_arc_show(pilot)) {
			if(isMapCenter) {
				altRangeArc.setArcByCenter(0, 0 + 350f - (700f * this.xpd.green_arc(pilot)) , 350f, 62, 56, Arc2D.OPEN);
			}else {
				altRangeArc.setArcByCenter(0, 0 + 350f - (680f * this.xpd.green_arc(pilot)) , 350f, 62, 56, Arc2D.OPEN);
			}
			
		
			g2.draw(altRangeArc);
		}
		g2.setTransform(original_trans);

		//selected course line
		if(this.vor_mode || this.app_mode) {
			gc.displayImage(rs.img_Selected_Course_Line, (this.xpd.course(pilot) - this.xpd.heading(pilot))%360, 365.050f, -561.099f, g2);
			if(!navFreqDisagree) {

				g2.translate(this.map_center_x, this.map_center_y);
				g2.scale(gc.scalex, gc.scaley);
				g2.setColor(gc.color_magenta);
				g2.rotate(Math.toRadians((this.xpd.course(pilot) - this.xpd.heading(pilot))%360), 0, 0);
				if(pilot == "cpt") {
					g2.translate(0 + (this.xpd.nav1_hdef_dot() * 80f), 0);
				}else {
					g2.translate(0 + (this.xpd.nav2_hdef_dot() * 80f), 0);
				}

				if(this.pilot == "cpt" && nav1_object != null && (nav1_radio.freq_is_localizer() || nav1_radio.freq_is_nav())) {
					g2.drawRect(-8, -100, 16, 200);
				}
				if(this.pilot == "fo" && nav2_object != null && (nav2_radio.freq_is_localizer() || nav1_radio.freq_is_nav())) {
					g2.drawRect(-8, -100, 16, 200);
				}
				g2.setTransform(original_trans);
				if(this.pilot == "cpt" && nav1_object != null && !nav1_radio.freq_is_localizer()) {
					if(vorTo) {
						gc.drawText2("TO", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}else {
						gc.drawText2("FROM", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}
				}else if(this.pilot == "fo" && nav2_object != null && !nav2_radio.freq_is_localizer()){
					if(vorTo) {
						gc.drawText2("TO", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}else {
						gc.drawText2("FROM", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}
				}
			}

		}else if(this.vor_ctr_mode) {

			gc.displayImage(rs.img_Selected_Course_Line_ctr, (this.xpd.course(pilot) - this.xpd.heading(pilot))%360, 365.050f, 179.309f, g2);
			if(!navFreqDisagree) {
				g2.translate(this.map_center_x, this.map_center_y);
				g2.scale(gc.scalex, gc.scaley);
				g2.setColor(gc.color_magenta);
				g2.rotate(Math.toRadians((this.xpd.course(pilot) - this.xpd.heading(pilot))%360), 0, 0);
				if(pilot == "cpt") {
					g2.translate(0 + (this.xpd.nav1_hdef_dot() * 80f), 0);
				}else {
					g2.translate(0 + (this.xpd.nav2_hdef_dot() * 80f), 0);
				}
				if(this.pilot == "cpt" && nav1_object != null && (nav1_radio.freq_is_localizer() || nav1_radio.freq_is_nav())) {
					g2.drawRect(-8, -100, 16, 200);
				}
				if(this.pilot == "fo" && nav2_object != null && (nav2_radio.freq_is_localizer() || nav1_radio.freq_is_nav())) {
					g2.drawRect(-8, -100, 16, 200);
				}
				g2.setTransform(original_trans);
				if(this.pilot == "cpt" && nav1_object != null && !nav1_radio.freq_is_localizer()) {
					if(vorTo) {
						gc.drawText2("TO", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}else {
						gc.drawText2("FROM", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}
				}else if(this.pilot == "fo" && nav2_object != null && !nav2_radio.freq_is_localizer()){
					if(vorTo) {
						gc.drawText2("TO", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}else {
						gc.drawText2("FROM", 800, 150, 30f, 0, gc.color_markings, true, "left", g2);
					}
				}
			}
		}else if(this.app_ctr_mode) {
			gc.displayImage(rs.img_Selected_Course_Line_ctr, (this.xpd.course(pilot) - this.xpd.heading(pilot))%360, 365.050f, 179.309f, g2);
			if(!navFreqDisagree) {
				g2.translate(this.map_center_x, this.map_center_y);
				g2.scale(gc.scalex, gc.scaley);
				g2.setColor(gc.color_magenta);
				g2.rotate(Math.toRadians((this.xpd.course(pilot) - this.xpd.heading(pilot))%360), 0, 0);
				if(pilot == "cpt") {
					g2.translate(0 + (this.xpd.nav1_hdef_dot() * 80f), 0);
				}else {
					g2.translate(0 + (this.xpd.nav2_hdef_dot() * 80f), 0);
				}
				if(this.pilot == "cpt" && nav1_object != null && (nav1_radio.freq_is_localizer() || nav1_radio.freq_is_nav())) {
					g2.drawRect(-8, -100, 16, 200);
				}
				if(this.pilot == "fo" && nav2_object != null && (nav2_radio.freq_is_localizer() || nav1_radio.freq_is_nav())) {
					g2.drawRect(-8, -100, 16, 200);
				}
				g2.setTransform(original_trans);
			}
		}

		if(this.xpd.irs_aligned() && !this.pln_mode) {
			// heading bug and line
			int radius = -758;
			if(isMapCenter) {
				radius = -380;
			}
			g2.setColor(gc.color_magenta);
			g2.translate(this.map_center_x, this.map_center_y);
			g2.rotate(Math.toRadians(heading_bug_rotate), 0, 0);
			g2.scale(gc.scalex, gc.scaley);
			g2.setStroke(stroke4);
			int[] xpoints = {-27, 27, 27, 12, 0, -12, -27};
			int[] ypoints = {radius, radius, radius -20, radius - 20, radius, radius - 20, radius - 20};
			g2.drawPolygon(xpoints, ypoints, 7);
			g2.setStroke(heading_bug_line);
			if(this.xpd.hdg_bug_line(pilot)) {
				
				g2.drawLine(0, 0, 0, radius);
			}
			g2.setTransform(original_trans);
		}
		
		//app mode glideslope
		if(this.app_mode || this.app_ctr_mode) {
			
			int vdef_ydelta = 0;
			boolean show_glideslope = false;
			
			if(this.pilot == "cpt") {
				vdef_ydelta = (int)(85f * this.xpd.nav1_vdef_dot());
				if(this.xpd.nav1_cdi() == 1) {
					show_glideslope = true;
				}else {
					show_glideslope = false;
				}
			}else {
				vdef_ydelta = (int)(85f * this.xpd.nav2_vdef_dot());
				if(this.xpd.nav2_cdi() == 1) {
					show_glideslope = true;
				}else {
					show_glideslope = false;
				}
			}
			

			
			if(show_glideslope) {
				
				g2.scale(gc.scalex, gc.scaley);
				
				if(this.app_mode) {
					g2.translate(1000f, 680f);
				}else {
					g2.translate(1000f, 526.5f);
				}
				
				g2.setColor(Color.BLACK);
				g2.fillRect(0, -180, 60, 360);
				g2.setColor(rs.color_markings);
				g2.setStroke(rs.stroke5);
				
				g2.drawOval(20, -189, 18, 18);
				g2.drawOval(20, -99, 18, 18);

				g2.drawOval(20, 81, 18, 18);
				g2.drawOval(20, 171, 18, 18);
				
				g2.drawLine(2, 0, 58, 0);
				
				g2.setColor(rs.color_magenta);
		
				int[] pointer_x = {15, 30, 45, 30};
				int[] pointer_y = {0 +vdef_ydelta, -25 +vdef_ydelta, 0 +vdef_ydelta, 25 +vdef_ydelta};
				
				g2.fillPolygon(pointer_x, pointer_y, 4);
				
				g2.setTransform(original_trans);
			}
			

		}

	}

	private void drawTopRight() {

		if(isMapCenter && this.xpd.fpln_active()) { //background
			g2.setColor(Color.BLACK);
			g2.scale(gc.scalex, gc.scaley);
			g2.fillRect(780, 0, 295, 150);
			g2.setTransform(original_trans);
		}

		if (this.xpd.irs_aligned()) {
			
			// active waypoint
			if (this.map_ctr_mode || this.map_mode || this.pln_mode) {
				if (this.xpd.fpln_active()) {
					if (!(this.xpd.active_waypoint() == null || this.xpd.active_waypoint().equals("-----"))) {
						gc.drawText2(this.xpd.active_waypoint(), 900, 1000, 32, 0, gc.color_magenta, false, "left", g2);
						if(this.xpd.fpln_nav_id_eta() != null) {
							gc.drawText2(this.xpd.fpln_nav_id_eta(), 1008, 960, 32, 0, gc.color_markings, false, "right", g2);
							gc.drawText2("Z", 1012, 960, 24, 0, gc.color_markings, false, "left", g2);
						}
						gc.drawText2(gc.dme_formatter.format(this.xpd.fpln_nav_id_dist()), 967, 925, 32, 0, gc.color_markings, false, "right", g2);
						gc.drawText2("NM", 970, 925, 24, 0, gc.color_markings, false, "left", g2);

					}
				}
			}
			// vor info
			if (this.vor_mode || this.vor_ctr_mode) {
				String nav_type = "";
				String nav_id = "";
				int nav_course = this.xpd.course(pilot);
				float nav_dme = 0f;
				boolean receiving = false;
				if (pilot == "cpt") {
					if (nav1_radio.receiving()) {
						if (nav1 != null && nav1_radio.freq_is_nav()) {
							receiving = true;
							nav_type = "VOR 1";
							nav_id = nav1.ilt;
						} else {
							receiving = false;
						}
						if (nav1 != null && nav1.type != RadioNavBeacon.TYPE_NDB) {

							nav_dme = this.xpd.nav1_dme_nm();
						}
					}
				} else {
					if (nav2_radio.receiving()) {
						if (nav2 != null && nav2_radio.freq_is_nav()) {
							receiving = true;
							nav_type = "VOR 2";
							nav_id = nav2.ilt;
						} else {
							receiving = false;
						}
						if (nav2 != null && nav2.type != RadioNavBeacon.TYPE_NDB) {

							nav_dme = this.xpd.nav2_dme_nm();
						}
					}
				}
				if (receiving) {
					gc.drawText2(nav_type, 790, 1000, 32, 0, gc.color_lime, false, "left", g2);
					gc.drawText2(nav_id, 940, 1000, 32, 0, gc.color_markings, false, "left", g2);
					gc.drawText2("CRS", 920, 965, 26, 0, gc.color_markings, false, "left", g2);
					gc.drawText2(gc.df3.format(nav_course), 970, 965, 30, 0, gc.color_markings, false, "left", g2);
					gc.drawText2("DME", 920, 930, 26, 0, gc.color_markings, false, "left", g2);
					if (nav_dme > 0) {
						gc.drawText2(gc.dme_formatter.format(nav_dme), 1045, 930, 26, 0, gc.color_markings, false,
								"right", g2);
					} else {
						gc.drawText2("---", 1045, 930, 26, 0, gc.color_markings, false, "right", g2);
					}
				}
			}
			// ils info
			if (this.app_mode || this.app_ctr_mode) {

				String nav_type = "";
				String nav_id = "";
				int nav_course = 0;
				float nav_dme = 0f;
				boolean receiving = false;

				if (pilot == "cpt") {
					if (nav1_radio.receiving()) {
						if (nav1_radio != null && nav1_radio.freq_is_localizer()) {
							receiving = true;
							nav_type = "ILS 1";
							if(nav1_object != null) {
								nav_id = nav1_object.ilt;
							}else {
								nav_id = "";
							}
							nav_course = Math.round(this.xpd.nav1_course());
							nav_dme = this.xpd.nav1_dme_nm();
						} else {
							receiving = false;
						}
//						if (nav1 != null && nav1.type != RadioNavBeacon.TYPE_NDB) {
//
//							
//						}
					}
				} else {
					if (nav2_radio.receiving()) {
						if (nav2_radio != null && nav2_radio.freq_is_localizer()) {
							receiving = true;
							nav_type = "ILS 2";
							if(nav2_object != null) {
								nav_id = nav2_object.ilt;
							}else {
								nav_id = "";
							}
							nav_course = Math.round(this.xpd.nav2_course());
							nav_dme = this.xpd.nav2_dme_nm();
						} else {
							receiving = false;
						}
//						if (nav2 != null && nav2.type != RadioNavBeacon.TYPE_NDB) {
//
//							
//						}
					}
				}
				if (receiving) {
					gc.drawText2(nav_type, 790, 1000, 32, 0, gc.color_lime, false, "left", g2);
					gc.drawText2(nav_id, 940, 1000, 32, 0, gc.color_markings, false, "left", g2);
					gc.drawText2("CRS", 920, 965, 26, 0, gc.color_markings, false, "left", g2);
					gc.drawText2(gc.df3.format(nav_course), 970, 965, 30, 0, gc.color_markings, false, "left", g2);
					gc.drawText2("DME", 920, 930, 26, 0, gc.color_markings, false, "left", g2);
					if (nav_dme > 0) {
						gc.drawText2(gc.dme_formatter.format(nav_dme), 1045, 930, 26, 0, gc.color_markings, false,
								"right", g2);
					} else {
						gc.drawText2("---", 1045, 930, 26, 0, gc.color_markings, false, "right", g2);
					}
				}
			}
		}
	}

	private void drawTopLeft() {

		//backgrounds - especially for CTR modes
		if(isMapCenter) {
			g2.setColor(Color.BLACK);
			g2.scale(gc.scalex, gc.scaley);

			g2.fillRect(0, 0, 255, 90);
			g2.fillRect(0, 89, 130, 100);

			g2.setTransform(original_trans);
		}


		if (this.xpd.irs_aligned()) {
			gc.drawText2("GS", 20, 1010, 24, 0, gc.color_markings, false, "left", g2);
			if (this.xpd.groundspeed() < 30) {
				gc.drawText2(gc.df3hash.format(this.xpd.groundspeed()), 112, 1005, 40, 0, gc.color_markings, false,
						"right", g2);
			} else {
				gc.drawText2(gc.df3hash.format(this.xpd.groundspeed()), 55, 1010, 32, 0, gc.color_markings, false,
						"left", g2);
			}

			String tas = "---";
			if (this.xpd.true_airspeed_knots() > 100) {
				tas = gc.df3hash.format(this.xpd.true_airspeed_knots());
			}
			gc.drawText2("TAS", 145, 1010, 24, 0, gc.color_markings, false, "left", g2);
			gc.drawText2(tas, 190, 1010, 32, 0, gc.color_markings, false, "left", g2);

			String wind_speed = "---";
			String wind_direction = "---";
			boolean display_arrow = false;

			if (this.xpd.true_airspeed_knots() > 101) {
				if (this.xpd.wind_speed_knots() > 6) {
					wind_speed = gc.df3hash.format(this.xpd.wind_speed_knots());
					wind_direction = gc.df3.format(this.xpd.wind_heading());
					display_arrow = true;
				}else {
					display_arrow = false;	
				}
			}

			gc.drawText2(wind_direction + "°" + "/" + wind_speed, 20, 970, 32, 0, gc.color_markings, false,
					"left", g2);
			temp_trans = g2.getTransform();
			g2.scale(gc.scalex, gc.scaley);
			g2.setStroke(new BasicStroke(4f, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
			g2.setColor(gc.color_markings);
			g2.translate(70, 140);
			g2.rotate(Math.toRadians((this.xpd.heading(pilot) - this.xpd.wind_heading() - 180f) * -1), 0, 0);
			if(display_arrow) {
				g2.drawLine(0, -40, -8, -35);
				g2.drawLine(0, -40, 0, 40);
				g2.drawLine(0, -40, 8, -35);
			}
			g2.setTransform(temp_trans);
		}
	}

	private void drawForeGround() {

		g2.scale(gc.scalex, gc.scaley);
		g2.setColor(Color.BLACK);
		if((this.xpd.efis_apt_mode(pilot) || this.xpd.efis_wpt_mode(pilot) || this.xpd.efis_sta_mode(pilot)) && (this.map_mode || this.map_ctr_mode || this.pln_mode)) {
			int[] xpoints = {0, 80, 0};
			int[] ypoints = {662, 662, 630};
			g2.fillPolygon(xpoints, ypoints, xpoints.length); //ARPT WPT STA
			g2.fillRect(0, 660, 80, 85); // ARPT WPT STA
		}
		if(this.xpd.efis_terr_on(pilot) || this.xpd.efis_wxr_on(pilot)) {
			g2.fillRect(0, 743, 100, 115); //Terrain WXR
		}

		g2.fillRect(0, 855, 100, 80); //TCAS

		g2.setTransform(original_trans);

		if(this.xpd.irs_aligned()) {

			if (this.xpd.efis_apt_mode(pilot) && (this.map_mode || this.map_ctr_mode || this.pln_mode)) {
				gc.drawText2("ARPT", 10f, 365f, 24f, 0f, gc.color_navaid, false, "left", g2);
			}
			if (this.xpd.efis_wpt_mode(pilot) && (this.map_mode || this.map_ctr_mode || this.pln_mode)) {
				gc.drawText2("WPT", 10f, 340f, 24f, 0f, gc.color_navaid, false, "left", g2);
			}
			if (this.xpd.efis_sta_mode(pilot) && (this.map_mode || this.map_ctr_mode || this.pln_mode)) {
				gc.drawText2("STA", 10f, 315f, 24f, 0f, gc.color_navaid, false, "left", g2);
			}
			if (this.xpd.efis_terr_on(pilot) && egpws_loaded) {
				if(this.map_mode || this.map_ctr_mode || this.vor_mode || this.app_mode) {
					gc.drawText2("TERR", 10f, 275f, 30f, 0f, gc.color_navaid, false, "left", g2);

					float terr_max = terrain.terrain_max;
					float terr_min = terrain.terrain_min;

					if(terrain.terrain_max > terrain.ref_alt + 2000) {
						peak_max_color = Color.RED;
					}else if(terrain.terrain_max > terrain.ref_alt -500) {
						peak_max_color = Color.ORANGE;
					}else {
						peak_max_color = Color.GREEN;
					}
					if(terrain.terrain_min > terrain.ref_alt + 2000) {
						peak_min_color = Color.RED;
					}else if(terrain.terrain_min > terrain.ref_alt -500) {
						peak_min_color = Color.ORANGE;
					}else {
						peak_min_color = Color.GREEN;
					}
					if (terrain.terrain_min < 0) terr_min = 0;

					peak_max_string = gc.df3.format(Math.round(terr_max/100));
					peak_min_string = gc.df3.format(Math.round(terr_min/100));

					if(peak_min_string != null) {
						gc.drawText2(peak_max_string, 10f, 240f, 30f, 0f, peak_max_color, false, "left", g2);
					}
					if(peak_min_string != null && Math.round(terr_min/100) > 0) {
						gc.drawText2(peak_min_string, 10f, 200f, 30f, 0f, peak_min_color, false, "left", g2);	
					}
				}
			}
			if (this.xpd.efis_wxr_on(pilot)) {
				gc.drawText2("WXR", 10f, 275f, 29f, 0f, gc.color_navaid, false, "left", g2);
			}

			if(this.xpd.transponder_pos() == 0) {
				gc.drawText2("TCAS TEST", 10f, 160f, 25f, 0f, gc.color_navaid, true, "left", g2);
			}
			if(this.xpd.transponder_pos() == 4) {
				gc.drawText2("TA ONLY", 10f, 160f, 25f, 0f, gc.color_navaid, true, "left", g2);
			}
			if(this.xpd.transponder_pos() == 5 && this.xpd.tcas_on(pilot) && !this.xpd.map_mode_pln(pilot)) {
				gc.drawText2("TFC", 10f, 160f, 25f, 0f, gc.color_navaid, false, "left", g2);
			}

		}
		if(!(this.xpd.transponder_pos() == 4 || this.xpd.transponder_pos() == 5 || this.xpd.transponder_pos() == 0)) {
			gc.drawText2("TCAS", 10f, 160f, 27f, 0f, gc.color_amber, false, "left", g2);
			gc.drawText2("OFF", 10f, 130f, 27f, 0f, gc.color_amber, false, "left", g2);	
		}

		//airplane symbol
		if(this.map_mode || this.map_ctr_mode || this.vor_mode || this.app_mode) {
			if(isMapCenter) {
				gc.displayImage(rs.img_plane_triangle, 0, 502, 122.050 + 328, g2);
			}else {
				gc.displayImage(rs.img_plane_triangle, 0, 502, 122.050, g2);
			}
		}else if (this.vor_ctr_mode || this.app_ctr_mode){
			gc.displayImage(rs.img_plane_vor_symbol, 0, 488f, 476.600f, g2);
		}
		//map range
		if(!this.pln_mode) {
			g2.translate(this.map_center_x, this.map_center_y);
			g2.rotate(Math.toRadians(track_line_rotate), 0, 0);
			g2.translate(-this.map_center_x, -this.map_center_y);
			if(isMapCenter) {
				if(!(this.app_ctr_mode || this.vor_ctr_mode)) {
					gc.drawText2(this.map_range, 525f, (1053f / 2) + 187.5f, 30f, 0f, gc.color_markings, true, "right", g2);
					gc.drawText2(this.map_range, 525f, (1053f / 2) - 187.5f, 30f, 0f, gc.color_markings, true, "right", g2);
				}
			}else {
				gc.drawText2(this.map_range, 525f, 595f, 30f, 0f, gc.color_markings, true, "right", g2);
			}
			g2.setTransform(original_trans);
		}

		//failure messages

		if(navFreqDisagree) {
			gc.drawText2("EFIS MODE/NAV FREQ DISAGREE", 535, 680, 36, 0, gc.color_amber, true, "center", g2);
		}


		if (!this.xpd.irs_aligned()) {
			gc.displayImage(rs.img_hdg_fail, 0, 496.441f, 995f, g2);
			gc.displayImage(rs.img_map_fail, 0, 496.441f, 398f, g2);
			gc.drawText2("TERR POS", 15, 560f, 26f, 0f, gc.color_amber, false, "left", g2);
		}

		if(this.xpd.tcas_traffic_ra(pilot)) {

			gc.drawText2("TRAFFIC", 900f, 580f, 32f, 0, Color.RED, true, "left", g2);

		}else if(this.xpd.tcas_traffic_ta(pilot)) {

			gc.drawText2("TRAFFIC", 900f, 580f, 32f, 0, gc.color_amber, true, "left", g2);
		}

		//xraas
		if(this.xpd.xraas_message() != null && this.preferences.get_preference(ZHSIPreferences.PREF_XRAAS_ENABLE).equals("true")) {
			Color xraas_color = gc.color_amber;
			if(this.xpd.xraas_color() == 0) {
				xraas_color = Color.GREEN;
			}
			gc.xraasText(this.xpd.xraas_message(), 540, 530, 130f, 0, xraas_color, false, "center", g2);
		}
	}

	private void updateStuff() {

		//set center lat/lon - will change for CTR waypoints
		if(!this.pln_mode) {
			this.center_lat = this.xpd.latitude();
			this.center_lon = this.xpd.longitude();
		}else {
			if(this.xpd.legs_step_ctr_idx(pilot) != 0) {
				if(this.xpd.fpln_active() && this.xpd.legs_mod_active()) {
//					this.center_lat = FMS.active_entries[this.xpd.legs_step_ctr_idx(pilot) - 1].getLat();
//					this.center_lon = FMS.active_entries[this.xpd.legs_step_ctr_idx(pilot) - 1].getLon();
					this.center_lat = this.xpd.mod_legs_lat()[this.xpd.legs_step_ctr_idx(pilot) - 1];
					this.center_lon = this.xpd.mod_legs_lon()[this.xpd.legs_step_ctr_idx(pilot) - 1];

				}else if(this.xpd.fpln_active()) {
//					this.center_lat = FMS.modified_entries[this.xpd.legs_step_ctr_idx(pilot) - 1].getLat();
//					this.center_lon = FMS.modified_entries[this.xpd.legs_step_ctr_idx(pilot) - 1].getLon();
					this.center_lat = this.xpd.legs_lat()[this.xpd.legs_step_ctr_idx(pilot) - 1];
					this.center_lon = this.xpd.legs_lon()[this.xpd.legs_step_ctr_idx(pilot) - 1];
					//System.out.println("center lat=" + this.center_lat + " center_lon=" + this.center_lon);
				}else {
					this.center_lat = this.xpd.mod_legs_lat()[this.xpd.legs_step_ctr_idx(pilot) - 1];
					this.center_lon = this.xpd.mod_legs_lon()[this.xpd.legs_step_ctr_idx(pilot) - 1];
				}

			}else {
				this.center_lat = this.xpd.latitude();
				this.center_lon = this.xpd.longitude();
			}
		}


		this.rose_radius = 758f * gc.scaley;

		// get tuned radios
		nav1 = this.xpd.get_tuned_navaid(1, pilot);
		nav2 = this.xpd.get_tuned_navaid(2, pilot);
		nav1_radio = this.xpd.get_nav_radio(1);
		nav2_radio = this.xpd.get_nav_radio(2);
		nav1_object = nav1_radio.get_radio_nav_object();
		nav2_object = nav2_radio.get_radio_nav_object();

		
		if(this.pilot == "cpt") {
			if((this.app_ctr_mode || this.app_mode) && !nav1_radio.freq_is_localizer() && nav1_object != null) {
				navFreqDisagree = true;
			}else if((this.vor_ctr_mode || this.vor_mode) && nav1_radio.freq_is_localizer() && nav1_object != null) {
				navFreqDisagree = true;
			}else {
				navFreqDisagree = false;
			}
		}else {
			if((this.app_ctr_mode || this.app_mode) && !nav2_radio.freq_is_localizer() && nav2_object != null) {
				navFreqDisagree = true;
			}else if((this.vor_ctr_mode || this.vor_mode) && nav2_radio.freq_is_localizer() && nav2_object != null) {
				navFreqDisagree = true;
			}else {
				navFreqDisagree = false;
			}
		}



		if(this.pilot == "cpt" && this.xpd.nav1_fromto() == 1) {
			vorTo = true;
		}else if(this.pilot == "fo" && this.xpd.nav2_fromto() == 1) {
			vorTo = true;
		}else {
			vorTo = false;
		}


		// map modes booleans
		if (this.xpd.efis_ctr_map(pilot) && this.xpd.irs_aligned()) {

			if (this.xpd.map_mode_app(pilot) && this.xpd.irs_aligned()) {
				app_mode = false;
				app_ctr_mode = true;
				vor_mode = false;
				vor_ctr_mode = false;
				map_mode = false;
				map_ctr_mode = false;
				pln_mode = false;
			} else if (this.xpd.map_mode_vor(pilot) && this.xpd.irs_aligned()) {
				app_mode = false;
				app_ctr_mode = false;
				vor_mode = false;
				vor_ctr_mode = true;
				map_mode = false;
				map_ctr_mode = false;
				pln_mode = false;
			} else if (this.xpd.map_mode_map(pilot)) {
				app_mode = false;
				app_ctr_mode = false;
				vor_mode = false;
				vor_ctr_mode = false;
				map_mode = false;
				map_ctr_mode = true;
				pln_mode = false;
			} else if (this.xpd.map_mode_pln(pilot) && this.xpd.irs_aligned()) {
				app_mode = false;
				app_ctr_mode = false;
				vor_mode = false;
				vor_ctr_mode = false;
				map_mode = false;
				map_ctr_mode = false;
				pln_mode = true;
			}
		} else {

			if (this.xpd.map_mode_app(pilot) && this.xpd.irs_aligned()) {
				app_mode = true;
				app_ctr_mode = false;
				vor_mode = false;
				vor_ctr_mode = false;
				map_mode = false;
				map_ctr_mode = false;
				pln_mode = false;
			} else if (this.xpd.map_mode_vor(pilot) && this.xpd.irs_aligned()) {
				app_mode = false;
				app_ctr_mode = false;
				vor_mode = true;
				vor_ctr_mode = false;
				map_mode = false;
				map_ctr_mode = false;
				pln_mode = false;
			} else if (this.xpd.map_mode_map(pilot)) {
				app_mode = false;
				app_ctr_mode = false;
				vor_mode = false;
				vor_ctr_mode = false;
				map_mode = true;
				map_ctr_mode = false;
				pln_mode = false;
			} else if (this.xpd.map_mode_pln(pilot) && this.xpd.irs_aligned()) {
				app_mode = false;
				app_ctr_mode = false;
				vor_mode = false;
				vor_ctr_mode = false;
				map_mode = false;
				map_ctr_mode = false;
				pln_mode = true;
			}
		}
		
		
		

		if (this.app_ctr_mode || this.vor_ctr_mode || this.map_ctr_mode || this.pln_mode) {
			this.map_center_x = this.getWidth() / 2;
			this.map_center_y = this.getHeight() / 2;
			this.isMapCenter = true;
		} else {
			this.map_center_x = this.getWidth() / 2;
			this.map_center_y = (this.getHeight() - (198.5f * gc.scaley));
			this.isMapCenter = false;
		}

		// get map orientation
	
		if (this.xpd.map_mode_map(pilot)) {
			if (this.xpd.on_gound()) {
				this.map_up = ((int) this.xpd.track() - this.xpd.magnetic_variation()) * -1;
			} else {
				this.map_up = (this.xpd.track() - this.xpd.magnetic_variation()) * -1;
			}
		} else if (this.xpd.map_mode_pln(pilot)) {
			this.map_up = 0.0f - this.xpd.magnetic_variation() * -1;
		} else {
			if (this.xpd.on_gound()) {
				this.map_up = ((int) this.xpd.heading(pilot) - this.xpd.magnetic_variation()) * -1;
			} else {
				this.map_up = (this.xpd.heading(pilot) - this.xpd.magnetic_variation()) * -1;
			}
		}

		if (this.isMapCenter) {

			switch (xpd.map_range(pilot)) {
			case 0:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "1.25";
					this.max_range = 2.5f;
					this.max_range_str = "2.5";
					this.pixels_per_nm = (375f / 2.5f) * gc.scaling_factor;
				} else {
					this.map_range = "2.5";
					this.max_range = 5f;
					this.max_range_str = "5";
					this.pixels_per_nm = (425f / 5f) * gc.scaling_factor;
				}
				break;
			case 1:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "2.5";
					this.max_range = 5f;
					this.max_range_str = "5";
					this.pixels_per_nm = (375f / 5f) * gc.scaling_factor;
				} else {
					this.map_range = "5";
					this.max_range = 10f;
					this.max_range_str = "10";
					this.pixels_per_nm = (425f / 10f) * gc.scaling_factor;
				}
				break;
			case 2:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "5";
					this.max_range = 10f;
					this.max_range_str = "10";
					this.pixels_per_nm = (375f / 10f) * gc.scaling_factor;
				} else {
					this.map_range = "10";
					this.max_range = 20f;
					this.max_range_str = "20";
					this.pixels_per_nm = (425f / 20f) * gc.scaling_factor;
				}
				break;
			case 3:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "10";
					this.max_range = 20f;
					this.max_range_str = "20";
					this.pixels_per_nm = (375f / 20f) * gc.scaling_factor;
				} else {
					this.map_range = "20";
					this.max_range = 40f;
					this.max_range_str = "40";
					this.pixels_per_nm = (425f / 40f) * gc.scaling_factor;
				}
				break;
			case 4:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "20";
					this.max_range = 40f;
					this.max_range_str = "40";
					this.pixels_per_nm = (375f / 40f) * gc.scaling_factor;
				} else {
					this.map_range = "40";
					this.max_range = 80f;
					this.max_range_str = "80";
					this.pixels_per_nm = (425f / 80f) * gc.scaling_factor;
				}
				break;
			case 5:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "40";
					this.max_range = 80f;
					this.max_range_str = "80";
					this.pixels_per_nm = (375f / 80f) * gc.scaling_factor;
				} else {
					this.map_range = "80";
					this.max_range = 160;
					this.max_range_str = "160";
					this.pixels_per_nm = (425f / 160f) * gc.scaling_factor;
				}
				break;
			case 6:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "80";
					this.max_range = 160f;
					this.max_range_str = "160";
					this.pixels_per_nm = (375f / 160f) * gc.scaling_factor;
				} else {
					this.map_range = "160";
					this.max_range = 320;
					this.max_range_str = "320";
					this.pixels_per_nm = (425f / 320f) * gc.scaling_factor;
				}
				break;
			case 7:

				if (this.map_ctr_mode || this.app_ctr_mode || this.vor_ctr_mode) {
					this.map_range = "160";
					this.max_range = 320f;
					this.max_range_str = "320";
					this.pixels_per_nm = (375f / 320f) * gc.scaling_factor;
				} else {
					this.map_range = "320";
					this.max_range = 640;
					this.max_range_str = "640";
					this.pixels_per_nm = (425f / 640f) * gc.scaling_factor;
				}
				break;
			}
		} else {
			switch (xpd.map_range(pilot)) {
			case 0:
				this.map_range = "2.5";
				this.max_range = 5;
				this.max_range_str = "5";
				this.pixels_per_nm = (758f / 5f) * gc.scaling_factor;
				break;
			case 1:
				this.map_range = "5";
				this.max_range = 10;
				this.max_range_str = "10";
				this.pixels_per_nm = (758f / 10f) * gc.scaling_factor;
				break;
			case 2:
				this.map_range = "10";
				this.max_range = 20;
				this.max_range_str = "20";
				this.pixels_per_nm = (758f / 20f) * gc.scaling_factor;
				break;
			case 3:
				this.map_range = "20";
				this.max_range = 40;
				this.max_range_str = "40";
				this.pixels_per_nm = (758f / 40f) * gc.scaling_factor;
				break;
			case 4:
				this.map_range = "40";
				this.max_range = 80;
				this.max_range_str = "80";
				this.pixels_per_nm = (758f / 80f) * gc.scaling_factor;
				break;
			case 5:
				this.map_range = "80";
				this.max_range = 160;
				this.max_range_str = "160";
				this.pixels_per_nm = (758f / 160f) * gc.scaling_factor;
				break;
			case 6:
				this.map_range = "160";
				this.max_range = 320;
				this.max_range_str = "320";
				this.pixels_per_nm = (758f / 320f) * gc.scaling_factor;
				break;
			case 7:
				this.map_range = "320";
				this.max_range = 640;
				this.max_range_str = "640";
				this.pixels_per_nm = (758f / 640f) * gc.scaling_factor;
				break;
			}
		}
		if(this.xpd.efis_terr(pilot) != old_terr_on_value) {
			terrain_cycle = 0;
			terrain_invert = false;
			old_terr_on_value = this.xpd.efis_terr(pilot);
		}

		if(this.xpd.efis_wxr(pilot) != old_wxr_on_value) {
			weather_cycle = 0;
			weather_invert = false;
			old_wxr_on_value = this.xpd.efis_wxr(pilot);
		}


		if(this.xpd.efis_map_ctr(pilot) != old_map_ctr_int) {
			old_map_ctr_int = this.xpd.efis_map_ctr(pilot);
			terrain_cycle = 0;
			terrain_invert = false;
			initial_sweep_required = true;
			initial_sweep_rotate = 180f;
			terrain_cycle = 0;
			weather_cycle = 0;
			terrain_invert = false;
			weather_invert = false;
			if(this.xpd.efis_terr_on(pilot)) {
				if(this.map_ctr_mode) {
					if(this.xpd.efis_terr_on(pilot) && egpws_loaded) {
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, true);
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, true);
					}
				}else {
					if(this.xpd.efis_terr_on(pilot) && egpws_loaded) {
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
					}
					if(this.xpd.efis_wxr_on(pilot) && ZHSIStatus.weather_receiving) {
						weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
						weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
					}
				}
			}
		}

		if(this.xpd.map_range(pilot) != old_map_range_value) {
			old_map_range_value = this.xpd.map_range(pilot);
			initial_sweep_required = true;
			initial_sweep_rotate = 180f;
			terrain_cycle = 0;
			weather_cycle = 0;
			terrain_invert = false;
			weather_invert = false;
			if(this.xpd.efis_terr_on(pilot)) {
				if(this.map_ctr_mode) {
					if(this.xpd.efis_terr_on(pilot) && egpws_loaded) {
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, true);
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, true);
					}
				}else {
					if(this.xpd.efis_terr_on(pilot) && ZHSIStatus.egpws_db_status.equals(ZHSIStatus.STATUS_EGPWS_DB_LOADED)) {
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
					}
					if(this.xpd.efis_wxr_on(pilot) && ZHSIStatus.weather_receiving) {
						weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
						weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
					}
				}
			}
		}
		if(this.xpd.map_mode(pilot) != old_map_mode_value) {
			old_map_mode_value = this.xpd.map_mode(pilot);
			initial_sweep_required = true;
			initial_sweep_rotate = 180f;
			terrain_cycle = 0;
			weather_cycle = 0;
			if(this.xpd.efis_terr_on(pilot)) {
				if(this.map_ctr_mode) {
					if(this.xpd.efis_terr_on(pilot) && egpws_loaded) {
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, true);
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, true);
					}
				}else {
					if(this.xpd.efis_terr_on(pilot) && egpws_loaded) {
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
						terrain.renderTerrain(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
					}
					if(this.xpd.efis_wxr_on(pilot) && ZHSIStatus.weather_receiving) {
						weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 1, false);
						weather.renderWeather(g2, pixels_per_nm, max_range, map_up, map_center_x, map_center_y, scaling_factor, 2, false);
					}
				}
			}
		}

		this.map_projection.setAcf(this.center_lat, this.center_lon);
		this.map_projection.setCenter(this.map_center_x, this.map_center_y);
		this.map_projection.setScale(this.pixels_per_nm);
		
//		this.map_projection.setPoint(this.xpd.latitude(), this.xpd.longitude());
//		plane_x = (int) this.map_projection.getX();
//		plane_y = (int) this.map_projection.getY();
		plane_x = (int) this.map_projection.getAirPlaneX(this.xpd.latitude(), this.xpd.longitude(), this.pixels_per_nm, this.map_center_x);
		plane_y = (int) this.map_projection.getAirPlaneY(this.xpd.latitude(), this.xpd.longitude(), this.pixels_per_nm, this.map_center_y);


	}
	
	private float zibo_x(float x) {
		float x_px = (map_center_x + (x * 75.5f) * gc.scalex);
		//float x_px = (map_center_x + ((x * 100) / 1.2f) * gc.scalex);
		return x_px;
		
	}
	
	private float zibo_y(float y) {
		
		if(isMapCenter) {
			y = y - 4.1f;
			if(this.xpd.efis_vsd_map(pilot)) {
				y = y - 1f;
			}
		}
		float y_px = (map_center_y - (y * 75.25f) * gc.scaley);
		//float y_px = (map_center_y - ((y * 100) / 1.33f) * gc.scaley);
		return y_px;
		
	}

	private float turn_radius(float turn_speed, float speed) {

		return Math.abs(speed / (turn_speed * 20.0f * (float) Math.PI));

	}
	
	private void draw_position_trend_vector_segment(Graphics2D g2, float turn_radius, float turn_speed, float pixels_per_nm, float vector_start, float vector_end) {

		
		
		g2.scale(gc.scalex, gc.scaley);
		g2.setStroke(rs.stroke3);
		g2.setTransform(original_trans);
		g2.setColor(rs.color_markings);
		
		//vector_start = vector_start * gc.scaling_factor;
		//vector_end = vector_end * gc.scaling_factor;
		
		//float turn_radius_pixels = turn_radius * (30f * gc.scaling_factor);
		
		float turn_radius_pixels = turn_radius * this.pixels_per_nm;
		

		
//		
//		System.out.println("x=" + (float) ((this.map_center_x) - (turn_radius_pixels * 2.0f)));
//		System.out.println("y=" + (float) (this.map_center_y - turn_radius_pixels));
//		System.out.println("w=" + turn_radius_pixels * 2f);
//		System.out.println("h=" + turn_radius_pixels * 2f);
//		System.out.println("start=" + vector_start * Math.abs(turn_speed));
//		System.out.println("extent=" + (vector_end-vector_start) * Math.abs(turn_speed));
//		System.out.println("---------------");
		
		
		  if (turn_speed >= 0) {
	            // right turn
	            g2.draw(new Arc2D.Float(
	                    this.map_center_x,
	                    this.map_center_y - turn_radius_pixels - (10f * gc.scaley),
	                    turn_radius_pixels * 2f,
	                    turn_radius_pixels * 2f,
	                    180.0f - (vector_end * turn_speed),
	                    (vector_end-vector_start) * turn_speed,
	                    Arc2D.OPEN));
	        } else {
	            // left turn
	            g2.draw(new Arc2D.Float(
	                    this.map_center_x - (turn_radius_pixels * 2.0f),
	                    this.map_center_y - turn_radius_pixels - (10f * gc.scaley),
	                    turn_radius_pixels * 2f,
	                    turn_radius_pixels * 2f,
	                    vector_start * Math.abs(turn_speed),
	                    (vector_end-vector_start) * Math.abs(turn_speed),
	                    Arc2D.OPEN));
		}


	}

	public void stopRender() {
		renderLoop.terminate();
	}

}
