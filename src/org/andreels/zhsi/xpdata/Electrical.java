/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;

import org.andreels.zhsi.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zhsi.ExtPlaneInterface.data.DataRef;
import org.andreels.zhsi.ExtPlaneInterface.util.Observer;

public class Electrical extends BaseDataClass {
	
	Observer<DataRef> electrical;
	
	private final String DC_VOLTMETER_VALUE = "sim/cockpit2/electrical/dc_voltmeter_value";
	private final String BATTERY_VOLTAGE = "sim/cockpit2/electrical/battery_voltage_indicated_volts";
	
	private final String BATTERY_AMPS = "sim/cockpit2/electrical/battery_amps";
	private final String BUS_LOAD_AMPS = "sim/cockpit2/electrical/bus_load_amps";
	
	private final String CPS_FREQ0 = "laminar/B738/ac_freq_mode0";
	private final String CPS_FREQ1 = "laminar/B738/ac_freq_mode1";
	private final String CPS_FREQ2 = "laminar/B738/ac_freq_mode2";
	private final String CPS_FREQ3 = "laminar/B738/ac_freq_mode3";
	private final String CPS_FREQ4 = "laminar/B738/ac_freq_mode4";
	private final String CPS_FREQ5 = "laminar/B738/ac_freq_mode5";
	
	private final String AC_VOLT_MODE1 = "laminar/B738/ac_volt_mode1";
	private final String AC_VOLT_MODE2 = "laminar/B738/ac_volt_mode2";
	private final String AC_VOLT_MODE3 = "laminar/B738/ac_volt_mode3";
	private final String AC_VOLT_MODE4 = "laminar/B738/ac_volt_mode4";
	private final String AC_VOLT_MODE5 = "laminar/B738/ac_volt_mode5";
	
	private final String GENERATOR_AMPS = "sim/cockpit2/electrical/generator_amps";
	private final String APU_GENERATOR_AMPS = "sim/cockpit2/electrical/APU_generator_amps";
	
	public float cps_freq0 = 0;
	public float cps_freq1 = 0;
	public float cps_freq2 = 0;
	public float cps_freq3 = 0;
	public float cps_freq4 = 0;
	public float cps_freq5 = 0;

	public float dc_volt_mode0 = 0;
	public float dc_volt_mode1 = 0;
	public float dc_volt_mode2 = 0;
	public float dc_amps_mode2 = 0;
	public float dc_amps_mode3 = 0;
	public float dc_amps_mode4 = 0;
	public float dc_amps_mode5 = 0;
	
	public float ac_volt_mode0 = 0;
	public float ac_volt_mode1 = 0;
	public float ac_volt_mode2 = 0;
	public float ac_volt_mode3 = 0;
	public float ac_volt_mode4 = 0;
	public float ac_volt_mode5 = 0;
	public float ac_amps_mode2 = 0;
	public float ac_amps_mode3 = 0;
	public float ac_amps_mode4 = 0;
	

	public Electrical(ExtPlaneInterface iface) {
		super(iface);
		this.drefs.add(DC_VOLTMETER_VALUE);
		this.drefs.add(BATTERY_VOLTAGE);
		this.drefs.add(CPS_FREQ0);
		this.drefs.add(CPS_FREQ1);
		this.drefs.add(CPS_FREQ2);
		this.drefs.add(CPS_FREQ3);
		this.drefs.add(CPS_FREQ4);
		this.drefs.add(CPS_FREQ5);
		this.drefs.add(AC_VOLT_MODE1);
		this.drefs.add(AC_VOLT_MODE2);
		this.drefs.add(AC_VOLT_MODE3);
		this.drefs.add(AC_VOLT_MODE4);
		this.drefs.add(AC_VOLT_MODE5);
		this.drefs.add(GENERATOR_AMPS);
		this.drefs.add(APU_GENERATOR_AMPS);
		this.drefs.add(BUS_LOAD_AMPS);
		this.drefs.add(BATTERY_AMPS);
		
		electrical = new Observer<DataRef>() {

			@Override
			public void update(DataRef object) {
				
				switch(object.getName()) {
				case CPS_FREQ0: cps_freq0 = Float.parseFloat(object.getValue()[0]);
				break;
				case CPS_FREQ1: cps_freq1 = Float.parseFloat(object.getValue()[0]);
				break;
				case CPS_FREQ2: cps_freq2 = Float.parseFloat(object.getValue()[0]);
				break;
				case CPS_FREQ3: cps_freq3 = Float.parseFloat(object.getValue()[0]);
				break;
				case CPS_FREQ4: cps_freq4 = Float.parseFloat(object.getValue()[0]);
				break;
				case CPS_FREQ5: cps_freq5 = Float.parseFloat(object.getValue()[0]);
				break;
				case AC_VOLT_MODE1: ac_volt_mode1 = Float.parseFloat(object.getValue()[0]);
				break;
				case AC_VOLT_MODE2: ac_volt_mode2 = Float.parseFloat(object.getValue()[0]);
				break;
				case AC_VOLT_MODE3: ac_volt_mode3 = Float.parseFloat(object.getValue()[0]);
				break;
				case AC_VOLT_MODE4: ac_volt_mode4 = Float.parseFloat(object.getValue()[0]);
				break;
				case AC_VOLT_MODE5: ac_volt_mode5 = Float.parseFloat(object.getValue()[0]);
				break;
				case BATTERY_VOLTAGE: 
					dc_volt_mode0 = Float.parseFloat(object.getValue()[1]);
					ac_volt_mode0 = Float.parseFloat(object.getValue()[1]);
					dc_volt_mode1 = Float.parseFloat(object.getValue()[0]);
				break;
				case DC_VOLTMETER_VALUE: dc_volt_mode2 = Float.parseFloat(object.getValue()[0]);
				break;
				case APU_GENERATOR_AMPS: ac_amps_mode3 = Float.parseFloat(object.getValue()[0]);
				break;
				case BATTERY_AMPS: dc_amps_mode2 = Float.parseFloat(object.getValue()[0]);
				break;
				case BUS_LOAD_AMPS:
					dc_amps_mode3 = Float.parseFloat(object.getValue()[0]);
					dc_amps_mode4 = Float.parseFloat(object.getValue()[1]);
					dc_amps_mode5 = Float.parseFloat(object.getValue()[2]);
				break;
				case GENERATOR_AMPS:
					ac_amps_mode2 = Float.parseFloat(object.getValue()[0]);
					ac_amps_mode4 = Float.parseFloat(object.getValue()[1]);
				break;

				}
				
			}
			
		};
		
	}

	@Override
	public void subscribeDrefs() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void includeDrefs() {
		
		for(String dref : drefs) {
			iface.includeDataRef(dref);
			iface.observeDataRef(dref, electrical);
		}	
		
	}

	@Override
	public void excludeDrefs() {
		
		for(String dref : drefs) {
			iface.excludeDataRef(dref);
			iface.unObserveDataRef(dref, electrical);
		}	
		
	}

}
