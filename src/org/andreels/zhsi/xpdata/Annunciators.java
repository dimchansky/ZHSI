/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;

import org.andreels.zhsi.AnnunciatorWindow;
import org.andreels.zhsi.ExtPlaneInterface.ExtPlaneInterface;
import org.andreels.zhsi.ExtPlaneInterface.data.DataRef;
import org.andreels.zhsi.ExtPlaneInterface.util.Observer;

public class Annunciators extends BaseDataClass {

	Observer<DataRef> Annunciators;
	
	AnnunciatorWindow aw;
	
	private final String LOW_FUEL_PRESS_C1 = "laminar/B738/annunciator/low_fuel_press_c1";
	private final String LOW_FUEL_PRESS_C2 = "laminar/B738/annunciator/low_fuel_press_c2";
	private final String LOW_FUEL_PRESS_L1 = "laminar/B738/annunciator/low_fuel_press_l1";
	private final String LOW_FUEL_PRESS_L2 = "laminar/B738/annunciator/low_fuel_press_l2";
	private final String LOW_FUEL_PRESS_R1 = "laminar/B738/annunciator/low_fuel_press_r1";
	private final String LOW_FUEL_PRESS_R2 = "laminar/B738/annunciator/low_fuel_press_r2";
	private final String APU_DUAL_BLEED = "laminar/B738/annunciator/dual_bleed";
	private final String WINDOW_HEAT_L_SIDE_ON = "laminar/B738/annunciator/window_heat_l_side";
	
	//laminar/b738/alert/pfd_pull_up
	
	public float low_fuel_press_c1 = 0;
	public float low_fuel_press_c2 = 0;
	public float low_fuel_press_l1 = 0;
	public float low_fuel_press_l2 = 0;
	public float low_fuel_press_r1 = 0;
	public float low_fuel_press_r2 = 0;
	public float apu_dual_bleed = 0;
	public float window_heat_l_side_on = 0;
	
	
	public Annunciators(ExtPlaneInterface iface) {
		super(iface);
		
		drefs.add(LOW_FUEL_PRESS_C1);
		drefs.add(LOW_FUEL_PRESS_C2);
		drefs.add(LOW_FUEL_PRESS_L1);
		drefs.add(LOW_FUEL_PRESS_L2);
		drefs.add(LOW_FUEL_PRESS_R1);
		drefs.add(LOW_FUEL_PRESS_R2);
		drefs.add(APU_DUAL_BLEED);
		drefs.add(WINDOW_HEAT_L_SIDE_ON);
		
		//this.aw = AnnunciatorWindow.getInstance();
		
		Annunciators = new Observer<DataRef>() { // create FMS objects when data changes

			@Override
			public void update(DataRef object) {
				
				switch(object.getName()) {
				case LOW_FUEL_PRESS_C1:
					low_fuel_press_c1 = Float.parseFloat(object.getValue()[0]);
					break;
				case LOW_FUEL_PRESS_C2:
					low_fuel_press_c2 = Float.parseFloat(object.getValue()[0]);
					break;
				case LOW_FUEL_PRESS_L1:
					low_fuel_press_l1 = Float.parseFloat(object.getValue()[0]);
					break;
				case LOW_FUEL_PRESS_L2:
					low_fuel_press_l2 = Float.parseFloat(object.getValue()[0]);
					break;
				case LOW_FUEL_PRESS_R1:
					low_fuel_press_r1 = Float.parseFloat(object.getValue()[0]);
					break;
				case LOW_FUEL_PRESS_R2:
					low_fuel_press_r2 = Float.parseFloat(object.getValue()[0]);
					break;
				case APU_DUAL_BLEED:
					apu_dual_bleed = Float.parseFloat(object.getValue()[0]);
					break;
				case WINDOW_HEAT_L_SIDE_ON:
					window_heat_l_side_on = Float.parseFloat(object.getValue()[0]);
					break;
				}
				//aw.updateAnnuns();
			}
		};

	}


	@Override
	public void subscribeDrefs() {
		
	}


	@Override
	public void includeDrefs() {
		
		for(String dref : drefs) {
			iface.includeDataRef(dref);
			iface.observeDataRef(dref, Annunciators);
		}	
		
	}


	@Override
	public void excludeDrefs() {
		
		for(String dref : drefs) {
			iface.excludeDataRef(dref);
			iface.unObserveDataRef(dref, Annunciators);
		}	
	
	}
	
}
