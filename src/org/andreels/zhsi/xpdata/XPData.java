/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.xpdata;

import org.andreels.zhsi.navdata.NavigationObject;
import org.andreels.zhsi.navdata.NavigationRadio;
import org.andreels.zhsi.navdata.RadioNavBeacon;

public interface XPData {
	
	public boolean sim_paused();
	
	public boolean power_on();
	
	public boolean on_gound();
	
	public boolean ac_standby_on();
	
	public boolean dc_standby_on();
	
	public boolean irs_aligned();
	
	public float airspeed(String pilot);
	
	public float airspeed_mach();
	
	public float airspeed_acceleration();
	
	public float groundspeed();
	
	public float wind_speed_knots();
	
	public float wind_heading();
	
	public float true_airspeed_knots();
	
	public float aoa();
	
	public float heading(String pilot);
	
	public float altitude(String pilot);
	
	public float radio_alt_feet();
	
	public float latitude();
	
	public float longitude();
	
	public float elevation_msl();
	
	public float roll();
	
	public float pitch();
	
	public float slip_deg();
	
	public float vvi(String pilot);
	
	public float track();
	
	public float magnetic_variation();
	
	public float mcp_speed();
	
	public int mcp_alt();
	
	public float mcp_heading();
	
	public float mcp_vvi();
	
	public float max_speed();
	public float max_maneuver_speed();
	public boolean max_maneuver_speed_show();
	
	public float min_speed();
	public boolean min_speed_show();
	public float min_maneuver_speed();
	public boolean min_maneuver_speed_show();


	public boolean alt_disagree();
	
	public boolean ias_disagree();
	
	public boolean cmd_status(String pilot);
	
	public boolean fd_status(String pilot);
	

	public boolean fd_mode();
	public boolean fd_pitch_show(String pilot);
	public boolean fd_roll_show(String pilot);
	
	public boolean fd_rec(String pilot);
	
	public float du_brightness(String title);
	
	public int main_panel_du(String pilot);
	
	public int lower_panel_du(String pilot);
	
	//Aircraft
	public boolean gear_up();
	public float yaw_rotation();
	public float fpv_alpha();
	public float fpv_beta();
	
	//Avionics
	public int vspeed();
	public boolean vspeed_digit_show(String pilot);
	public boolean vspeed_vref_show(String pilot);
	public int vspeed_mode();
	public boolean nd_vert_path();
	public boolean pfd_vert_path(String pilot);
	public float vnav_err();
	public float rwy_altitude();
	public boolean pfd_rwy_show();
	
	
	//TCAS
	public float tcas_x(String pilot, int index);
	public float tcas_y(String pilot, int index);
	public String tcas_alt(int index);
	public String tcas_alt_fo(int index);
	public boolean tcas_ai_show(String pilot, int index);
	public boolean tcas_show(String pilot);
	public float tcas_type_show(String pilot, int index);
	public float tcas_alt_dn_up_show(String pilot, int index);
	public float tcas_arrow_dn_up_show(String pilot, int index);
	public boolean tcas_traffic_ra(String pilot);
	public boolean tcas_traffic_ta(String pilot);
	
	//Environment
	public float outside_air_temp_c();
	
	//PFD
	public float flaps_1();
	public boolean flaps_1_show();
	public float flaps_5();
	public boolean flaps_5_show();
	public float flaps_15();
	public boolean flaps_15_show();
	public float flaps_25();
	public boolean flaps_25_show();
	public float flaps_up();
	public boolean flaps_up_show();
	public boolean spd_80_show();
	
	
	//ND
	public boolean hdg_bug_line(String pilot);
	public float green_arc(String pilot);
	public boolean green_arc_show(String pilot);
	public int fmc_source(String pilot);
	public boolean show_hold(String pilot, int index);
	public int hold_type(String pilot, int index);
	public float hold_dist(String pilot, int index);
	public int hold_crs(String pilot, int index);
	public float hold_x(String pilot, int index);
	public float hold_y(String pilot, int index);
	
	//AP
	public int course (String pilot);
	public int back_course (String pilot);
	public int pfd_spd_mode();
	public boolean rec_thr_modes();
	public boolean rec_thr2_modes();
	public int pfd_hdg_mode();
	public boolean rec_hdg_mode();
	public int pfd_hdg_mode_arm();
	public int pfd_alt_mode();
	public boolean rec_alt_modes();
	public int pfd_alt_mode_arm();
	public float ap_fd_roll_deg();
	public float ap_fd_pitch_deg();
	public boolean single_ch();
	public boolean single_ch_rec_mode();
	public int flare_status();
	public int pfd_mode(String pilot);
	public boolean vs_mode();
	public float mcp_speed_mach();
	public boolean mcp_speed_is_mach();
	public float nps_deviation();
	public boolean cws_r_status();
	public boolean cws_p_status();
	
	//EFIS
	public float baro_sel_in_hg(String pilot);
	public boolean baro_in_hpa(String pilot);
	public boolean baro_std_set(String pilot);
	public boolean efis_rst(String pilot);
	public int map_range(String pilot);
	public int map_mode(String pilot);
	public boolean map_mode_app(String pilot);
	public boolean map_mode_vor(String pilot);
	public boolean map_mode_map(String pilot);
	public boolean map_mode_pln(String pilot);
	public boolean efis_apt_mode(String pilot);
	public boolean efis_sta_mode(String pilot);
	public boolean efis_wpt_mode(String pilot);
	public boolean tcas_on(String pilot);
	public int vor1_pos(String pilot);
	public int vor2_pos(String pilot);
	public boolean efis_pos_on(String pilot);
	public int minimums(String pilot);
	public int minimums_mode(String pilot);
	public boolean efis_terr_on(String pilot);
	public int efis_terr(String pilot);
	public boolean efis_wxr_on(String pilot);
	public int efis_wxr(String pilot);
	public boolean efis_ctr_map(String pilot);
	public int efis_map_ctr(String pilot);
	public boolean efis_vsd_map(String pilot);
	public boolean efis_data_on(String pilot);
	public boolean efis_mtrs_on(String pilot);
	public boolean efis_fpv_show(String pilot);

	
	//FMS
	public float anp();
	public float rnp();
	public int trans_alt();
	public String active_waypoint();
	public boolean fpln_active();
	public String origin_arpt();
	public String dest_arpt();
	public float fms_vref();
	public String fpln_nav_id_eta(); 
	public float fpln_nav_id_dist();
	public float fms_vr_set();
	public float fms_v1_set();
	public float fms_v2_15();
	public int approach_speed();
	public int approach_flaps();
	public int fms_legs_num2();
	public int fms_legs_num();
	public boolean legs_mod_active();
	public int legs_step_ctr_idx(String pilot);
	public float ils_rotate0(String pilot);
	public float ils_rotate(String pilot);
	public boolean ils_show0(String pilot);
	public boolean ils_show(String pilot);
	public int missed_app_wpt_idx();
	public boolean missed_app_active();
	//active waypoint data
	public int active_num_of_wpts();
	public String[] active_waypoints();
	public float[] legs_lat();
	public float[] legs_lon();
	public float[] legs_alt_calc();
	public float[] legs_alt_rest1();
	public float[] legs_alt_rest2();
	public float[] legs_alt_rest_type();
	public float[] legs_type();
	//modified waypoint data
	public int mod_num_of_wpts();
	public String[] mod_waypoints();
	public float[] mod_legs_lat();
	public float[] mod_legs_lon();
	public float[] mod_legs_alt_calc();
	public float[] mod_legs_alt_rest1();
	public float[] mod_legs_alt_rest2();
	public float[] mod_legs_alt_rest_type();
	public float[] mod_legs_type();
	public int vnav_idx();
	public boolean direct_active();
	public float fms_track();
	public float fms_course();
	public boolean fms_track_active();
	public float rte_edit_rot_act();

	
	//NAVDATA
	public float apt_x(String pilot, int index);
	public float apt_y(String pilot, int index);
	public String apt_id(String pilot, int index);
	public boolean apt_enable(String pilot, int index);
	public String tc_id(String pilot);
	public boolean tc_show(String pilot);
	public float tc_x(String pilot);
	public float tc_y(String pilot);
	public String decel_id(String pilot);
	public boolean decel_show(String pilot);
	public float decel_x(String pilot);
	public float decel_y(String pilot);
	public String td_id(String pilot);
	public boolean td_show(String pilot);
	public float td_x(String pilot);
	public float td_y(String pilot);
	public boolean[] fix_show(String pilot);
	public String[] fix_id(String pilot);
	//public boolean[] fix_id_fo_show();
	public float[] fix_dist_0();
	public float[] fix_dist_1();
	public float[] fix_dist_2();
	public float[] fix_rad_dist_0();
	public float[] fix_rad_dist_1();
	public float[] fix_rad_dist_2();
	public float ils_x(String pilot);
	public float ils_y(String pilot);
	public float ils_x0(String pilot);
	public float ils_y0(String pilot);
	public String ils_runway();
	public String ils_runway0();

	
	//RADIOS
	public float nav1_freq();
	public float nav1_freq_standby();
	public String nav1_id();
	public float nav1_rel_bearing();
	public float nav1_dme_nm();
	public float nav1_time_sec();
	public int nav1_fromto();
	public float nav1_hdef_dot();
	public float nav1_vdef_dot();
	public int nav1_cdi();
	public float adf1_freq();
	public String adf1_id();
	public float adf1_rel_bearing();
	public float adf1_dme_nm();
	public float nav1_course();
	public float nav2_course();
	public int nav1_type();
	public int nav2_type();
	public float rmi_arrow1();
	public float rmi_arrow2();
	public float rmi_arrow1_no_avail();
	public float rmi_arrow2_no_avail();
	
	public float nav2_freq();
	public float nav2_freq_standby();
	public String nav2_id();
	public float nav2_rel_bearing();
	public float nav2_dme_nm();
	public float nav2_time_sec();
	public int nav2_fromto();
	public float nav2_hdef_dot();
	public float nav2_vdef_dot();
	public int nav2_cdi();
	public float adf2_freq();
	public String adf2_id();
	public float adf2_rel_bearing();
	public float adf2_dme_nm();
	
	//ENGINES
	public float eng1_n1();
	public float eng2_n1();
	public float eng1_n2();
	public float eng2_n2();
	public float eng1_n1_req();
	public float eng2_n1_req();
	public float eng1_egt();
	public float eng2_egt();
	public float fuel_qty_kg_1();
	public float fuel_qty_kg_2();
	public float fuel_qty_kg_c();
	public float fuel_flow1();
	public float fuel_flow2();
	public boolean eng_start_value_1();
	public boolean eng_start_value_2();
	public boolean eng_oil_bypass_1();
	public boolean eng_oil_bypass_2();
	public boolean oil_pressure_annun_1();
	public boolean oil_pressure_annun_2();
	public float oil_qty_1();
	public float oil_qty_2();
	public float oil_temp_c_1();
	public float oil_temp_c_2();
	public float eng_oil_press_1();
	public float eng_oil_press_2();
	public float eng_n1_bug_1();
	public float eng_n1_bug_2();
	public String n1_mode();
	public boolean reverser1_moved();
	public boolean reverser1_deployed();
	public boolean reverser2_moved();
	public boolean reverser2_deployed();
	public float eng1_tai();
	public float eng2_tai();
	public boolean egt_redline1();
	public boolean egt_redline2();
	public boolean fuel_flow_used_show();
	public float fuel_flow_used1();
	public float fuel_flow_used2();
	public boolean eng1_out();
	public boolean eng2_out();
	
	
	//Buttons and Switches
	public int spd_ref();
	public int spd_ref_adjust();
	public boolean mfd_eng();
	public boolean mfd_sys();
	public boolean fuel_tank_ctr1_pos();
	public boolean fuel_tank_ctr2_pos();
	public boolean fuel_tank_lft1_pos();
	public boolean fuel_tank_lft2_pos();
	public boolean fuel_tank_rgt1_pos();
	public boolean fuel_tank_rgt2_pos();
	public int transponder_pos();
	public int ac_power_knob();
	public int dc_power_knob();
	
	//Controls
	public float throttle_axis();
	
	//Annunciators
	public boolean low_fuel_press_c1();
	public boolean low_fuel_press_c2();
	public boolean low_fuel_press_l1();
	public boolean low_fuel_press_l2();
	public boolean low_fuel_press_r1();
	public boolean low_fuel_press_r2();
	
	//Systems
	public float brake_temp_l_in();
	public float brake_temp_l_out();
	public float brake_temp_r_in();
	public float brake_temp_r_out();	
	public float hyd_a_qty();
	public float hyd_b_qty();
	public int hyd_a_pressure();
	public int hyd_b_pressure();
	public float left_elevator_deflection();
	public float right_elevator_deflection();
	public float left_ail_deflection();
	public float right_ail_deflection();
	public float rudder_deflection();
	public float left_splr_deflection();
	public float right_splr_deflection();
	public float stab_trim_deflection();
	public float flaps_l_deflection();
	public float flaps_r_deflection();
	public String xraas_message();
	public int xraas_color();
	public boolean windsheer();
	public boolean pullup();
	public float thr_lvr1();
	public float thr_lvr2();
	
	//ISFD
	public float isfd_altitude();
	public float isfd_alt_baro();
	public int isfd_mode();
	public boolean isfd_std_mode();
	
	//Clocks
	public int time_zulu_hrs();
	public int time_zulu_min();
	public int time_local_hrs();
	public int time_local_min();
	public int time_day();
	public int time_month();
	public int chrono_mode(String pilot);
	public float chrono_needle(String pilot);
	public int chrono_minute(String pilot);
	public int chrono_display_mode(String pilot);
	public int chrono_et_mode(String pilot);
	public int chrono_et_hrs(String pilot);
	public int chrono_et_min(String pilot);
	
	//electrical
	public float cps_freq0();
	public float cps_freq1();
	public float cps_freq2();
	public float cps_freq3();
	public float cps_freq4();
	public float cps_freq5();
	//
	public float dc_volt_mode0();
	public float dc_volt_mode1();
	public float dc_volt_mode2();
	public float dc_amps_mode2();
	public float dc_amps_mode3();
	public float dc_amps_mode4();
	public float dc_amps_mode5();
	//
	public float ac_volt_mode0();
	public float ac_volt_mode1();
	public float ac_volt_mode2();
	public float ac_volt_mode3();
	public float ac_volt_mode4();
	public float ac_volt_mode5();
	public float ac_amps_mode2();
	public float ac_amps_mode3();
	public float ac_amps_mode4();
	
	//irs
	public String irs_left_string();
	public String irs_right_string();
	public boolean irs_decimals_show();
	

	
	
	public float rough_distance_to(NavigationObject nav_object);
	public RadioNavBeacon get_tuned_navaid(int bank, String pilot);
	public NavigationRadio get_nav_radio(int bank);
}
