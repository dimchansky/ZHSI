/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.fms;

public class ActiveLegs {
	
	public int id;
	public float startLat;
	public float endLat;
	public float startLon;
	public float endLon;
	public String startName;
	public String endName;
	
	public ActiveLegs(int id) {
		this.id = id;
		this.startLat = 0.0f;
		this.endLat = 0.0f;
		this.startLon = 0.0f;
		this.endLon = 0.0f;
		this.startName = "";
		this.endName = "";
	}
	
	public ActiveLegs(int id, float startLat, float endLat, float startLon, float endLon, String startName, String endName) {
		this.id = id;
		this.startLat = startLat;
		this.endLat = endLat;
		this.startLon = startLon;
		this.endLon = endLon;
		this.startName = startName;
		this.endName = endName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public float getStartLat() {
		return startLat;
	}

	public void setStartLat(float startLat) {
		this.startLat = startLat;
	}

	public float getEndLat() {
		return endLat;
	}

	public void setEndLat(float endLat) {
		this.endLat = endLat;
	}

	public float getStartLon() {
		return startLon;
	}

	public void setStartLon(float startLon) {
		this.startLon = startLon;
	}

	public float getEndLon() {
		return endLon;
	}

	public void setEndLon(float endLon) {
		this.endLon = endLon;
	}

	public String getStartName() {
		return startName;
	}

	public void setStartName(String startName) {
		this.startName = startName;
	}

	public String getEndName() {
		return endName;
	}

	public void setEndName(String endName) {
		this.endName = endName;
	}

}
