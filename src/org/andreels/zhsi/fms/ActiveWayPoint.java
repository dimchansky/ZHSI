/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */
package org.andreels.zhsi.fms;

public class ActiveWayPoint {
	
	int id;
	String name; //will get from laminar/B738/fms/legs - string.split
	float lat; //laminar/B738/fms/legs_lat
	float lon; //laminar/B738/fms/legs_lon
	float type; //laminar/B738/fms/legs_type - currently empty ?
	int altitude; //laminar/B738/fms/legs_alt_calc 
	float eta; // used for data display - no dataref yet, for now always be 0
	float alt_rest; //alt restriction for below, above and destination runway
	float alt_rest_2; //alt restriction for between (block)
	float alt_rest_type; // 1= below, 2 = above, 3 = between, 4 = destination runway
	boolean active; //represents the waypoint the airplane is currently navigating too - can cross check with : laminar/B738/fms/fpln_nav_id
	
	public ActiveWayPoint() {
		super();
		this.id = 0;
		this.name = null;
		this.lat = 0;
		this.lon = 0;
		this.type = 0;
		this.altitude = 0;
		this.eta = 0f;
		this.alt_rest = 0f;
		this.alt_rest_2 = 0f;
		this.alt_rest_type = 0f;
		this.active = false;
	}

	public ActiveWayPoint(int id, String name, float lat, float lon, float type, int altitude, float eta, float alt_rest, float alt_rest_2, float alt_rest_type, boolean active) {
		super();
		this.id = id;
		this.name = name;
		this.lat = lat;
		this.lon = lon;
		this.type = type;
		this.altitude = altitude;
		this.eta = eta;
		this.alt_rest = alt_rest;
		this.alt_rest_2 = alt_rest_2;
		this.alt_rest_type = alt_rest_type;
		this.active = active;
	}

	public float getAlt_rest() {
		return alt_rest;
	}

	public void setAlt_rest(float alt_rest) {
		this.alt_rest = alt_rest;
	}

	public float getAlt_rest_2() {
		return alt_rest_2;
	}

	public void setAlt_rest_2(float alt_rest_2) {
		this.alt_rest_2 = alt_rest_2;
	}

	public float getAlt_rest_type() {
		return alt_rest_type;
	}

	public void setAlt_rest_type(float alt_rest_type) {
		this.alt_rest_type = alt_rest_type;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getLat() {
		return lat;
	}

	public void setLat(float lat) {
		this.lat = lat;
	}

	public float getLon() {
		return lon;
	}

	public void setLon(float lon) {
		this.lon = lon;
	}
	
	
	public float getType() {
		return type;
	}

	public void setType(float f) {
		this.type = f;
	}

	public int getAltitude() {
		return altitude;
	}

	public void setAltitude(int altitude) {
		this.altitude = altitude;
	}

	public float getEta() {
		return eta;
	}

	public void setEta(float eta) {
		this.eta = eta;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
