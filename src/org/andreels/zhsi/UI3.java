/**
 * 
 * Copyright (C) 2018  Andre Els (https://www.facebook.com/sum1els737)
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author Andre Els
 * 
 */

package org.andreels.zhsi;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JSeparator;

import java.util.logging.Logger;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.event.ItemListener;
import java.net.URL;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.ItemEvent;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.andreels.zhsi.Instruments.Chrono;
import org.andreels.zhsi.gauges.FlapGauge;
import org.andreels.zhsi.navdata.Airport;
import org.andreels.zhsi.navdata.NavigationObjectRepository;
import org.andreels.zhsi.xpdata.XPData;

public class UI3 extends JFrame {


	private static final long serialVersionUID = 1L;
	
	private ZHSIPreferences preferences;
	//private LoadResources rs;
	private ModelFactory model_instance;
	private XPData xpd;
	private NavigationObjectRepository nor;
	
	//private TrippleDes td;
	private static final String _KEY = "-.yrJ@F^+8<Fu:[k{";
	
	private String reg_status = " - Unregistered";
	
	private Image logo_image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("ZHSI_logo.png"));
	
	private static Logger logger = Logger.getLogger("org.andreels.zhsi");
	
	private About aboutDialog = null;
	private Settings settingsDialog = null;
	
	private DisplayUnit cptoutbd = null;
	private DisplayUnit cptinbd = null;
	private DisplayUnit fooutbd = null;
	private DisplayUnit foinbd = null;
	private DisplayUnit upeicas = null;
	private DisplayUnit loeicas = null;
	private FlapGauge flapGauge = null;
	private Instrument isfd = null;
	private Instrument irs = null;
	private Instrument elecPanel = null;
	private Instrument cptChrono = null;
	private Instrument foChrono = null;
	
	Desktop desktop = null;
	
	private JPanel contentPane;
	private JMenuBar menuBar;
	private JPanel statusBar;
	private JMenu mnDisplays;
	private JCheckBoxMenuItem chckboxCptOutDu;
	private JCheckBoxMenuItem chckboxCptInDu;
	private JSeparator separator;
	private JCheckBoxMenuItem chckboxFoOutDu;
	private JCheckBoxMenuItem chckboxFoInDu;
	private JSeparator separator_1;
	private JCheckBoxMenuItem chckboxUpperDu;
	private JCheckBoxMenuItem chckboxLowerDu;
	private JMenu mnOptions;
	private JCheckBoxMenuItem chckboxDuAlwaysOnTop;
	private JMenu mnFuelUnits;
	private JCheckBoxMenuItem chckboxFuelKgs;
	private JCheckBoxMenuItem chckboxFuelLbs;
	private JMenu mnFuelDisplay;
	private JCheckBoxMenuItem chckboxFuelSidebySide;
	private JCheckBoxMenuItem chckboxFuelOverUnder;
	private JMenuItem mntmSettings;
	private JSeparator separator_3;
	private JSeparator separator_4;
	private JLabel lblXPStatus;
	private JLabel lblNavDBStatus;
	private JLabel lblEgpwsStatus;
	private JLabel lblWeatherStatus;
	private JLabel statusXP;
	private JLabel statusNavDb;
	private JLabel statusEgpws;
	private JLabel statusWeather;
	private JMenu mnHelp;
	private JMenuItem mntmAbout;
	private JSeparator separator_2;
	private JMenuItem mntmCreateIssue;
	private JMenuItem mntmGetHelp;
	private JSeparator separator_6;
	private JCheckBoxMenuItem chckboxRoundAltimeter;
	private JSeparator separator_7;
	private JCheckBoxMenuItem chckboxTrimIndicator;
	private JMenu mnGauges;
	private JCheckBoxMenuItem chckboxFlaps;
	private JPanel dashBoardPanel;
	private JPanel nearestAirportPanel;
	private JPanel latLonPanel;
	private JPanel altitudePanel;
	private JPanel iasPanel;
	private JPanel headingTrackPanel;
	private JPanel pitchPanel;
	private JPanel bankPanel;
	private JLabel lblNearestAirport;
	private JLabel lblNearestAirportValue;
	private JLabel lblLatitudeLongtitude;
	private JLabel lblLatLonValue;
	private JLabel lblAltitude;
	private JLabel lblAltitudeValue;
	private JLabel lblIas;
	private JLabel lblIasValue;
	private JLabel lblHeadingTrack;
	private JLabel lblHeadingTrackValue;
	private JLabel lblPitch;
	private JLabel lblPitchValue;
	private JLabel lblBank;
	private JLabel lblBankValue;
	private JCheckBoxMenuItem chckboxXRaas;
	private JSeparator separator_8;
	private JMenu mnWeatherRadarDetail;
	private JCheckBoxMenuItem chckboxWeatherLow;
	private JCheckBoxMenuItem chckboxWeatherMedium;
	private JCheckBoxMenuItem chckboxWeatherHigh;
	private JMenu mnTerrainDisplayDetail;	
	private JCheckBoxMenuItem chckboxTerrainLow;	
	private JCheckBoxMenuItem chckboxTerrainMedium;	
	private JCheckBoxMenuItem chckboxTerrainHigh;
	private JSeparator separator_9;
	private JCheckBoxMenuItem chckbxHwThrottlePos;
	private JCheckBoxMenuItem chckbxCompactDisplay;
	private JSeparator separator_10;
	private JMenuItem mntmRemoveNag;
	private JMenuItem mntmRegister;
	private JCheckBoxMenuItem chckbxUiMinimized;
	private JMenuItem mnInstruments;
	private JCheckBoxMenuItem chckbxIsfd;
	private JCheckBoxMenuItem chckbxCptChrono;
	private JCheckBoxMenuItem chckbxFoChrono;
	private JMenu mnInstDisplays;
	private JCheckBoxMenuItem chckbxElecDisplay;
	private JCheckBoxMenuItem chckbxIRSDisplay;
	

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
//					UI3 frame = new UI3();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 * @param title 
	 * @param model_instance 
	 */
	public UI3(ModelFactory model_instance, String title) {
		this.model_instance = model_instance;
		this.xpd = this.model_instance.getInstance();
		//this.rs = LoadResources.getInstance();
		this.preferences = ZHSIPreferences.getInstance();
		
		settingsDialog = new Settings();
		this.setIconImage(this.logo_image);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_UI_POS_X).equals("none")) {
			setBounds(100, 100, 600, 400);
			this.setLocationRelativeTo(null);
		}else {
			setBounds(Integer.parseInt(this.preferences.get_preference(ZHSIPreferences.PREF_UI_POS_X)), Integer.parseInt(this.preferences.get_preference(ZHSIPreferences.PREF_UI_POS_Y)), 600, 400);
		}
		contentPane = new JPanel();
		//contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		//this.rs.setBorder(null);
		//this.rs.setBounds(0, 0, 0, 0);
		this.nor = NavigationObjectRepository.get_instance();
		

		
//		try {
//			td = new TrippleDes();
//		} catch (Exception e1) {
//			// TODO Auto-generated catch block
//			e1.printStackTrace();
//		}
//	
//	    String key = preferences.get_preference(ZHSIPreferences.USER_KEY);
//		String target = preferences.get_preference(ZHSIPreferences.USER_FIRST_NAME) + "-" + preferences.get_preference(ZHSIPreferences.USER_LAST_NAME) + "-" + preferences.get_preference(ZHSIPreferences.USER_EMAIL) + _KEY + "ZHSI version: 0.0.1o";
//	    String decrypted_key = td.decrypt(key);
//
//	
//	    if(decrypted_key.equals(target)) {
//	    	ZHSIStatus.non_commercial_registered = true;
//	    	ZHSIStatus.user_name = preferences.get_preference(ZHSIPreferences.USER_FIRST_NAME);
//	    	ZHSIStatus.user_lastname = preferences.get_preference(ZHSIPreferences.USER_LAST_NAME);
//	    	ZHSIStatus.user_email = preferences.get_preference(ZHSIPreferences.USER_EMAIL);
//	    	reg_status = " - Registered to " + ZHSIStatus.user_name + " " + ZHSIStatus.user_lastname;
//	    	logger.info("License Key is good !");
//	    }else {
//	    	ZHSIStatus.non_commercial_registered = false;
//	    	ZHSIStatus.user_name = "UNREGISTERED";
//	    	ZHSIStatus.user_lastname = "UNREGISTERED";
//	    	ZHSIStatus.user_email = "none";
//	    	logger.info("ERROR: Invalid License Key!!");
//	    }
	    
	    this.setTitle(title + reg_status);
	    aboutDialog = new About(title);

		this.addComponentListener(new ComponentListener() {

			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentMoved(ComponentEvent e) {

				preferences.set_preference(ZHSIPreferences.PREF_UI_POS_X, Integer.toString(e.getComponent().getLocation().x));
				preferences.set_preference(ZHSIPreferences.PREF_UI_POS_Y, Integer.toString(e.getComponent().getLocation().y));
				
			}

			@Override
			public void componentResized(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
		

		
		menuBar = new JMenuBar();
		contentPane.add(menuBar, BorderLayout.NORTH);
		
		mnDisplays = new JMenu("Displays");
		menuBar.add(mnDisplays);
		
		chckboxCptOutDu = new JCheckBoxMenuItem("Captain Outboard DU");
		chckboxCptOutDu.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (cptoutbd == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									cptoutbd = new DisplayUnit(model_instance, "Captain OutBoard Display");
									cptoutbd.setVisible(true);
									DUHeartbeat cptoutbd_heartbeat = new DUHeartbeat(cptoutbd, 500);
									cptoutbd_heartbeat.start();
									preferences.set_preference(ZHSIPreferences.PREF_CPT_OUTBD_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (cptoutbd != null) {
						cptoutbd.dispose();
						//cptoutbd_heartbeat.stop();
						cptoutbd = null;
						preferences.set_preference(ZHSIPreferences.PREF_CPT_OUTBD_ENABLE, "false");
					}
				}
			}
		});
		mnDisplays.add(chckboxCptOutDu);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_CPT_OUTBD_ENABLE).equals("true")) {
			chckboxCptOutDu.setSelected(true);
		}
		
		chckboxCptInDu = new JCheckBoxMenuItem("Captain InBoard DU");
		chckboxCptInDu.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (cptinbd == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									cptinbd = new DisplayUnit(model_instance, "Captain InBoard Display");
									cptinbd.setVisible(true);		
									DUHeartbeat cptinbd_heartbeat = new DUHeartbeat(cptinbd, 500);
									cptinbd_heartbeat.start();
									preferences.set_preference(ZHSIPreferences.PREF_CPT_INBD_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (cptinbd != null) {
						cptinbd.dispose();
						//cptinbd_heartbeat.stop();
						cptinbd = null;
						preferences.set_preference(ZHSIPreferences.PREF_CPT_INBD_ENABLE, "false");
					}
				}
			}
		});
		mnDisplays.add(chckboxCptInDu);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_CPT_INBD_ENABLE).equals("true")) {
			chckboxCptInDu.setSelected(true);
		}
		
		separator = new JSeparator();

		mnDisplays.add(separator);
		
		chckboxFoOutDu = new JCheckBoxMenuItem("First Officer OutBoard DU");
		chckboxFoOutDu.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (fooutbd == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									fooutbd = new DisplayUnit(model_instance, "FO OutBoard Display");
									fooutbd.setVisible(true);
									DUHeartbeat fooutbd_heartbeat = new DUHeartbeat(fooutbd, 500);
									fooutbd_heartbeat.start();
									preferences.set_preference(ZHSIPreferences.PREF_FO_OUTBD_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (fooutbd != null) {
						fooutbd.dispose();
						//fooutbd_heartbeat.stop();
						fooutbd = null;
						preferences.set_preference(ZHSIPreferences.PREF_FO_OUTBD_ENABLE, "false");
					}
				}
			}
		});
		mnDisplays.add(chckboxFoOutDu);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_FO_OUTBD_ENABLE).equals("true")) {
			chckboxFoOutDu.setSelected(true);
		}
		
		chckboxFoInDu = new JCheckBoxMenuItem("First Officer InBoard DU");
		chckboxFoInDu.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (foinbd == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									foinbd = new DisplayUnit(model_instance, "FO InBoard Display");
									foinbd.setVisible(true);
									DUHeartbeat foinbd_heartbeat = new DUHeartbeat(foinbd, 500);
									foinbd_heartbeat.start();
									preferences.set_preference(ZHSIPreferences.PREF_FO_INBD_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (foinbd != null) {
						foinbd.dispose();
						//foinbd_heartbeat.stop();
						foinbd = null;
						preferences.set_preference(ZHSIPreferences.PREF_FO_INBD_ENABLE, "false");
					}
				}
			}
		});
		mnDisplays.add(chckboxFoInDu);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_FO_INBD_ENABLE).equals("true")) {
			chckboxFoInDu.setSelected(true);
		}
		
		separator_1 = new JSeparator();

		mnDisplays.add(separator_1);
		
		chckboxUpperDu = new JCheckBoxMenuItem("Upper EICAS DU");
		chckboxUpperDu.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (upeicas == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									upeicas = new DisplayUnit(model_instance, "Upper EICAS Display");
									upeicas.setVisible(true);
									DUHeartbeat upeicas_heartbeat = new DUHeartbeat(upeicas, 500);
									upeicas_heartbeat.start();
									preferences.set_preference(ZHSIPreferences.PREF_UP_EICAS_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (upeicas != null) {
						upeicas.dispose();
						//upeicas_heartbeat.stop();
						upeicas = null;
						preferences.set_preference(ZHSIPreferences.PREF_UP_EICAS_ENABLE, "false");
					}
				}
			}
		});
		mnDisplays.add(chckboxUpperDu);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_UP_EICAS_ENABLE).equals("true")) {
			chckboxUpperDu.setSelected(true);
		}
		
		chckboxLowerDu = new JCheckBoxMenuItem("Lower EICAS DU");
		chckboxLowerDu.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (loeicas == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									loeicas = new DisplayUnit(model_instance, "Lower EICAS Display");
									loeicas.setVisible(true);
									DUHeartbeat loeicas_heartbeat = new DUHeartbeat(loeicas, 500);
									loeicas_heartbeat.start();
									preferences.set_preference(ZHSIPreferences.PREF_LO_EICAS_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (loeicas != null) {
						loeicas.dispose();
						//loeicas_heartbeat.stop();
						loeicas = null;
						preferences.set_preference(ZHSIPreferences.PREF_LO_EICAS_ENABLE, "false");
					}
				}
			}
		});
		mnDisplays.add(chckboxLowerDu);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_LO_EICAS_ENABLE).equals("true")) {
			chckboxLowerDu.setSelected(true);
		}
		
		mnInstruments = new JMenu("Instruments");
		menuBar.add(mnInstruments);
		
		chckbxIsfd = new JCheckBoxMenuItem("Integrated Standby Flight Display (ISFD)");
		chckbxIsfd.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (isfd == null) {
						EventQueue.invokeLater(new Runnable() {

							@Override
							public void run() {
								try {
									isfd = new Instrument(model_instance, "ISFD", "Captain");
									isfd.setVisible(true);
									preferences.set_preference(ZHSIPreferences.PREF_ISFD_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}						
						});
					}
				}else {
					if (isfd != null) {
						isfd.dispose();
						isfd = null;
						preferences.set_preference(ZHSIPreferences.PREF_ISFD_ENABLE, "false");
					}
				}
			}			
		});
		mnInstruments.add(chckbxIsfd);
		
		
		chckbxCptChrono = new JCheckBoxMenuItem("Captain Chrono");
		chckbxCptChrono.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (cptChrono == null) {
						EventQueue.invokeLater(new Runnable() {

							@Override
							public void run() {
								try {
									cptChrono = new Instrument(model_instance, "Captain Chrono", "Captain");
									cptChrono.setVisible(true);
									preferences.set_preference(ZHSIPreferences.PREF_CPTCHRONO_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}						
						});
					}
				}else {
					if (cptChrono != null) {
						cptChrono.dispose();
						cptChrono = null;
						preferences.set_preference(ZHSIPreferences.PREF_CPTCHRONO_ENABLE, "false");
					}
				}
			}			
		});
		mnInstruments.add(chckbxCptChrono);
		
		chckbxFoChrono = new JCheckBoxMenuItem("First Officer Chrono");
		chckbxFoChrono.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (foChrono == null) {
						EventQueue.invokeLater(new Runnable() {

							@Override
							public void run() {
								try {
									foChrono = new Instrument(model_instance, "First Officer Chrono", "First Officer");
									foChrono.setVisible(true);
									preferences.set_preference(ZHSIPreferences.PREF_FOCHRONO_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}						
						});
					}
				}else {
					if (foChrono != null) {
						foChrono.dispose();
						foChrono = null;
						preferences.set_preference(ZHSIPreferences.PREF_FOCHRONO_ENABLE, "false");
					}
				}
			}			
		});
		mnInstruments.add(chckbxFoChrono);
		
		mnInstDisplays = new JMenu("Instrument Displays");
		mnInstruments.add(mnInstDisplays);
		
		chckbxElecDisplay = new JCheckBoxMenuItem("Electrical Display");
		chckbxElecDisplay.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (elecPanel == null) {
						EventQueue.invokeLater(new Runnable() {

							@Override
							public void run() {
								try {
									elecPanel = new Instrument(model_instance, "Electrical Display", "Captain");
									elecPanel.setVisible(true);
									preferences.set_preference(ZHSIPreferences.PREF_ELECPANEL_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}						
						});
					}
				}else {
					if (elecPanel != null) {
						elecPanel.dispose();
						elecPanel = null;
						preferences.set_preference(ZHSIPreferences.PREF_ELECPANEL_ENABLE, "false");
					}
				}
			}			
		});
		mnInstDisplays.add(chckbxElecDisplay);
		
		
		chckbxIRSDisplay = new JCheckBoxMenuItem("IRS Display");
		chckbxIRSDisplay.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (irs == null) {
						EventQueue.invokeLater(new Runnable() {

							@Override
							public void run() {
								try {
									irs = new Instrument(model_instance, "IRS Display", "Captain");
									irs.setVisible(true);
									preferences.set_preference(ZHSIPreferences.PREF_IRSPANEL_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}	
							}						
						});
					}
				}else {
					if (irs != null) {
						irs.dispose();
						irs = null;
						preferences.set_preference(ZHSIPreferences.PREF_IRSPANEL_ENABLE, "false");
					}
				}
			}			
		});
		mnInstDisplays.add(chckbxIRSDisplay);
		
		
		
		if(this.preferences.get_preference(ZHSIPreferences.PREF_CPTCHRONO_ENABLE).equals("true")) {
			chckbxCptChrono.setSelected(true);
		}else {
			chckbxCptChrono.setSelected(false);
		}
		if(this.preferences.get_preference(ZHSIPreferences.PREF_FOCHRONO_ENABLE).equals("true")) {
			chckbxFoChrono.setSelected(true);
		}else {
			chckbxFoChrono.setSelected(false);
		}
		
		
		if(this.preferences.get_preference(ZHSIPreferences.PREF_ISFD_ENABLE).equals("true")) {
			chckbxIsfd.setSelected(true);
		}else {
			chckbxIsfd.setSelected(false);
		}
		
		if(this.preferences.get_preference(ZHSIPreferences.PREF_ELECPANEL_ENABLE).equals("true")) {
			chckbxElecDisplay.setSelected(true);
		}else {
			chckbxElecDisplay.setSelected(false);
		}
		
		if(this.preferences.get_preference(ZHSIPreferences.PREF_IRSPANEL_ENABLE).equals("true")) {
			chckbxIRSDisplay.setSelected(true);
		}else {
			chckbxIRSDisplay.setSelected(false);
		}
		
		
		mnGauges = new JMenu("Gauges");
		menuBar.add(mnGauges);
		
		chckboxFlaps = new JCheckBoxMenuItem("Flap Gauge");
		chckboxFlaps.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					if (flapGauge == null) {
						EventQueue.invokeLater(new Runnable() {
							public void run() {
								try {
									flapGauge = new FlapGauge(model_instance, "Flap Gauge");
									flapGauge.setVisible(true);
									preferences.set_preference(ZHSIPreferences.PREF_FLAP_GAUGE_ENABLE, "true");
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						});	
					}
				}else {
					if (flapGauge != null) {
						flapGauge.dispose();
						flapGauge = null;
						preferences.set_preference(ZHSIPreferences.PREF_FLAP_GAUGE_ENABLE, "false");
					}
				}
			}
		});
		mnGauges.add(chckboxFlaps);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_FLAP_GAUGE_ENABLE).equals("true")) {
			chckboxFlaps.setSelected(true);
		}else {
			chckboxFlaps.setSelected(false);
		}
		
		mnOptions = new JMenu("Options");
		menuBar.add(mnOptions);
		
		chckboxDuAlwaysOnTop = new JCheckBoxMenuItem("Displays Always On Top");
		chckboxDuAlwaysOnTop.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_ALWAYSONTOP, "true");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_ALWAYSONTOP, "false");
				}
			}
		});
		
		chckboxDuAlwaysOnTop.setToolTipText("DU Displays Always on top - requires restart");
		mnOptions.add(chckboxDuAlwaysOnTop);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_ALWAYSONTOP).equals("true")) {
			chckboxDuAlwaysOnTop.setSelected(true);
		}else {
			chckboxDuAlwaysOnTop.setSelected(false);
		}
		
		chckbxUiMinimized = new JCheckBoxMenuItem("Start with UI Minimized");
		chckbxUiMinimized.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_START_MINIMIZED, "true");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_START_MINIMIZED, "false");
				}
			}
		});
		chckbxUiMinimized.setToolTipText("Start this Window minimized - requires restart");
		mnOptions.add(chckbxUiMinimized);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_START_MINIMIZED).equals("true")) {
			chckbxUiMinimized.setSelected(true);
		}else {
			chckbxUiMinimized.setSelected(false);
		}
		
		separator_4 = new JSeparator();
		mnOptions.add(separator_4);
		
		mnFuelUnits = new JMenu("Fuel Units");
		mnOptions.add(mnFuelUnits);
		
		chckboxFuelKgs = new JCheckBoxMenuItem("KGS");
		chckboxFuelLbs = new JCheckBoxMenuItem("LBS");
	
		chckboxFuelKgs.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_FUEL_LBS, "false");
					chckboxFuelLbs.setSelected(false);
				}
			}
		});
		chckboxFuelKgs.setToolTipText("Show Fuel in Kilograms");
		mnFuelUnits.add(chckboxFuelKgs);
		
		
		chckboxFuelLbs.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_FUEL_LBS, "true");
					chckboxFuelKgs.setSelected(false);
				}
			}
		});
		chckboxFuelLbs.setToolTipText("Show Fuel in Pounds");
		
		if(preferences.get_preference(ZHSIPreferences.PREF_FUEL_LBS).equals("false")) {
			chckboxFuelKgs.setSelected(true);
			chckboxFuelLbs.setSelected(false);
		}else {
			chckboxFuelKgs.setSelected(false);
			chckboxFuelLbs.setSelected(true);
		}
		mnFuelUnits.add(chckboxFuelLbs);
		
		mnFuelDisplay = new JMenu("Fuel Display");
		mnOptions.add(mnFuelDisplay);
		
		chckboxFuelSidebySide = new JCheckBoxMenuItem("Side-by-Side");
		chckboxFuelOverUnder = new JCheckBoxMenuItem("Over Under");
		
		chckboxFuelSidebySide.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_FUEL_OVER_UNDER, "false");
					chckboxFuelOverUnder.setSelected(false);
				}
			}
		});
		
		chckboxFuelOverUnder.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_FUEL_OVER_UNDER, "true");
					chckboxFuelSidebySide.setSelected(false);
				}
			}
		});
		
		chckboxFuelSidebySide.setToolTipText("Enable Side-by-Side Fuel Display");
		mnFuelDisplay.add(chckboxFuelSidebySide);
		
		
		chckboxFuelOverUnder.setToolTipText("Enable Over Under Fuel Display");
		mnFuelDisplay.add(chckboxFuelOverUnder);
		
		if(preferences.get_preference(ZHSIPreferences.PREF_FUEL_OVER_UNDER).equals("true")) {
			chckboxFuelOverUnder.setSelected(true);
			chckboxFuelSidebySide.setSelected(false);
		}else {
			chckboxFuelOverUnder.setSelected(false);
			chckboxFuelSidebySide.setSelected(true);
		}
		
		separator_3 = new JSeparator();
		mnOptions.add(separator_3);
		
		chckboxRoundAltimeter = new JCheckBoxMenuItem("Display Round Altimeter (PFD)");
		chckboxRoundAltimeter.setToolTipText("Displays the Round Atlimeter instead of the AoA indicator");
		chckboxRoundAltimeter.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_AOA_INDICATOR, "false");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_AOA_INDICATOR, "true");
				}
			}
		});
		mnOptions.add(chckboxRoundAltimeter);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_AOA_INDICATOR).equals("false")) {
			chckboxRoundAltimeter.setSelected(true);
		}
		
		chckboxTrimIndicator = new JCheckBoxMenuItem("Show Stab Trim Indicator (Upper EICAS)");
		chckboxTrimIndicator.setToolTipText("Displays the Stab Trim indicator in the Upper EICAS");
		if(this.preferences.get_preference(ZHSIPreferences.PREF_TRIM_INDICATOR).equals("true")) {
			chckboxTrimIndicator.setSelected(true);
		}
		chckboxTrimIndicator.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_TRIM_INDICATOR, "true");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_TRIM_INDICATOR, "false");
				}
			}
		});
		mnOptions.add(chckboxTrimIndicator);
		
		chckbxHwThrottlePos = new JCheckBoxMenuItem("Show Hardware Throttle Lever Positions (Upper  EICAS)");
		chckbxHwThrottlePos.setToolTipText("Displays the Hardware Throttle Positions in the Upper EICAS");
		if(this.preferences.get_preference(ZHSIPreferences.PREF_HARDWARE_LEVERS).equals("true")) {
			chckbxHwThrottlePos.setSelected(true);
		}
		chckbxHwThrottlePos.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_HARDWARE_LEVERS, "true");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_HARDWARE_LEVERS, "false");
				}
			}
		});
		mnOptions.add(chckbxHwThrottlePos);
		
		chckbxCompactDisplay = new JCheckBoxMenuItem("Enable Compact Engine Display (Upper  EICAS)");
		chckbxCompactDisplay.setToolTipText("Enables the Compact Engine Display in the Upper EICAS");
		if(this.preferences.get_preference(ZHSIPreferences.PREF_COMPACT_DISPLAY).equals("true")) {
			chckbxCompactDisplay.setSelected(true);
		}
		chckbxCompactDisplay.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_COMPACT_DISPLAY, "true");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_COMPACT_DISPLAY, "false");
				}
			}
		});
		mnOptions.add(chckbxCompactDisplay);
		
		separator_7 = new JSeparator();
		mnOptions.add(separator_7);
		
		mnWeatherRadarDetail = new JMenu("Weather Radar Detail");
		mnOptions.add(mnWeatherRadarDetail);
		
		chckboxWeatherLow = new JCheckBoxMenuItem("Low");

		mnWeatherRadarDetail.add(chckboxWeatherLow);
		
		chckboxWeatherMedium = new JCheckBoxMenuItem("Medium");
		mnWeatherRadarDetail.add(chckboxWeatherMedium);
		
		chckboxWeatherHigh = new JCheckBoxMenuItem("High (more CPU Intensive)");
		mnWeatherRadarDetail.add(chckboxWeatherHigh);
		
		chckboxWeatherLow.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_WEATHER_DETAIL, "low");
					chckboxWeatherMedium.setSelected(false);
					chckboxWeatherHigh.setSelected(false);
				}
			}
		});
		chckboxWeatherMedium.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_WEATHER_DETAIL, "medium");
					chckboxWeatherLow.setSelected(false);
					chckboxWeatherHigh.setSelected(false);
				}
			}
		});
		chckboxWeatherHigh.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_WEATHER_DETAIL, "high");
					chckboxWeatherLow.setSelected(false);
					chckboxWeatherMedium.setSelected(false);
				}
			}
		});
		
		if(this.preferences.get_preference(ZHSIPreferences.PREF_WEATHER_DETAIL).equals("low")) {
			chckboxWeatherLow.setSelected(true);
			chckboxWeatherMedium.setSelected(false);
			chckboxWeatherHigh.setSelected(false);
		}else if(this.preferences.get_preference(ZHSIPreferences.PREF_WEATHER_DETAIL).equals("medium")) {
			chckboxWeatherLow.setSelected(false);
			chckboxWeatherMedium.setSelected(true);
			chckboxWeatherHigh.setSelected(false);
		}else {
			chckboxWeatherLow.setSelected(false);
			chckboxWeatherMedium.setSelected(false);
			chckboxWeatherHigh.setSelected(true);
		}
		
		mntmSettings = new JMenuItem("Settings...");
		mntmSettings.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				settingsDialog.setVisible(true);
			}
		});
		
		chckboxXRaas = new JCheckBoxMenuItem("Enable X-RAAS Messages on ND");
		chckboxXRaas.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_XRAAS_ENABLE, "true");
				}else {
					preferences.set_preference(ZHSIPreferences.PREF_XRAAS_ENABLE, "false");
				}
			}
		});
		
		mnTerrainDisplayDetail = new JMenu("Terrain Display Detail");
		mnOptions.add(mnTerrainDisplayDetail);
		
		chckboxTerrainLow = new JCheckBoxMenuItem("Low");
		mnTerrainDisplayDetail.add(chckboxTerrainLow);
		
		chckboxTerrainMedium = new JCheckBoxMenuItem("Medium");
		mnTerrainDisplayDetail.add(chckboxTerrainMedium);
		
		chckboxTerrainHigh = new JCheckBoxMenuItem("High (more CPU Intensive)");
		mnTerrainDisplayDetail.add(chckboxTerrainHigh);
		
		chckboxTerrainLow.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_TERRAIN_DETAIL, "low");
					chckboxTerrainMedium.setSelected(false);
					chckboxTerrainHigh.setSelected(false);
				}
			}
		});
		chckboxTerrainMedium.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_TERRAIN_DETAIL, "medium");
					chckboxTerrainLow.setSelected(false);
					chckboxTerrainHigh.setSelected(false);
				}
			}
		});
		chckboxTerrainHigh.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED) {
					preferences.set_preference(ZHSIPreferences.PREF_TERRAIN_DETAIL, "high");
					chckboxTerrainLow.setSelected(false);
					chckboxTerrainMedium.setSelected(false);
				}
			}
		});
		
		if(this.preferences.get_preference(ZHSIPreferences.PREF_TERRAIN_DETAIL).equals("low")) {
			chckboxTerrainLow.setSelected(true);
			chckboxTerrainMedium.setSelected(false);
			chckboxTerrainHigh.setSelected(false);
		}else if(this.preferences.get_preference(ZHSIPreferences.PREF_TERRAIN_DETAIL).equals("medium")) {
			chckboxTerrainLow.setSelected(false);
			chckboxTerrainMedium.setSelected(true);
			chckboxTerrainHigh.setSelected(false);
		}else {
			chckboxTerrainLow.setSelected(false);
			chckboxTerrainMedium.setSelected(false);
			chckboxTerrainHigh.setSelected(true);
		}
		
		separator_9 = new JSeparator();
		mnOptions.add(separator_9);
		mnOptions.add(chckboxXRaas);
		if(preferences.get_preference(ZHSIPreferences.PREF_XRAAS_ENABLE).equals("true")) {
			chckboxXRaas.setSelected(true);
		}else {
			chckboxXRaas.setSelected(false);
		}
		
		separator_8 = new JSeparator();
		mnOptions.add(separator_8);
		mnOptions.add(mntmSettings);
		
		mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		mntmAbout = new JMenuItem("About");
		mntmAbout.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				aboutDialog.setVisible(true);
			}
		});
		mnHelp.add(mntmAbout);
		
		separator_2 = new JSeparator();
		mnHelp.add(separator_2);
		
		mntmCreateIssue = new JMenuItem("View or Create a New Issue ...");
		mntmCreateIssue.setToolTipText("Before creating a new issue, please look at the current list of issues first !!");
		mntmCreateIssue.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				openWebPage("https://gitlab.com/sum1els737/zhsi-releases/issues");
			}
		});
		mnHelp.add(mntmCreateIssue);
		
		separator_6 = new JSeparator();
		mnHelp.add(separator_6);
		
		mntmGetHelp = new JMenuItem("Wiki ...");
		mntmGetHelp.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				openWebPage("https://gitlab.com/sum1els737/zhsi-releases/wikis/ZHSI");
			}
		});
		mnHelp.add(mntmGetHelp);
		
		separator_10 = new JSeparator();
		mnHelp.add(separator_10);
		
		mntmRemoveNag = new JMenuItem("Patreon Support ...");
		mntmRemoveNag.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				openWebPage("https://www.patreon.com/sum1els737");
				
			}
		});
		mnHelp.add(mntmRemoveNag);
		
		mntmRegister = new JMenuItem("Register ...");
		mntmRegister.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseReleased(MouseEvent e) {
				openWebPage("https://bitbucket.org/sum1els737/zhsi/wiki/Home#markdown-header-license-key-register");
				
			}
		});
		if(!ZHSIStatus.non_commercial_registered) {
			mnHelp.add(mntmRegister);
		}
		
		
		
		statusBar = new JPanel();
		
		statusBar.setBackground(Color.BLACK);
		contentPane.add(statusBar, BorderLayout.SOUTH);
		statusBar.setLayout(new GridLayout(2, 2, 1, 1));
		
		lblXPStatus = new JLabel("X-Plane Connection:");
		lblXPStatus.setForeground(Color.WHITE);
		lblXPStatus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(lblXPStatus);
		
		lblNavDBStatus = new JLabel("NAV DB Status:");
		lblNavDBStatus.setForeground(Color.WHITE);
		lblNavDBStatus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(lblNavDBStatus);
		
		lblEgpwsStatus = new JLabel("EGPWS Status:");
		lblEgpwsStatus.setForeground(Color.WHITE);
		lblEgpwsStatus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(lblEgpwsStatus);
		
		lblWeatherStatus = new JLabel("Weather Status:");
		lblWeatherStatus.setForeground(Color.WHITE);
		lblWeatherStatus.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(lblWeatherStatus);
		
		statusXP = new JLabel("Not Connected!");
		statusXP.setForeground(Color.RED);
		statusXP.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(statusXP);
		
		statusNavDb = new JLabel("Not Loaded!");
		statusNavDb.setForeground(Color.RED);
		statusNavDb.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(statusNavDb);
		
		statusEgpws = new JLabel("Not Found!");
		statusEgpws.setForeground(Color.RED);
		statusEgpws.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(statusEgpws);
		
		statusWeather = new JLabel("Not Recieving!");
		statusWeather.setForeground(Color.RED);
		statusWeather.setFont(new Font("Tahoma", Font.PLAIN, 12));
		statusBar.add(statusWeather);
		
		//contentPane.add(this.rs, BorderLayout.EAST);
		
		dashBoardPanel = new JPanel();
		contentPane.add(dashBoardPanel, BorderLayout.CENTER);
		dashBoardPanel.setLayout(new GridLayout(7, 2, 0, 0));
		
		nearestAirportPanel = new JPanel();
		dashBoardPanel.add(nearestAirportPanel);
		nearestAirportPanel.setLayout(null);
		
		lblNearestAirport = new JLabel("Nearest Airport:");
		lblNearestAirport.setBounds(10, 11, 153, 20);
		nearestAirportPanel.add(lblNearestAirport);
		
		lblNearestAirportValue = new JLabel("---");
		lblNearestAirportValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblNearestAirportValue.setBounds(155, 11, 430, 20);
		nearestAirportPanel.add(lblNearestAirportValue);
		
		latLonPanel = new JPanel();
		dashBoardPanel.add(latLonPanel);
		latLonPanel.setLayout(null);
		
		lblLatitudeLongtitude = new JLabel("Latitude / Longtitude:");
		lblLatitudeLongtitude.setBounds(10, 11, 153, 20);
		latLonPanel.add(lblLatitudeLongtitude);
		
		lblLatLonValue = new JLabel("---");
		lblLatLonValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblLatLonValue.setBounds(155, 11, 430, 20);
		latLonPanel.add(lblLatLonValue);
		
		altitudePanel = new JPanel();
		dashBoardPanel.add(altitudePanel);
		altitudePanel.setLayout(null);
		
		lblAltitude = new JLabel("Altitude:");
		lblAltitude.setBounds(10, 11, 153, 20);
		altitudePanel.add(lblAltitude);
		
		lblAltitudeValue = new JLabel("---");
		lblAltitudeValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblAltitudeValue.setBounds(155, 11, 430, 20);
		altitudePanel.add(lblAltitudeValue);
		
		iasPanel = new JPanel();
		dashBoardPanel.add(iasPanel);
		iasPanel.setLayout(null);
		
		lblIas = new JLabel("Indicated Air Speed:");
		lblIas.setBounds(10, 11, 153, 20);
		iasPanel.add(lblIas);
		
		lblIasValue = new JLabel("---");
		lblIasValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblIasValue.setBounds(155, 11, 430, 20);
		iasPanel.add(lblIasValue);
		
		headingTrackPanel = new JPanel();
		dashBoardPanel.add(headingTrackPanel);
		headingTrackPanel.setLayout(null);
		
		lblHeadingTrack = new JLabel("Heading / Track:");
		lblHeadingTrack.setBounds(10, 11, 153, 20);
		headingTrackPanel.add(lblHeadingTrack);
		
		lblHeadingTrackValue = new JLabel("---");
		lblHeadingTrackValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblHeadingTrackValue.setBounds(155, 11, 430, 20);
		headingTrackPanel.add(lblHeadingTrackValue);
		
		pitchPanel = new JPanel();
		dashBoardPanel.add(pitchPanel);
		pitchPanel.setLayout(null);
		
		lblPitch = new JLabel("Pitch:");
		lblPitch.setBounds(10, 11, 153, 20);
		pitchPanel.add(lblPitch);
		
		lblPitchValue = new JLabel("---");
		lblPitchValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblPitchValue.setBounds(155, 11, 430, 20);
		pitchPanel.add(lblPitchValue);
		
		bankPanel = new JPanel();
		dashBoardPanel.add(bankPanel);
		bankPanel.setLayout(null);
		
		lblBank = new JLabel("Bank:");
		lblBank.setBounds(10, 11, 153, 20);
		bankPanel.add(lblBank);
		
		lblBankValue = new JLabel("---");
		lblBankValue.setHorizontalAlignment(SwingConstants.CENTER);
		lblBankValue.setBounds(155, 11, 430, 20);
		bankPanel.add(lblBankValue);
		
		//set minimized if selected:
		this.setVisible(true);
		if(this.preferences.get_preference(ZHSIPreferences.PREF_START_MINIMIZED).equals("true")) this.setState(Frame.ICONIFIED);
	}
	
	public void heartbeat() {
		if (ZHSIStatus.receiving) {
			statusXP.setForeground(Color.GREEN);
			statusXP.setText(ZHSIStatus.cStatus);
			lblLatLonValue.setText("" + this.xpd.latitude() + " / " + this.xpd.longitude());
			lblAltitudeValue.setText(String.format("%.0f", this.xpd.altitude("cpt")) + " feet");
			lblIasValue.setText(String.format("%.0f", this.xpd.airspeed("cpt")) + " knots");
			lblHeadingTrackValue.setText("" + Math.round(this.xpd.heading("cpt")) + " / " + Math.round(this.xpd.track()) + " degrees");
			String pitchStr;
			int pitch = Math.round(this.xpd.pitch());
			if(pitch < 0) {
				pitchStr = String.format("%.0f", Math.abs(this.xpd.pitch())) + " degrees nose down";
			}else if(pitch > 0) {
				pitchStr = String.format("%.0f", Math.abs(this.xpd.pitch())) + " degrees nose up";
			}else {
				pitchStr = "0 degress";
			}
			lblPitchValue.setText(pitchStr);
			String bank;
			int roll = Math.round(this.xpd.roll());
			if(roll < 0) {
				bank = String.format("%.0f", Math.abs(this.xpd.roll())) + " degrees to the left";
			}else if(roll > 0) {
				bank = String.format("%.0f", Math.abs(this.xpd.roll())) + " degrees to the right";
			}else {
				bank = "0 degress";
			}
			lblBankValue.setText(bank);
			if (this.xpd.latitude() != 0.0f || this.xpd.longitude() != 0.0f) {
				try {
					String nearest_iaco = nor.find_nrst_arpt(this.xpd.latitude(), this.xpd.longitude(), 1700f, true);
					if (nearest_iaco != null) {
						Airport airport = nor.get_airport(nearest_iaco);
						if(airport != null) {
							float distance = this.xpd.rough_distance_to(airport);
							ZHSIStatus.nearest_airport_elev = airport.elev;
							lblNearestAirportValue.setText(airport.icao_code + " : " + airport.name + " (" + String.format("%.2f", distance) + "nm)");		
						}else {
							lblNearestAirportValue.setForeground(Color.RED);
							lblNearestAirportValue.setText("Error: NAV DB Not Loaded");
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		}else {
			lblLatLonValue.setText("---");
			lblAltitudeValue.setText("---");
			lblIasValue.setText("---");
			lblHeadingTrackValue.setText("---");
			lblPitchValue.setText("---");
			lblBankValue.setText("---");
			lblNearestAirportValue.setText("---");
			statusXP.setForeground(Color.RED);
			statusXP.setText("Trying to connect ...");
		}
		if(ZHSIStatus.nav_db_status == ZHSIStatus.STATUS_NAV_DB_LOADED) {
			statusNavDb.setText(ZHSIStatus.navStatus);
			statusNavDb.setForeground(Color.GREEN);
		}else{
			statusNavDb.setText("Not Loaded!");
			statusNavDb.setForeground(Color.RED);
		}
		if(ZHSIStatus.egpws_db_status == ZHSIStatus.STATUS_EGPWS_DB_LOADED) {
			statusEgpws.setText(ZHSIStatus.egpwsStatus);
			statusEgpws.setForeground(Color.GREEN);
		}else {
			statusEgpws.setText("Not Found!");
			statusEgpws.setForeground(Color.RED);
		}
		if(ZHSIStatus.weather_receiving) {
			statusWeather.setText("Recieving");
			statusWeather.setForeground(Color.GREEN);
		}else {
			statusWeather.setText("Not Recieving!");
			statusWeather.setForeground(Color.RED);
		}
		
	}
	public void openWebPage(String url) {
		
		desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
		if(desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
			try {
				desktop.browse(new URL(url).toURI());
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
}
